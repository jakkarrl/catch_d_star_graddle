package com.jakkarrlgames.entity;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

/**
* @author Raghuram Iyer
*This class is not part of animation Manager.this is just a helper class which i wrote a while ago.
*/
public class Entity {

/**
*
*/
public Sprite sprite;
public TextureRegion region;

public Entity() {

}
public TextureRegion getRegionFromTexture(Texture texture,int tx,int ty,int width,int height) {
region =new TextureRegion(texture, tx, ty,width,height);
return region;

}
public Sprite createSprite(Vector2 size,Vector2 origin,Vector2 position) {

sprite = new Sprite(region);
sprite.setSize(size.x, size.y);
sprite.setOrigin(origin.x,origin.y);
sprite.setPosition(position.x,position.y);
return sprite;
}
public Sprite createSprite(Vector2 size,Vector2 position) {

sprite = new Sprite(region);
sprite.setSize(size.x, size.y);
sprite.setOrigin(sprite.getWidth()/2,sprite.getHeight()/2);
sprite.setPosition(position.x,position.y);
return sprite;
}



public void disposeEntity() {
sprite = null;
region = null;
}
}