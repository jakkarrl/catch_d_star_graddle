package com.jakkarrlgames.entity;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.jakkarrlgames.assetloader.AssetLoader;
import com.jakkarrlgames.game.GamePlay;
import com.jakkarrlgames.game.PreGamePlay;
import com.jakkarrlgames.game.catch_d_stars;
import com.jakkarrlgames.helperclass.Bullet;
import com.jakkarrlgames.helperclass.ClockCount;
import com.jakkarrlgames.helperclass.SaveGame;
import com.jakkarrlgames.persistant.Files;
import com.jakkarrlgames.scroller.Alarm;
import com.jakkarrlgames.scroller.PowerUp;
import com.jakkarrlgames.scroller.SupportPlatform;
import com.jakkarrlgames.scroller.Platform;
import com.jakkarrlgames.scroller.Collectable_Item;
import com.jakkarrlgames.scroller.Comet;
import com.jakkarrlgames.scroller.ScrollHandler;
import com.jakkarrlgames.scroller.UnCollectable_Item;
import com.jakkarrlgames.scroller.Word_Char;
import com.jakkarrlgames.scroller.collectableItem.Candy;
import com.jakkarrlgames.scroller.collectableItem.Coin;
import com.jakkarrlgames.scroller.collectableItem.CupCake;
import com.jakkarrlgames.scroller.collectableItem.Fruit;
import com.jakkarrlgames.scroller.collectableItem.Star;
import com.jakkarrlgames.scroller.word.Word;

public class Player {
	private Vector2 position,velocity,acceleration;
	private int width,height;
	private float keyFrame = 0;
	private float speed = 90 * 5;
	private final float gravity = -350 * 1.8f;
	private Polygon playerBox;
	private Rectangle playerFoot;
	private ScrollHandler scrollHandler;
	private float screenWidth;
	private float screenHeight;
	private Animation animation;
//	private float yPos;
	private boolean isCollisionOccured = false;
	private Random random;
	private Sound jumpSound;
	private Sound pickupSound;
	private catch_d_stars catchdstars;
//	private FontRenderer fontRenderer;
	public static int SCORE = 0;
	public static int STAR,CANDY,COIN,CUPCAKE,FRUIT;
	private boolean gameOverCollision,clockFlag = true,magnetPower = false,superPower = false,switchScreen = false;
//	private boolean isWordCountOver = false;int wordCharPosition =0;
	public static ArrayList<Character> collectedLetters;
	private ArrayList<Bullet> bulletList;
	private Music music;
	//clockPowercode
	private SaveGame saveGame;
	private GUIEntity clockPower,tapToContinue;
//	private float tempX,tempY;
	private OrthographicCamera camera;
	private float clockPowerCount;
	//---------
	
	
	public void dispose(){
		position = null;velocity = null; acceleration = null;
		playerBox = null;playerFoot = null;
		scrollHandler = null;
		animation = null;
		random = null;
		gameOverCollision = false;
		jumpSound.dispose();
		pickupSound.dispose();
	}
	private ClockCount clockCount,magnetBar,superPowerBar;
	public Player(float x,float y,int width,int height,ScrollHandler scrollHandler,ClockCount clockCount,ClockCount magnetBar,ClockCount superPowerBar,catch_d_stars catchdstars,OrthographicCamera camera,Music music){
//		fontRenderer= new FontRenderer("data/font/kenvector_future_thin.ttf",20);
//		
		position = new Vector2(x,y);
		velocity = new Vector2(0,0);
		acceleration = new Vector2(0,gravity);
		this.width = width;
		this.height = height;
		this.music = music;
		playerBox = new Polygon(new float[]{x,y,x+width,y,x+width,y+height,x,y+height});
		playerFoot = new Rectangle(x,y,width,2.5f);
		this.scrollHandler = scrollHandler;
		jumpSound  = Gdx.audio.newSound(Gdx.files.internal("data/SFX/jump.ogg"));
		pickupSound  = Gdx.audio.newSound(Gdx.files.internal("data/SFX/pickup.ogg"));
		
//		this.yPos = y;
		
		animation = PreGamePlay.playerRunAnimation;

		random = new Random();
		this.clockCount = clockCount;
		this.magnetBar = magnetBar;
		this.superPowerBar = superPowerBar;
		
		bulletList = new ArrayList<Bullet>();
		collectedLetters = new ArrayList<Character>();
		
	
		this.catchdstars = catchdstars;
		
		screenWidth = Gdx.graphics.getWidth();
		screenHeight = Gdx.graphics.getHeight();
		//clockPowerCodesaveFromFile = Files.getSaveGame();
		clockPower = new GUIEntity(AssetLoader.clockPowerGUI,AssetLoader.clockPowerGUIPressed);
//		clockPower = catchdstars.getObject().getClock();
		clockPower.setSize(screenWidth/10, screenHeight/15);
		clockPower.setPosition(-500,-100);
//		
		tapToContinue =  new GUIEntity(AssetLoader.tapToContinue,AssetLoader.tapToContinuePressed);
//		tapToContinue =  catchdstars.getObject().getTap();
		tapToContinue.setSize(screenWidth/10, screenHeight/15);
		tapToContinue.setPosition(-25, -25);
		this.camera = camera;
		saveGame = Files.getSaveGame("a");
		System.out.println("SaveGame From File --> " + saveGame.getClockPowerCount() );
		
		//------------
	}
	
	public void resetToOld(){
		speed = 90 * 5;
		keyFrame = 0;
		isCollisionOccured = false;
		SCORE = 0;
		clockPowerCount = 0;
		STAR=CANDY=COIN=CUPCAKE=FRUIT=0;
		gameOverCollision = false;clockFlag = true;magnetPower = false;superPower = false;switchScreen = false;
		int x = Gdx.graphics.getWidth()/10; int y =Gdx.graphics.getHeight() + 100;
		position.set(x, y);
		velocity.set(0,0);
		acceleration.set(0,gravity);
		
		playerBox = new Polygon(new float[]{x,y,x+width,y,x+width,y+height,x,y+height});
		playerFoot = new Rectangle(x,y,width,2.5f);
		animation = PreGamePlay.playerRunAnimation;
		bulletList.clear();
		collectedLetters.clear();
	}
	
	
	
	public void update(float delta){
		
			
		//Update Bullet
				Iterator<Bullet> bullet = bulletList.iterator();
				while(bullet.hasNext()){
					Bullet blt = bullet.next();
					blt.update(delta);
					if(blt.getPosition().x > Gdx.graphics.getWidth()){
						bullet.remove();
					}
					
				}

		playerFoot.setPosition(position);
		keyFrame += delta;
		
		if(position.y > 0){
			velocity.add(acceleration.cpy().scl(delta));
			if(velocity.y > speed) velocity.y = speed;
		}else{
			if(position.y < -50)
				velocity.y = 0;
		}
		
		if(!PlatformCollision() && !supportPlatformCollision()){
			if(!boyIsRaising()){
				position.add(velocity.cpy().scl(delta));
				playerBox.setVertices(getVertices());
				playerFoot.setPosition(position);
				collectableItemCollision();
			}
		}else{
			//Code to Stop the Game
//			setScore(0);
//			scrollHandler.stopScrolling();
		}
		
		//---- Check for Jump Animation -----//
		if(animation == PreGamePlay.playerJumpAnimation){
//			if(animation.getKeyFrameIndex(keyFrame) == 9)
//				animation = PreGamePlay.playerRunAnimation;
			
			System.out.println(animation.getKeyFrameIndex(keyFrame));
		}
		//-------------End-------------------//
		
		//Check for Star Collision
		
		collectableItemCollision();
		unCollectableItemCollision();
		clockCollision();
		bulletCollision();
		
		//------Magnetic Powerup Code-------
		//Checks for player and magnet collisino
		magneticCollision();
		
		if(magnetPower){
			//code to turn on the magnetic field
			magneticField();
			//update the magnet life bar
			magnetBar.update(delta);
		}
		//Turn off the magnetic field if life bar turns 0
		if(magnetBar.getMeterReading() < 1){
			magnetPower = false;
		}
		
		
		//------Super Power Code
//		superPowerCollision();
		if(superPower){
			superPowerBar.update(delta);
		}
		if(superPowerBar.getMeterReading() < 1){
			superPower = false;
		}
		
		//---WordCount Code
//		if(!isWordCountOver){
//			wordCollision();
//		}
		
		Word_Char word = scrollHandler.getwordChar();
		if(word != null && !word.isCountMatch()){
			wordCollision();
		}
		
		if(switchScreen && clockPowerCount < 6) clockPowerCount+=delta;
		
	}

	public float[] getVertices(){
		return new float[]{position.x,position.y,position.x+width,position.y,position.x+width,position.y+height,position.x,position.y+height};
	}
	
	public void render(SpriteBatch batch,OrthographicCamera cam){
//		batch.draw(animation.getKeyFrame(keyFrame), position.x + getWidth(), (position.y - 12),-getWidth(),getHeight());
		batch.draw(animation.getKeyFrame(keyFrame), position.x, position.y ,getWidth(),getHeight());
//		if(superPower)	batch.draw(AssetLoader.shieldAnimation.getKeyFrame(keyFrame), (position.x -10),position.y ,65,65);
		if(superPower)	batch.draw(AssetLoader.shieldAnimation.getKeyFrame(keyFrame), (position.x -15),position.y ,75,75);
		
//		fontRenderer.render(batch, "C : " + saveGame.getCandy()+ ",S : " + saveGame.getStar() + ", clock : " + saveGame.getClockPowerCount() + ", super : " + saveGame.getSuperPowerCount() , 10, 200);
		//fontRenderer.render(batch, "Bullet : " + bulletList.size(), 500, 325);
		//fontRenderer.render(batch, "Magnet Power : " + magnetPower, 400, 225);
		//fontRenderer.render(batch, "MGT" + (int) magnetBar.getMeterReading(), 300, 225);
		//fontRenderer.render(batch, "SPP" + (int) superPowerBar.getMeterReading(), 360, 215);
		//fontRenderer.render(batch, "GameCollision ????" + switchScreen, 360, 115);
		
		//Rendering Bullet
		Iterator<Bullet> bullet = bulletList.iterator();
		while(bullet.hasNext()){
			bullet.next().render(batch);
		}
		
		
		if(animation == PreGamePlay.playerDeadAnimation){
			if(animation.getKeyFrameIndex(keyFrame) == 9){
//				fontRenderer.render(batch, "Press Esc to continue", 100, 200);
				switchScreen = true;
			}
		}
		
		
		//----- draw PrompBG
		if(switchScreen){
			float tempX = (FontRenderer.CAMWIDTH - AssetLoader.promptBG.getWidth())/2;
			float tempY = (FontRenderer.CAMHEIGHT - AssetLoader.promptBG.getHeight())/2;
			batch.end();
//			--------
//			if(Gdx.input.isTouched()){
//				catchdstars.setScreen(catchdstars.getMainScreen());
//			}
			
//			------
			
			
//			if(saveGame == null)  
				saveGame = Files.getSaveGame();
			
			batch.begin();
//			fontRenderer.font.setColor(Color.RED);
			String text = "Game Over, Tap to Continue";
			int len = text.length();
			//fontRenderer.render(batch, text, (tempX + AssetLoader.promptBG.getWidth())/2 - len, (tempY + AssetLoader.promptBG.getHeight())/2);
//			fontRenderer.render(batch,text,tempX+AssetLoader.promptBG.getWidth()/4,FontRenderer.CAMHEIGHT/1.50f);
//			fontRenderer.render(batch,"Clock Count " + clockPowerCount ,100,200);
//			fontRenderer.render(batch,"From File count " + saveGame.getClockPowerCount() + " -- " + saveGame.getSuperPowerCount() ,200,300);
//			fontRenderer.font.setColor(Color.WHITE);
			batch.setProjectionMatrix(cam.combined);
			
			if(clockPower.isPressed()){
				if(position.y < 0) { float tmpX = position.x;position = new Vector2(tmpX,Gdx.graphics.getHeight());}
				if(clockCount.getMeterReading() < 1) clockCount.setMeterReading(100);
				resumeGame();
				animation = PreGamePlay.playerRunAnimation;
				clockCount.setStop(false);
				switchScreen = false;
				isCollisionOccured = false;gameOverCollision = false;magnetPower = false; superPower = false;switchScreen = false;
				clockPowerCount = 0;
				
				//Change the sampleJson
				saveGame.setClockPowerCount(saveGame.getClockPowerCount() - 1);
				Files.saveGameStatus(saveGame);
				System.out.println("Changed----> " + Files.getSaveGame("a").getClockPowerCount());
			}
			if(tapToContinue.isPressed()){
//				fontRenderer.render(batch, "Touched on screen", Gdx.graphics.getWidth()/2.54f, Gdx.graphics.getHeight()/1.75f);
				//catchdstars.setScreen(catchdstars.getMainScreen());
				if(music.isPlaying()) {
					music.stop();
				}
				GamePlay.isPlaying = false;
				catchdstars.setScreen(catchdstars.getGameOut());
			}
//			else if(Gdx.input.isTouched()){
////				fontRenderer.render(batch, "Touched on screen", Gdx.graphics.getWidth()/2.54f, Gdx.graphics.getHeight()/1.75f);
//				//catchdstars.setScreen(catchdstars.getMainScreen());
//				catchdstars.setScreen(catchdstars.getGameOut());
//			}
		}
	
	}
	public void renderPrompt(SpriteBatch batch) {
		if(switchScreen) {

			float tempX = (FontRenderer.CAMWIDTH - AssetLoader.promptBG.getWidth())/2;
			float tempY = (FontRenderer.CAMHEIGHT - AssetLoader.promptBG.getHeight())/2;
			batch.draw(AssetLoader.promptBG,tempX,tempY);
			batch.end();
			tapToContinue.setPosition(screenWidth/2.75f,screenHeight/3f);
			tapToContinue.render(batch,camera);
			
			if(clockPowerCount < 6 && saveGame.getClockPowerCount() > 0){
//				if(clockPowerCount < 6){
				System.out.println("RENDERING");
				clockPower.setPosition(screenWidth/1.90f,screenHeight/3f);	
				//clockPower.setPosition(screenWidth/2.75f,screenHeight/2.35f); 
					clockPower.render(batch,camera);
				}
			else{
					clockPower.setPosition(-500, -100);
					clockPower.render(batch,camera);
				}
				batch.begin();
		}
	}
	
	public void onClick(){
		if(!gameOverCollision){
			if(PlatformCollision() || supportPlatformCollision()){
				PreGamePlay.playerJumpAnimation.setPlayMode(PlayMode.NORMAL);
				animation = PreGamePlay.playerJumpAnimation;
				keyFrame = 0;
				
				if(!gameOverCollision){
					velocity.y = speed/1.1f;
					int tmp =0;
					while(tmp < 3){
						position.add(velocity.cpy().scl(Gdx.graphics.getDeltaTime()));
						playerBox.setVertices(getVertices());
						collectableItemCollision();
						clockCollision();
						PlatformCollision();
						supportPlatformCollision();
						wordCollision();
						magneticCollision();
						tmp++;
						if(GamePlay.saveGame.isMusicOn()) {
							jumpSound.play(0.27f);
						}
						
					}
				}
			}
		}
	}
	
	public void slide(){
		velocity.y = -350;
		velocity.add(acceleration.cpy().scl(Gdx.graphics.getDeltaTime()));
	}
	
//	//Detect Plan vs Ground Collision and stop plane falling down further
	public boolean PlatformCollision(){
		Iterator<Platform> it = scrollHandler.getCloudPlatform().iterator();
		while(it.hasNext()){
			Platform cloud = it.next();
				if((cloud.getY() + cloud.getHeight() /2) < position.y){
					
					if(Intersector.overlapConvexPolygons(cloud.getPolygon(),playerBox)){
						//Set Animation = RunAnimation from JumpAnimation
						animation = PreGamePlay.playerRunAnimation;
//						keyFrame = 0;
						return isCollisionOccured = true;
					}
				}
			
		}
		return false;
	}
	
	//Code to pull the coin towards player
	public void magneticField(){
		float tmpx = 0,tmpy =0;
		Iterator<Collectable_Item> item = scrollHandler.getCollectableItem().iterator();
		while(item.hasNext()){
			Collectable_Item coll = item.next();
			
			if(coll.getX() < getPosition().x){
				tmpx = (coll.getX()+3);
			}else if(coll.getX() > getPosition().x){
				if((coll.getX() - getPosition().x) < 80){
					tmpx = (coll.getX()-3);
				}else{
					tmpx =coll.getX();
				}
			}
			if((coll.getX() - getPosition().x) < 80){
				if(coll.getY() < getPosition().y){
					tmpy = (coll.getY()+10); 
				}else if(coll.getY() > getPosition().y){
					tmpy = (coll.getY()-5);
				}
			}else{
				tmpy = coll.getY();
			}
			coll.setPosition(tmpx, tmpy);
			
		}
	}
	
	public boolean supportPlatformCollision(){
		Iterator<SupportPlatform> it = scrollHandler.getCloud().iterator();
		while(it.hasNext()){
			SupportPlatform cld = it.next();
			if(velocity.y < 0 ){
				if(Intersector.overlaps(cld.getRectangle(), playerFoot)){
					//Set Animation = RunAnimation from JumpAnimation
					animation = PreGamePlay.playerRunAnimation;
//					keyFrame = 0;
					return isCollisionOccured = true;
				}
			}
		}
		return false;
	}
	
	public boolean wordCollision(){
		Word_Char word = scrollHandler.getwordChar();
		if(word!=null){
			if(Intersector.overlapConvexPolygons(word.getPolygon(), playerBox)){
				collectedLetters.add(word.getCurrentLetter());
				word.reset(-random.nextInt(Gdx.graphics.getWidth()),Gdx.graphics.getHeight());
				if((word.totalWordCount()-1) > word.getCurrentPosition() ){
					word.setWordPosition(word.getWordPosition() + 1);
				}else{
					word.stop();
					//scrollHandler.setWord();
				}
			}
		}
		return false;
	}
	public boolean boyIsRaising(){
		if(velocity.y > 800) return true;
		return false;
	}
	
	
	public void createBullet(){
		bulletList.add(new Bullet(position.x,(position.y+height/2),15,15));
	}

	public void collectableItemCollision(){
		Iterator<Collectable_Item> item = scrollHandler.getCollectableItem().iterator();
		while(item.hasNext()){
			Collectable_Item itemTmp = item.next();
			if(Intersector.overlapConvexPolygons(itemTmp.getPolygon(),playerBox)){
				itemTmp.reset(random.nextInt(Gdx.graphics.getWidth()) + 20,Gdx.graphics.getHeight());
				
				if(itemTmp instanceof Star){
					SCORE += 3;
					STAR++;
				}else if(itemTmp instanceof Candy){
					SCORE += 4;
					CANDY++;
				}else if(itemTmp instanceof Coin){
					SCORE += 5;
					COIN++;
				}else if(itemTmp instanceof CupCake){
					SCORE += 8;
					CUPCAKE++;
				}else if(itemTmp instanceof Fruit){
					SCORE += 10;
					FRUIT++;
				}
//				else if(itemTmp instanceof CollectableItem2)
//					playScore += 15;
//				else if(itemTmp instanceof CollectableItem3)
//					playScore += 20;
				if(GamePlay.saveGame.isMusicOn()) {
					pickupSound.play(0.1f);
				}
			}
		}
	}
	
	
	public void unCollectableItemCollision(){
		Iterator<UnCollectable_Item> it = scrollHandler.getUnCollectableItem().iterator();
		while(it.hasNext()){
			UnCollectable_Item unCollectableItem = it.next();
		
			if(unCollectableItem instanceof Comet){
				if(Intersector.overlapConvexPolygons(unCollectableItem.getPolygon(),playerBox)){
					if(!superPower)	{
//						unCollectableItem.reset(random.nextInt(Gdx.graphics.getWidth()), Gdx.graphics.getHeight());
						gameOverCollision = true;
						keyFrame = 0;
					}
					unCollectableItem.reset(-Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
				}
			}else if(unCollectableItem instanceof Spider){
				//Player collision with SpiderWeb
				if(Intersector.overlapConvexPolygons(((Spider) unCollectableItem).getWebPolygon(), playerBox)){
					((Spider) unCollectableItem).setTargetFallPosition(0);
				}
				
				//Player Collision with SPider
				if(Intersector.overlapConvexPolygons(unCollectableItem.getPolygon(),playerBox)){
					if(!superPower)	{
						((Spider)unCollectableItem).setTargetFallPosition(0);
//						unCollectableItem.reset(random.nextInt(Gdx.graphics.getWidth()), Gdx.graphics.getHeight());
						gameOverCollision = true;
						keyFrame = 0;
					}
					unCollectableItem.reset(-Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
				}
			}else if(unCollectableItem instanceof Alien){
				if(Intersector.overlapConvexPolygons(unCollectableItem.getPolygon(),playerBox)){
					if(!superPower)	{
//						unCollectableItem.reset(random.nextInt(Gdx.graphics.getWidth()), Gdx.graphics.getHeight());
						gameOverCollision = true;
						keyFrame = 0;
					}
					unCollectableItem.reset(-Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
				}
			}
			
		}
	}

	
	public void clockCollision(){
		Alarm alarm = scrollHandler.getAlarm();
		if(Intersector.overlapConvexPolygons(alarm.getPolygon(), playerBox)){
			alarm.reset(-10 - alarm.getWidth(),Gdx.graphics.getHeight());
			clockCount.setMeterReading(100);
			if(clockFlag) clockFlag = false;
			else clockFlag = true;
//			scrollHandler.increaseGameSpeed();
			//Add Stuffs to the Game
			if(clockFlag){
				
				int tmp = scrollHandler.getcollectableItemList().length;
				if(tmp == 2){
					scrollHandler.setcollectableItemList(new String[]{"star","candy","coin"});
					scrollHandler.fillList(scrollHandler.getcollectableItemList());
				}else if(tmp == 3){
					scrollHandler.setcollectableItemList(new String[]{"star","candy","coin","cupcake"});
					scrollHandler.fillList(scrollHandler.getcollectableItemList());
				}else if(tmp == 4){
					scrollHandler.setcollectableItemList(new String[]{"star","candy","coin","cupcake","spaceship"});
					scrollHandler.fillList(scrollHandler.getcollectableItemList());
					System.out.println("Tmp : " + tmp);
				}
			}
		}
		
	}
	
	public void bulletCollision(){
		Iterator<Bullet> blt = bulletList.iterator();
		while(blt.hasNext()){
			Bullet tmp = blt.next();
			System.out.println("Bullet Tmp");
			Iterator<UnCollectable_Item> it = scrollHandler.getUnCollectableItem().iterator();
			while(it.hasNext()){
				UnCollectable_Item itm = it.next();
				System.out.println("UnCollectable ITM");
				
					if(itm instanceof Comet){
						if(Intersector.overlapConvexPolygons(itm.getPolygon(),tmp.getBulletPolygon())){
							itm.reset(-Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
							blt.remove();
						}
					}else if(itm instanceof Spider){
						//Player collision with SpiderWeb
						if(Intersector.overlapConvexPolygons(((Spider) itm).getWebPolygon(), tmp.getBulletPolygon())){
							((Spider) itm).setTargetFallPosition(0);
							blt.remove();
						}
						
						//Player Collision with SPider
						if(Intersector.overlapConvexPolygons(itm.getPolygon(),tmp.getBulletPolygon())){
							((Spider)itm).setTargetFallPosition(0);
							((Spider)itm).reset(-Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
							blt.remove();
						}
					}else if(itm instanceof Alien){
						if(Intersector.overlapConvexPolygons(itm.getPolygon(),tmp.getBulletPolygon())){
							itm.reset(-Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
							blt.remove();
						}
					}
				
			}
		}
	}
	
	//Code to check player and magnet collides with each other
	public void magneticCollision(){
		PowerUp magnet = scrollHandler.getMagneticPower();
		if(Intersector.overlapConvexPolygons(magnet.getPolygon(),playerBox)){
			magnetPower = true;
			magnetBar.setMeterReading(30);
			magnet.reset(-Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		}
	}
	
	
	public void superPowerCollision(){
//		PowerUp superPower = scrollHandler.getSuperPower();
//		if(Intersector.overlapConvexPolygons(superPower.getPolygon(),playerBox)){
		if(!superPower){
			this.superPower = true;
			superPowerBar.setMeterReading(30);
		}
//			superPower.reset(-Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
//		}
	}
	
	public void onReset(){
		//Code to reset the plane,Blocks
		position.y = Gdx.graphics.getHeight();
		playerBox.setPosition(position.x, position.y);
		playerBox.setVertices(getVertices());
//		scrollHandler.startScrolling(); 
	}
	
	public void stopGame(){
		scrollHandler.stopScrolling();
		superPower = false;
		magnetPower = false;
	}
	
	public void resumeGame(){scrollHandler.startScrolling();}
	
	public Vector2 getPosition(){return position;}
	public int getWidth(){return width;}
	public int getHeight(){return height;}
	public boolean isCollisionOccured(){return isCollisionOccured;}
	public boolean isGameOverCollision(){return gameOverCollision;}
	
	public Animation getAnimation(){return animation;}
	public void setAnimation(Animation animation){this.animation = animation;}
	
	public boolean isSuperPower(){return superPower;}
	public boolean isMagnetPower(){return magnetPower;}
	public boolean canSwitchScreen(){return switchScreen;}
	
//	public GUIEntity getClockPowerEntity(){return clockPower;}
	
}
