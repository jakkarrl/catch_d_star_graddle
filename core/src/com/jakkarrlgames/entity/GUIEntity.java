package com.jakkarrlgames.entity;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.jakkarrlgames.entity.Entity;
import com.jakkarrlgames.game.catch_d_stars;
import com.jakkarrlgames.helperclass.SaveGame;
import com.jakkarrlgames.persistant.Files;
public class GUIEntity {

	public boolean DEBUG_ON = false;
	private Entity defaultGuiEntity;
	private Entity pressedGuiEntity;
	private TextureRegion pressedTexture;
	private TextureRegion defaultTexture;
	private ShapeRenderer shapeRenderer;
	private float x;
	private float y;
	private SaveGame saveGame;
	private Sound click;
	private float width;
	private float height;
	private float clickX;
	private float clickY;
	private boolean touched;
	private boolean pressed;
	private SpriteBatch batch;
	
	public GUIEntity(Texture defaultGui,Texture pressedGui) {
		this(new TextureRegion(defaultGui),new TextureRegion(pressedGui));
	}
	
	public GUIEntity(TextureRegion defaultGui,TextureRegion pressedGui){
		batch = new SpriteBatch();
		defaultGuiEntity = new Entity();
		pressedGuiEntity = new Entity();
		shapeRenderer = new ShapeRenderer();
		defaultTexture =  defaultGui;
		pressedTexture =  pressedGui;			
		defaultGuiEntity.getRegionFromTexture(defaultTexture.getTexture(), 0, 0, defaultTexture.getTexture().getWidth(), defaultTexture.getTexture().getHeight());
		pressedGuiEntity.getRegionFromTexture(pressedTexture.getTexture(), 0, 0, pressedTexture.getTexture().getWidth(), pressedTexture.getTexture().getHeight());
		click = Gdx.audio.newSound(Gdx.files.internal("data/SFX/Menu2A.wav"));
		saveGame = Files.getSaveGame();
		setPosition(0,0);
	}
	
	public GUIEntity(String pathToDefaultTexture) {
		this(new Texture(Gdx.files.internal(pathToDefaultTexture)),new Texture(Gdx.files.internal(pathToDefaultTexture)));
	}
	
	public GUIEntity(String pathToDefaultTexture,String pathToPressedTexture) {
		this(new Texture(Gdx.files.internal(pathToDefaultTexture)),new Texture(Gdx.files.internal(pathToPressedTexture)));
	}
	
	public void setPosition(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public void setSize(float width,float height) {
		this.width = width;
		this.height = height;
	}
	
	public boolean istouchDown() {
		 for(int i = 0; i<10; i++) {
			 if(Gdx.input.isTouched(i)) {
				 clickX = Gdx.input.getX(i);
				 clickY = Gdx.input.getY(i);
				 float adjustedY = Gdx.graphics.getHeight()-y;
//				 System.out.println(clickX+"-----"+clickY);
				 if( (clickX > x) && (clickX < (x+width)) &&
						 (clickY < adjustedY) && (clickY > (adjustedY-height))) {
//					 	System.out.println(x+"ssss"+clickX);
				 		return true;
				 }
			 }
		 }
		 return false;
	}
	
	public boolean isPressed() {
		 	 
		if(Gdx.input.isTouched()) {
				if(!touched) {
					
					touched = true;
					clickX = Gdx.input.getX(0);
					clickY = Gdx.input.getY(0);
					float adjustedY = Gdx.graphics.getHeight()-y;
					if( (clickX > x) && (clickX < (x+width)) &&
							(clickY < adjustedY) && (clickY > (adjustedY-height))) {
//					 		System.out.println(x+"ssss"+clickX);
							pressed = true;
					}
					return false;	
			 }
				
		}
		else if(!Gdx.input.isTouched()) {
			pressed = false;	
			if(touched) {
					touched = false;
//					System.out.println("pressed");
//					System.out.println(touched);
					float adjustedY = Gdx.graphics.getHeight()-y;
//					System.out.println(clickX+"-----"+clickY);
					if( (clickX > x) && (clickX < (x+width)) &&
						(clickY < adjustedY) && (clickY > (adjustedY-height))) {
//				 		System.out.println(x+"ssss"+clickX);
						if(saveGame.isMusicOn()) {
							if(catch_d_stars.isMusicOn)
								click.play(1.0f);
						}
						return true;
					}
				}
		}
		 return false;
	}
	
	public void render(SpriteBatch batch, OrthographicCamera cam) {

//		System.out.println(touched);
		batch.setProjectionMatrix(cam.combined);
		batch.begin();
		if(pressed) {
			batch.draw(pressedTexture,x,y,width,height);
		}
		else {
			batch.draw(defaultTexture, x, y, width, height);
		}
		batch.end();
		
		if(DEBUG_ON) {
		shapeRenderer.begin(ShapeType.Line);
		shapeRenderer.setColor(new Color(1f,0f,0f,1f));
		shapeRenderer.rect(clickX, clickY, width, height);
		shapeRenderer.end();
		}
		
	}
	
	public void setDefaultTexture(TextureRegion texture){this.defaultTexture = texture;}
	public void setPressedTexture(TextureRegion texture){this.pressedTexture = texture;}
	
	public TextureRegion getDefaultTextureRegion(){return defaultTexture;}
	public TextureRegion getPressedTextureRegion(){return pressedTexture;}
	
	public int getWidth(){return defaultTexture.getTexture().getWidth();}
	public int getHeight(){return defaultTexture.getTexture().getHeight();}
	
	
	public void dispose() {
		defaultGuiEntity.disposeEntity();
		pressedGuiEntity.disposeEntity();
		pressedTexture = null;
		defaultTexture = null;
		shapeRenderer.dispose();
		saveGame = null;
		click.dispose();
		batch.dispose();
		
	}
}
