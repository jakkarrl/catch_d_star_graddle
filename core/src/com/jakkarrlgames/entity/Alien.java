package com.jakkarrlgames.entity;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.jakkarrlgames.scroller.ScrollHandler;
import com.jakkarrlgames.scroller.UnCollectable_Item;

public class Alien extends UnCollectable_Item{

	public Alien(float x, float y, int width, int height, float scrollSpeed,
			Animation starAnimation, ScrollHandler scrollHandler) {
		super(x, y, width, height, scrollSpeed, starAnimation, scrollHandler);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void update(float delta) {
		keyFrame += delta;
		boxRectangle.setPosition(position);
		boxPolygon.setVertices(getVertices());
		
		if(!cloudPlatformCollision() && !cloudCollision()){
			if(yAcceleration.y > 325) yAcceleration.y = 325;
			position.add(yAcceleration.cpy().scl(delta));
			boxPolygon.setVertices(getVertices());
			boxRectangle.setPosition(position);
		}else{
			position.add(acceleration.cpy().scl(delta));
			position.add(acceleration.cpy().scl(delta));
			if(position.x+width < 0){
				setScrolledLeft(true);
			}
		}
	}
}
