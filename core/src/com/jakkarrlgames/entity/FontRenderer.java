package com.jakkarrlgames.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.jakkarrlgames.game.catch_d_stars;
import com.jakkarrlgames.scroller.ScrollHandler;

public class FontRenderer {
	private FreeTypeFontGenerator generator;
	private FreeTypeFontParameter parameter;
	public BitmapFont font;
	public int i=0;
	public static OrthographicCamera camera;
	public static int CAMWIDTH = 1200;
	public static int CAMHEIGHT = 768;
	public FontRenderer(String fontPath,int fontSize) {
		camera = new OrthographicCamera();
		camera.setToOrtho(false,CAMWIDTH,CAMHEIGHT);
		setFonts(fontPath,fontSize);
	}
	public void setFonts(String fontPath,int fontSize) {
		generator = new FreeTypeFontGenerator(Gdx.files.internal(fontPath));
		parameter = new FreeTypeFontParameter();
		parameter.size = fontSize;
		//parameter.color = new Color(Color.RED);
		font = generator.generateFont(parameter); // font size 12 pixels
		generator.dispose();
	}
	
	public void render(SpriteBatch batch,String string,float x,float y) {
		setCamera(batch);
		font.draw(batch, string, x, y);
	}
	public void setCamera(SpriteBatch batch) {
		batch.setProjectionMatrix(camera.combined);
	}
	public boolean renderIncrementally(int step,SpriteBatch batch, String label, int value, int x, int y) {
		setCamera(batch);
		if(i<value) {
			font.draw(batch, label + i, x, y);
			i+=step;
			return false;
		}
		else {
			font.draw(batch, label + value, x, y);
			return true;
		}
		
	}
	public void dispose(){
		parameter = null;
		font.dispose();
		camera = null;
		
	}
}
