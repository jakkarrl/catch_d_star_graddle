package com.jakkarrlgames.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Polygon;
import com.jakkarrlgames.assetloader.AssetLoader;
import com.jakkarrlgames.scroller.UnCollectable_Item;
import com.jakkarrlgames.scroller.ScrollHandler;

public class Spider extends UnCollectable_Item {

	private float targetFallPosition;
	private Polygon webPolygon;
	private float yPos;
	public Spider(float x, float y, int width, int height, float scrollSpeed,
			Animation starAnimation, ScrollHandler scrollHandler,float targetFallPosition) {
		super(x, y, width, height, scrollSpeed, starAnimation, scrollHandler);
		this.targetFallPosition = targetFallPosition;
		yPos = Gdx.graphics.getHeight();
		webPolygon = new Polygon(new float[]{(position.x+ getWidth()/2),(position.y + getHeight()/2),
											(position.x +getWidth()/2) + 3 ,(position.y + getHeight()/2),
											(position.x +getWidth()/2) + 3,yPos,
											(position.x+ getWidth()/2),yPos});
	}
	
	@Override
	public void update(float delta) {
		keyFrame += delta;
		
		boxRectangle.setPosition(position);
		boxPolygon.setVertices(getVertices());
		webPolygon.setVertices(getWebVertices());
		
		//Code to make the spider comes down until some random Point
		if(targetFallPosition != 0){
			if(position.y > targetFallPosition){
				if(yAcceleration.y > 325) yAcceleration.y = 325;
				position.add(yAcceleration.cpy().scl(delta));
			}else{
				position.add(acceleration.cpy().scl(delta));
				if(position.x+width < 0){
					setScrolledLeft(true);
				}
			}
		}
			
		//Code after Spider web gets cut
		if(targetFallPosition == 0){
			if(!cloudPlatformCollision() && !cloudCollision()){
				if(yAcceleration.y > 325) yAcceleration.y = 325;
				position.add(yAcceleration.cpy().scl(delta));
			}else{
				position.add(acceleration.cpy().scl(delta));
				position.add(acceleration.cpy().scl(delta));
				if(position.x+width < 0){
					setScrolledLeft(true);
				}
			}
		}
		
		 
	}
	
	private float[] getWebVertices() {
		return new float[]{(getX()+ getWidth()/2),(getY() + getHeight()/2),
				(getX() +getWidth()/2) + 3 ,(getY() + getHeight()/2),
				(getX() +getWidth()/2) + 3,yPos,
				(getX()+ getWidth()/2),yPos};

	}
	
	public void reset(float newX, float newY,float targetFallPosition) {
		// TODO Auto-generated method stub
		super.reset(newX, newY);
		this.targetFallPosition = targetFallPosition;
		
	}
	
	@Override
	public void render(SpriteBatch batch) {
		// TODO Auto-generated method stub
		super.render(batch);
		if(targetFallPosition!= 0)
			batch.draw(AssetLoader.webAnimation.getKeyFrame(keyFrame), (position.x +(getWidth())/2) - 5,(position.y + getHeight()/2),5,(Gdx.graphics.getHeight() - targetFallPosition));
	}
	
	public Polygon getWebPolygon(){return webPolygon;}
	public float getTargetFallPosition(){return targetFallPosition;}
	public void setTargetFallPosition(float targetFallPosition){this.targetFallPosition = targetFallPosition;} 

}
