package com.jakkarrlgames.persistant;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Json;
import com.jakkarrlgames.assetloader.AssetLoader;
import com.jakkarrlgames.game.catch_d_stars;
import com.jakkarrlgames.helperclass.SaveGame;
import com.jakkarrlgames.store.Item;
import com.jakkarrlgames.store.StoreAssets;

public class Files {

	public static SaveGame getSaveGame(){
		FileHandle file = Gdx.files.local(("bin/sampleJson.json"));
		Json json = new Json();
		SaveGame saveGame = null;
		if(!file.exists()){
			saveGame = new SaveGame(0,0,0,0,0,0,"Boy",0,0,true);
			file.writeString(json.toJson(saveGame),false);
		}else{
			
			saveGame = json.fromJson(SaveGame.class, file);
		}
		return saveGame;
	}
	
	public static SaveGame getSaveGame(String str){
		FileHandle file = Gdx.files.local(("bin/sampleJson.json"));
		Json json = new Json();
		SaveGame saveGame = null;
		if(!file.exists()){
			saveGame = new SaveGame(0,0,0,0,0,0,"Boy",0,0,true);
			file.writeString(json.toJson(saveGame),false);
			System.out.println("Not exist part");
		}else{
			System.out.println("file found value returned");
			saveGame = json.fromJson(SaveGame.class, file);
		}
		return saveGame;
	}
	
	public static void saveGameStatus(SaveGame saveGame){
		Json json = new Json();
		FileHandle file = Gdx.files.local(("bin/sampleJson.json"));
		file.writeString(json.toJson(saveGame), false);
	}
	
	public static Item getPowerUp1(){
		FileHandle file = Gdx.files.local("bin/data/jsonFiles/Powerups/powerUp1.json");
		Json json = new Json();
		Item item = null;
		if(!file.exists()){
			item = new Item("PowerUp1","Use this power to escape from enemies",AssetLoader.storeSuperPower,false,"Star",800);
			file.writeString(json.toJson(item),false);
		}else{
			item = json.fromJson(Item.class, file);
		}
		return item; 
	}
	
	public static Item getPowerUp2(){
		FileHandle file = Gdx.files.local("bin/data/jsonFiles/Powerups/powerUp2.json");
		Json json = new Json();
		Item item = null;
		if(!file.exists()){
			item = new Item("PowerUp2","Key to make you alive after death",AssetLoader.clockPower,false,"Candy",1000);
			file.writeString(json.toJson(item),false);
		}else{
			item = json.fromJson(Item.class, file);
		}
		return item; 
	}
	
//	public static Item getPowerUp2(){
//		FileHandle file = Gdx.files.local("bin/data/jsonFiles/Powerups/powerUp2.json");
//		Json json = new Json();
//		Item item = null;
//		if(!file.exists()){
//			item = new Item("PowerUp2",StoreAssets.powerUp2,true,"Star",100);
//			file.writeString(json.toJson(item),false);
//		}else{
//			item = json.fromJson(Item.class, file);
//		}
//		return item; 
//	}
	
	public static Item getCowBoy(){
		FileHandle file = Gdx.files.local("bin/data/jsonFiles/Characters/cowBoy.json");
		Json json = new Json();
		Item item = null;
		if(!file.exists()){
			item = new Item("Boy","cowBoy",AssetLoader.storeBoy,false,"",0);
			file.writeString(json.toJson(item),false);
		}else{
			item = json.fromJson(Item.class, file);
		}
		return item; 
	}
	
	public static Item getNinjaGirl(){
		FileHandle file = Gdx.files.local("bin/data/jsonFiles/Characters/ninjaGirl.json");
		Json json = new Json();
		Item item = null;
		if(!file.exists()){
			item = new Item("Girl","ninjaGirl",AssetLoader.storeGirl,true,"Coin",500);
			file.writeString(json.toJson(item),false);
		}else{
			item = json.fromJson(Item.class, file);
		}
		return item; 
	}
	
	public static Skin getSkin(){
		FileHandle file = Gdx.files.local("bin/data/jsonFiles/skin/uiskin.json");
		Json json = new Json();
		Skin skin = null;
		if(file.exists()){
			skin = json.fromJson(Skin.class, file);
		}
		return skin;
		
	}
	
	public static FileHandle getFileOf(String item){
		FileHandle file = null;
		if(item.toLowerCase().equals("powerup1"))
			file = Gdx.files.local("bin/data/jsonFiles/Powerups/powerUp1.json");
		else if(item.toLowerCase().equals("powerup2"))
			file = Gdx.files.local("bin/data/jsonFiles/Powerups/powerUp2.json");
		else if(item.toLowerCase().equals("boy"))
			file = Gdx.files.local("bin/data/jsonFiles/Characters/cowBoy.json");
		else if(item.toLowerCase().equals("girl"))
			file = Gdx.files.local("bin/data/jsonFiles/Characters/ninjaGirl.json");
		return file;
	}
	
	public static boolean isFirstTimePlay(){
		FileHandle file = Gdx.files.local(("bin/firstTimePlay.json"));
		Json json = new Json();
		if(file.exists()){
			return false;
		}else{
			file.write(true);
			return true;
		}
	}
}
