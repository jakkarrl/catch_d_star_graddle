package com.jakkarrlgames.helperclass;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.jakkarrlgames.assetloader.AssetLoader;
import com.jakkarrlgames.entity.Entity;
import com.jakkarrlgames.entity.FontRenderer;
import com.jakkarrlgames.entity.GUIEntity;
import com.jakkarrlgames.entity.Player;
import com.jakkarrlgames.game.catch_d_stars;
import com.jakkarrlgames.scroller.ScrollHandler;

public class ObjectInjector {
	
	private OrthographicCamera cam;
	private Entity backEntity,faderEntity;
	private ScrollHandler scrollHandler;
	private Skin skin;
	private Stage stage;
	private SpriteBatch batch;
	private GUIEntity playNow,help,music,store,exit,shoot,pause,gamePlayExit;//,tap,clock,itemLeftTouch,itemRightTouch,categoryLeftTouch,categoryRightTouch,goBack,select;
	private FontRenderer fontRenderer,fontArray[];
	private Player player;
	private ClockCount clockCount,magnetBar,superPowerBar;
	private ShapeRenderer lineShape;
	
	public ShapeRenderer getLineShape() {
		return lineShape;
	}
	public ObjectInjector(catch_d_stars catchdstars){
	cam = new OrthographicCamera();
	backEntity = new Entity();
	scrollHandler = new ScrollHandler(Gdx.graphics.getHeight());
	faderEntity = new Entity();
	skin = new Skin(Gdx.files.internal("data/skin/uiskin.json"));
	stage = new Stage(new FitViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
	batch = new SpriteBatch();
		
	playNow = new GUIEntity(AssetLoader.playNowText,AssetLoader.playNowPressedText);
	help = new GUIEntity(AssetLoader.help,AssetLoader.helpPressed);
	music = new GUIEntity(AssetLoader.musicEnabled,AssetLoader.musicPressed);
	store = new GUIEntity(AssetLoader.storeText,AssetLoader.storePressedText);
	exit = new GUIEntity(AssetLoader.exitText,AssetLoader.exitPressedText);
	shoot = new GUIEntity(AssetLoader.shoot,AssetLoader.shootPressed);
	pause = new GUIEntity(AssetLoader.pause,AssetLoader.pausePressed);
	gamePlayExit  = new GUIEntity(AssetLoader.back,AssetLoader.backPressed);
//	tap = new GUIEntity(AssetLoader.tapToContinue,AssetLoader.tapToContinuePressed);
//	clock = new GUIEntity(AssetLoader.clockPowerGUI,AssetLoader.clockPowerGUIPressed);
	
	fontRenderer= new FontRenderer("data/font/kenvector_future_thin.ttf",30);
	fontArray = new FontRenderer[8];
	fontArray[0] =  new FontRenderer("data/font/SF Slapstick Comic.ttf",30);
	fontArray[1] =  new FontRenderer("data/font/SF Slapstick Comic.ttf",30);
	fontArray[2] =  new FontRenderer("data/font/SF Slapstick Comic.ttf",30);
	fontArray[3] =  new FontRenderer("data/font/SF Slapstick Comic.ttf",30);
	fontArray[4] =  new FontRenderer("data/font/SF Slapstick Comic.ttf",30);
	fontArray[5] =  new FontRenderer("data/font/SF Slapstick Comic.ttf",30);
	fontArray[6] =  new FontRenderer("data/font/SF Slapstick Comic.ttf",30);
	fontArray[7] =  new FontRenderer("data/font/SF Slapstick Comic.ttf",30);
	for(int i = 0;i <8;i++) {
		fontArray[i].font.setColor(Color.BLACK);
	}
	
	clockCount = new ClockCount(100,fontRenderer, FontRenderer.CAMWIDTH-70, FontRenderer.CAMHEIGHT - 90,"clockCount");
	magnetBar = new ClockCount(30, fontRenderer, FontRenderer.CAMWIDTH-490, FontRenderer.CAMHEIGHT - 90,"magnetCount");
	superPowerBar = new ClockCount(30, fontRenderer, FontRenderer.CAMWIDTH-980, FontRenderer.CAMHEIGHT - 90,"superPowerCount");
	
	
	player = new Player(Gdx.graphics.getWidth()/10,Gdx.graphics.getHeight() + 100,45,60,scrollHandler,clockCount,magnetBar,superPowerBar,catchdstars,cam,AssetLoader.gamePlayMusic);
	
	lineShape = new ShapeRenderer();

	}
	public Player getPlayer() {
		return player;
	}
	
	public ClockCount getClockCount() {
		return clockCount;
	}
	public ClockCount getMagnetBar() {
		return magnetBar;
	}
	public ClockCount getSuperPowerBar() {
		return superPowerBar;
	}
	public OrthographicCamera getCam() {
		return cam;
	}
	public Entity getBackEntity() {
		return backEntity;
	}
	public Entity getFaderEntity() {
		return faderEntity;
	}
	public ScrollHandler getScrollHandler() {
		return scrollHandler;
	}
	
	public Skin getSkin() {
		return skin;
	}
	public Stage getStage() {
		return stage;
	}
	public GUIEntity getShoot() {
		return shoot;
	}
	public GUIEntity getPause() {
		return pause;
	}
	public GUIEntity getGamePlayExit() {
		return gamePlayExit;
	}
	public SpriteBatch getBatch() {
		return batch;
	}
	public GUIEntity getPlayNow() {
		return playNow;
	}
	public GUIEntity getHelp() {
		return help;
	}
	public GUIEntity getMusic() {
		return music;
	}
	public GUIEntity getStore() {
		return store;
	}
	public GUIEntity getExit() {
		return exit;
	}
	public FontRenderer getFontRenderer() {
		return fontRenderer;
	}
//	public GUIEntity getTap() {
//		return tap;
//	}
//	public GUIEntity getClock() {
//		return clock;
//	}
//	public GUIEntity getItemLeftTouch() {
//		return itemLeftTouch;
//	}
//	public GUIEntity getItemRightTouch() {
//		return itemRightTouch;
//	}
//	public GUIEntity getCategoryLeftTouch() {
//		return categoryLeftTouch;
//	}
//	public GUIEntity getCategoryRightTouch() {
//		return categoryRightTouch;
//	}
//	public GUIEntity getGoBack() {
//		return goBack;
//	}
//	public GUIEntity getSelect() {
//		return select;
//	}
	
	
	public FontRenderer[] getFontArray(){return fontArray;}
	
	
}
