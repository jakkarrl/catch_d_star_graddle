package com.jakkarrlgames.helperclass;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.jakkarrlgames.scroller.UnCollectable_Item;

public class UnCollectable_Item_Timer {
	private UnCollectable_Item item;
	private boolean reset,stop;
	private float sec,CONST_SEC;
	private Random random;
	public UnCollectable_Item_Timer(UnCollectable_Item item,float sec){
		this.item = item;
		reset = false;
		this.CONST_SEC = sec;
		this.sec = CONST_SEC;
		random = new Random();
		stop = false;
	}
	
	
	public void update(float delta) {
//		while(!isStop){
//			if(alarm.isScrolledLeft()){
		if(!stop){
			if(sec >= 0){
				sec -= delta;
			}else{
//				item.reset(random.nextInt(Gdx.graphics.getWidth()) + 50,Gdx.graphics.getHeight());
				reset = true;
				sec = CONST_SEC;
			}
		}
				
//			}
//		}
	}
	
	public boolean isReset(){return reset;}
	public void setReset(boolean reset){ this.reset = reset;}
	public float getSec(){return sec;}
	public void setSec(float sec){this.sec = sec;}
	public void stop(){stop = true;}
	public void start(){stop = false;}
}
