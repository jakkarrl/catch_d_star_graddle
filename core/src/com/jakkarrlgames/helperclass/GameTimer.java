package com.jakkarrlgames.helperclass;

public class GameTimer {
	private float sec,min,hour;
	private boolean stop;
	public GameTimer(){
		sec=0; min = 0; hour = 0;
		stop = false;
	}
	
	public void update(float delta){
		if(!stop){
			sec += delta;
			if(sec >= 59){
				min++;
				sec = 0;
				if(min >=59){
					hour++;
					min = 0; sec=0;
				}
			}
		}
	}
	
	public void resetTimer(){sec = 0;min =0;hour =0;stop =false;}
	public float getSec(){return sec;}
	public float getMin(){return min;}
	public float getHour(){return hour;}
	public void stop(){stop = true;}
	public void start(){stop = false;}
}

