package com.jakkarrlgames.helperclass;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.jakkarrlgames.entity.FontRenderer;

public class ClockCount{

	private float meterReading;//,decrementCounter;
	private boolean isStop;
	private FontRenderer fontRenderer;
	private float elapsedTime = 0;
	private String timerName;
	private float timerX,timerY;
	
	public ClockCount(float meterReading,FontRenderer fontRenderer,float timerX,float timerY,String timerName){
		this.meterReading = meterReading;
//		this.decrementCounter = decrementCounter;
		isStop = false;
		this.fontRenderer = fontRenderer;
		fontRenderer.font.setScale(1.5f, 1.5f);
		fontRenderer.font.setColor(Color.RED);
		this.timerX = timerX;this.timerY = timerY;
		this.timerName = timerName;
	}
	
	public void resetToOld(float meterReading){
		this.meterReading = meterReading;
		isStop = false;
		elapsedTime = 0;
	}
	
	public void update(float delta) {
//		while(!isStop){
//			meterReading -= (delta* 2);
		if(!isStop){
			elapsedTime += delta;
//			System.out.println("ElapsedTime = " + elapsedTime);
			if(elapsedTime >1) {
				meterReading--;
				elapsedTime = 0;
			}
		}
//		}
	}
	
	public void render(SpriteBatch batch){
		if(meterReading > 60) fontRenderer.font.setColor(Color.GREEN);
		else if(meterReading > 20 && meterReading < 60) fontRenderer.font.setColor(Color.YELLOW);
		else if(meterReading < 20) fontRenderer.font.setColor(Color.RED);
		fontRenderer.render(batch, "" + (int)meterReading,timerX,timerY);
		if(meterReading <= 10)
			fontRenderer.render(batch," "+ (int)meterReading, FontRenderer.CAMWIDTH/3, FontRenderer.CAMHEIGHT/2);
		
	}
	
	public void setMeterReading(float meterReading){this.meterReading = meterReading;}
	public float getMeterReading(){return meterReading;}
	public void setStop(boolean stop){this.isStop = stop;}
	public boolean isStop(){return isStop;}
	public float getTimerX(){return timerX;}
	public float getTimerY(){return timerY;}
}
