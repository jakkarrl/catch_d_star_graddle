package com.jakkarrlgames.helperclass;

public class SaveGame {
	private int score,star,candy,coin,cupCake,fruit,superPowerCount,clockPowerCount;
	private boolean music;
	private String character;
	
	public SaveGame(){}
	
	public SaveGame(int score,int star,int candy,int coin,int cupcake,int fruit,String character,int superPowerCount,int clockPowerCount,boolean music){
		this.score = score;
		this.star = star;
		this.candy = candy;
		this.coin = coin;
		this.cupCake = cupcake;
		this.fruit = fruit;
		this.character = character;
		this.superPowerCount = superPowerCount;
		this.clockPowerCount = clockPowerCount;
		this.music = music;
	}
	
	public void setScore(int score){this.score = score;}
	public int getScore(){return score;}
	
	public void setStar(int star){this.star = star;}
	public int getStar(){return star;}
	
	public void setCandy(int candy){this.candy = candy;}
	public int getCandy(){return candy;}
	
	public void setCoin(int coin){this.coin = coin;}
	public int getCoin(){return coin;}
	
	public void setCupCake(int cupCake){this.cupCake = cupCake;}
	public int getCupCake(){return cupCake;}
	
	public void setFruit(int fruit){this.fruit = fruit;}
	public int getFruit(){return fruit;}
	
	public void setCharacter(String character){this.character = character;}
	public String getCharacter(){return character;}
	
	public int getValueOf(String item){
		if(item.toLowerCase().equals("star"))
			return star;
		else if(item.toLowerCase().equals("candy"))
			return candy;
		else if(item.toLowerCase().equals("coin"))
			return coin;
		else if(item.toLowerCase().equals("cupcake"))
			return cupCake;
		else if(item.toLowerCase().equals("fruit"))
			return fruit;
		else return 0;
					
	}
	
	public void setValueOf(String item,int amount){
		if(item.toLowerCase().equals("star"))
			this.star = amount;
		else if(item.toLowerCase().equals("candy"))
			this.candy = amount;
		else if(item.toLowerCase().equals("coin"))
			this.coin = amount;
		else if(item.toLowerCase().equals("cupcake"))
			this.cupCake = amount;
		else if(item.toLowerCase().equals("fruit"))
			this.fruit = amount;
		else System.err.println("SaveGame::setValueOf(String,int) : No such kinda item is found");;
	}
	
	public int getSuperPowerCount(){return superPowerCount;}
	public void setSuperPowerCount(int count){this.superPowerCount = count;}
	
	public int getClockPowerCount(){return clockPowerCount;}
	public void setClockPowerCount(int count){this.clockPowerCount = count;}
	
	public boolean isMusicOn(){return music;}
	public void setMusicOn(boolean music){this.music = music;}
}
