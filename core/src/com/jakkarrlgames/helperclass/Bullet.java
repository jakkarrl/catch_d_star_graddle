package com.jakkarrlgames.helperclass;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.jakkarrlgames.assetloader.AssetLoader;

public class Bullet {
	private Vector2 position;
	private Polygon bulletPolygon;
	private int width,height;
	private ShapeRenderer shape;
	
	public Bullet(float x,float y,int width,int height){
		position = new Vector2(x,y);
		bulletPolygon = new Polygon(new float[]{
				x,y,
				x+width,y,
				x+width,y+height,
				x,y+height
		});
		this.width = width;this.height = height;
		shape = new ShapeRenderer();
	}
	
	public void update(float delta){
		position.x += 300*delta;
		bulletPolygon.setVertices(getVetrices());
	}
	
	public float[] getVetrices(){
		return new float[]{position.x,position.y,
			position.x+width,position.y,
			position.x+width,position.y+height,
			position.x,position.y+height};}
	
	public void render(SpriteBatch batch){
		batch.draw(AssetLoader.bullet,position.x,position.y,width,height);
		batch.end();
		shape.begin(ShapeType.Line);
		shape.polygon(getVetrices());
		shape.end();
		batch.begin();
		
	}
	
	public Vector2 getPosition(){return position;}
	public Polygon getBulletPolygon(){return bulletPolygon;}

}
