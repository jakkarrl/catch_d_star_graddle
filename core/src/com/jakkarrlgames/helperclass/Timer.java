package com.jakkarrlgames.helperclass;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.jakkarrlgames.entity.FontRenderer;
import com.jakkarrlgames.scroller.Alarm;
import com.jakkarrlgames.scroller.Spawnable;

public class Timer {
	
	private Spawnable alarm;
//	private boolean isStop;
	private float sec,delayTime;
	private Random random;
	private boolean stop;
	public Timer(Spawnable alarm,float delayTime){
		this.alarm = alarm;
//		isStop = false;
		this.delayTime = delayTime;
		sec = delayTime;
		random = new Random();
		stop = false;
	}
	
	
	public void update(float delta) {
//		while(!isStop){
//			if(alarm.isScrolledLeft()){
		if(!stop){
			if(sec >= 0){
				sec -= delta;
			}else{
				alarm.reset(random.nextInt(50) + 500,Gdx.graphics.getHeight());
				sec = delayTime;
			}			
		}

//			}
//		}
	}
	
	public void render(SpriteBatch batch){
//		fontRenderer.render(batch, ""+sec, 100, 200);
	}
	
	public float getSec(){return sec;}
	public void setSec(float sec){this.sec = sec;}
	public void stop(){stop = true;}
	public void start(){stop = false;}
	
	public void resetTimer(float delayTime){this.delayTime = delayTime;sec = delayTime;stop = false;}
}
