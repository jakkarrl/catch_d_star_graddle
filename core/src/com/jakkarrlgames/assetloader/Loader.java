package com.jakkarrlgames.assetloader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.jakkarrlgames.game.catch_d_stars;

public class Loader {

	public Loader() {
		
		
		catch_d_stars.manager.load("data/images/background/dayBackGround.png", Texture.class);
		catch_d_stars.manager.load("data/images/background/blur.png",Texture.class);
		catch_d_stars.manager.load("data/images/background/greenPlatform.png",Texture.class);
		catch_d_stars.manager.load("data/images/background/supportPlatform.png",Texture.class);
		catch_d_stars.manager.load("data/images/background/fader.png",Texture.class);
		catch_d_stars.manager.load("data/images/background/storeBack.png",Texture.class);
		catch_d_stars.manager.load("data/images/background/PromptBG.png",Texture.class);
		catch_d_stars.manager.load("data/images/background/pauseBG.png",Texture.class);
		catch_d_stars.manager.load("data/images/background/highScore.png",Texture.class);
		catch_d_stars.manager.load("data/images/background/DashBoard.png",Texture.class);
		
		catch_d_stars.manager.load("data/images/CharacterSpriteSheet/NinjaGirl/ninjaGirl.png",Texture.class);
		catch_d_stars.manager.load("data/images/CharacterSpriteSheet/CowBoy/cowBoy.png",Texture.class);
		
		catch_d_stars.manager.load("data/images/items/item.png",Texture.class);
		
//		catch_d_stars.manager.load("data/images/store/store.png",Texture.class);
		catch_d_stars.manager.load("data/images/UI/UI.png",Texture.class);
		catch_d_stars.manager.load("data/images/UI/UI1.png",Texture.class);
		catch_d_stars.manager.load("data/images/UI/letters.png",Texture.class);
		catch_d_stars.manager.load("data/images/UI/menuUI.png",Texture.class);
		catch_d_stars.manager.load("data/images/store/store1.png", Texture.class);
		catch_d_stars.manager.load("data/images/store/clock.png", Texture.class);
		
		catch_d_stars.manager.load("data/music/Infinite Strings.mp3", Music.class);
		catch_d_stars.manager.load("data/music/Oranges melodies.mp3",Music.class);
	}

}
