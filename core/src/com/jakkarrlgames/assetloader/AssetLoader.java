package com.jakkarrlgames.assetloader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.jakkarrlgames.game.catch_d_stars;

public class AssetLoader {

	private static Texture items,store1,UI,UI1,menuUI,DashBoard;
	private static TextureRegion spider1,spider2,spider3,spider4,spider5,spider6;
	private static TextureRegion alien1,alien2,alien3,alien4,alien5,alien6;
	private static TextureRegion shield1,shield2,shield3,shield4,shield5,shield6;
	private static TextureRegion coin1,coin2,coin3,coin4,coin5,coin6;
	private static Texture letter;
	public static Texture[] helpScreen;
	
	
	public static Texture promptBG,background,highScore,pauseBG,blur,fader,supportPlatform,basicPlatform,storeBackground;
	public static TextureRegion bullet,clockPower;
	public static TextureRegion storeBoy,storeGirl,storeSuperPower,storeClock,help,helpPressed,magnetDashBoard, superPowerDashBoard, stopClockDashBoard;
	public static TextureRegion playNowText,playNowPressedText,storeText,storePressedText,exitText,exitPressedText,powerupText,characterText;
	public static TextureRegion buy,left,right,lock,select,back,jump,shoot,pause,buyPressed,leftPressed,rightPressed,lockPressed,selectPressed,backPressed,jumpPressed,shootPressed,pausePressed,clockPowerGUI,clockPowerGUIPressed,tapToContinue,tapToContinuePressed;
	public static TextureRegion musicEnabled,musicPressed,musicDisabled;
	public static Animation starAnimation,candyAnimation,cupCakeAnimation,cometAnimation,spiderAnimation,webAnimation,alienAnimation,
							fruitAnimation,coinAnimation;
	public static Music gamePlayMusic,mainMenuMusic;
	public static Animation alarmAnimation,magnetAnimation,shieldAnimation;
	public static Animation boyRunAnimation,boyJumpAnimation,boyDeadAnimation,girlRunAnimation,girlJumpAnimation,girlDeadAnimation;
	public static TextureRegion A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z;
	
	public static void load(String character){
//		System.out.println("In Load " + character);
		background = catch_d_stars.manager.get("data/images/background/dayBackGround.png");
		blur = catch_d_stars.manager.get("data/images/background/blur.png");
		basicPlatform = catch_d_stars.manager.get("data/images/background/greenPlatform.png");
		supportPlatform = catch_d_stars.manager.get("data/images/background/supportPlatform.png");
		fader = catch_d_stars.manager.get("data/images/background/fader.png");
		storeBackground = catch_d_stars.manager.get("data/images/background/storeBack.png");
		promptBG = catch_d_stars.manager.get("data/images/background/PromptBG.png");
		pauseBG = catch_d_stars.manager.get("data/images/background/pauseBG.png");
		highScore =catch_d_stars.manager.get("data/images/background/highScore.png");
		items = catch_d_stars.manager.get("data/images/items/item.png");
		store1 = catch_d_stars.manager.get("data/images/store/store1.png");
		UI = catch_d_stars.manager.get("data/images/UI/UI.png");
		menuUI = catch_d_stars.manager.get("data/images/UI/menuUI.png");
		UI1 =  catch_d_stars.manager.get("data/images/UI/UI1.png");
		
		gamePlayMusic = catch_d_stars.manager.get("data/music/Infinite Strings.mp3");
		mainMenuMusic = catch_d_stars.manager.get("data/music/Oranges melodies.mp3");
		
		DashBoard =catch_d_stars.manager.get("data/images/background/DashBoard.png");
//		 = new TextureRegion(DashBoard,0,0,242,113);
		stopClockDashBoard = new TextureRegion(DashBoard,100,114,84,102);
		magnetDashBoard = new TextureRegion(DashBoard,185,114,77,83);
		superPowerDashBoard = new TextureRegion(DashBoard,0,114,99,101);

		
		//
		helpScreen = new Texture[7];
		for(int i=0;i<7;i++){
			helpScreen[i] = new Texture(Gdx.files.internal("data/images/help/hint"+(i+1)+".png"));
		}
		
		//
		
		
		
		//clockPower
		clockPower = new TextureRegion((Texture)catch_d_stars.manager.get("data/images/store/clock.png"));
		
		//Shield Code
		shield1 = new TextureRegion(items,0,0,102,106);
		shield2 = new TextureRegion(items,103,0,102,106);
		shield3 = new TextureRegion(items,0,107,102,106);
		shield4 = new TextureRegion(items,103,107,102,106);
		shield5 = new TextureRegion(items,206,0,102,106);
		shield6 = new TextureRegion(items,309,0,102,106);
		shieldAnimation = new Animation(0.08f, new  TextureRegion[]{shield1,shield2,shield3,shield4,shield5,shield6});
		shieldAnimation.setPlayMode(PlayMode.LOOP);
		
		//Load Alarm
		alarmAnimation = new Animation(0.10f,new TextureRegion(items,100,214,95,118));
		alarmAnimation.setPlayMode(PlayMode.LOOP_PINGPONG);
		
		
		//Load Magnet Powerup
		magnetAnimation =  new Animation(0.10f,new TextureRegion(items,219,402,44,48));
		magnetAnimation.setPlayMode(PlayMode.LOOP);
		
			
		//Load Star
		starAnimation = new Animation(0.10f, new TextureRegion(items, 0,389,88,88));
		starAnimation.setPlayMode(PlayMode.LOOP_PINGPONG);
		
		//SpaceShip Load
		fruitAnimation = new Animation(0.10f, new TextureRegion(items,147,461,38,32));
		fruitAnimation.setPlayMode(PlayMode.LOOP_PINGPONG);
		
		//CoinLoad
		
		coin1 = new TextureRegion(items,161,333,40,40);
		coin2 = new TextureRegion(items,205,451,40,40);
		coin3 = new TextureRegion(items,246,451,40,40);
		coin4 = new TextureRegion(items,287,452,40,40);
		coin5 = new TextureRegion(items,328,452,40,40);
		coin6 = new TextureRegion(items,369,452,40,40);
		coinAnimation = new Animation(0.10f, new TextureRegion[]{coin1,coin2,coin3,coin4,coin5,coin6});
		coinAnimation.setPlayMode(PlayMode.LOOP_REVERSED);
		
		//Candy Load
		candyAnimation =new Animation(0.10f,new TextureRegion(items,100,333,60,100));
		candyAnimation.setPlayMode(PlayMode.LOOP);
				
		//CupCakeLoad
		
		cupCakeAnimation = new Animation(0.10f, new TextureRegion(items,412,0,75,100));
		cupCakeAnimation.setPlayMode(PlayMode.LOOP_PINGPONG);
	
		//Load Spider
		spider1 = new TextureRegion(items,89,434,57,26);
		spider2 = new TextureRegion(items,89,461,57,26);
		spider3 = new TextureRegion(items,147,434,57,26);
		spider4 = new TextureRegion(items,161,375,57,26);
		spider5 = new TextureRegion(items,161,402,57,26);
		spider6 = new TextureRegion(items,219,375,57,26);
		spiderAnimation = new Animation(0.10f,new TextureRegion[]{spider1,spider2,spider3,spider4,spider5,spider6});
		spiderAnimation.setPlayMode(PlayMode.LOOP_PINGPONG);
		
		webAnimation = new Animation(0.10f,new TextureRegion(items,196,214,5,58));
		webAnimation.setPlayMode(PlayMode.LOOP_PINGPONG);
		
		//alien Load
		alien1 = new TextureRegion(items,0,214,99,174);
		alien2 = new TextureRegion(items,300,281,89,170);
		alien3 = new TextureRegion(items,206,200,93,174);
		alien4 = new TextureRegion(items,390,281,89,170);
		alien5 = new TextureRegion(items,307,107,91,173);
		alien6 = new TextureRegion(items,399,107,90,173);
		alienAnimation = new Animation(0.10f,new TextureRegion[]{alien1,alien2,alien3,alien4,alien5,alien6});
		alienAnimation.setPlayMode(PlayMode.LOOP_PINGPONG);
		
		//Load Comet
		cometAnimation = new Animation(0.10f,new TextureRegion(items,206,107,100,92));
		cometAnimation.setPlayMode(PlayMode.LOOP_PINGPONG);
		//Bullet Code
		bullet = new TextureRegion(items,264,402,24,25);
		
		
		//---Store---//
		storeBoy = new TextureRegion(store1,270,0,231,231);
		storeGirl = new TextureRegion(store1,0,270,231,231);
		storeSuperPower = new TextureRegion(store1,0,0,231,231);
		storeClock = new TextureRegion(store1,273,270,231,231);
		//--UI--//
		playNowText = new TextureRegion			(menuUI,000,000,375,165);
		playNowPressedText = new TextureRegion	(menuUI,425,000,375,165);
		storeText = new TextureRegion			(menuUI,000,200,375,193);
		storePressedText = new TextureRegion	(menuUI,425,200,375,193);
		exitText = new TextureRegion			(menuUI,000,400,375,193);
		exitPressedText = new TextureRegion		(menuUI,425,400,375,193);
		powerupText = new TextureRegion			(UI,0,910,408,180);
		characterText = new TextureRegion		(UI,410,915,408,180);
		buy  =  new TextureRegion(UI,000,670,180,180);
		left =  new TextureRegion(UI,000,205,180,180);
		right=  new TextureRegion(UI,000,000,180,180);
		lock =  new TextureRegion(UI,000,1122,180,180);
		select= new TextureRegion(UI,430,210,180,180);
		back=   new TextureRegion(UI,420,000,180,180);
		jump=   new TextureRegion(UI,430,670,180,180);
		help =  new TextureRegion(UI,000,436,180,180);
		shoot = new TextureRegion(UI,216,1128,180,180);
		pause = new TextureRegion(UI,620,1130,180,180);
		buyPressed   = new TextureRegion(UI,215,670,180,180);
		leftPressed  = new TextureRegion(UI,215,205,180,180);
		rightPressed = new TextureRegion(UI,215,000,180,180);
		selectPressed= new TextureRegion(UI,625,210,180,180);
		backPressed  = new TextureRegion(UI,625,000,180,180);
		jumpPressed  = new TextureRegion(UI,625,670,180,180);
		helpPressed =  new TextureRegion(UI,215,436,180,180);
		shootPressed = new TextureRegion(UI,425,1128,180,180);
		pausePressed = new TextureRegion(UI,625,1365,180,180);
		tapToContinue  		 = 	new TextureRegion(UI1,000,000,223,97);
		clockPowerGUI = 	new TextureRegion(UI1,279,000,223,97);
		tapToContinuePressed = 	new TextureRegion(UI1,000,105,223,97);
		clockPowerGUIPressed = 	new TextureRegion(UI1,279,105,223,97);
		musicEnabled  = new TextureRegion(UI,000,1365,180,180);
		musicDisabled = new TextureRegion(UI,417,1365,180,180);
		musicPressed = new TextureRegion(UI,213,1365,180,180);
		//Letters
		letter = catch_d_stars.manager.get("data/images/UI/letters.png");
		A = new TextureRegion(letter, 144,0,61,64);
		B = new TextureRegion(letter, 104,210,50,66);
		C = new TextureRegion(letter, 237,0,52,66);
		D = new TextureRegion(letter, 182,65,54,66);
		E = new TextureRegion(letter, 103,277,45,64);
		F = new TextureRegion(letter, 155,264,44,64);
		G = new TextureRegion(letter, 237,67,52,66);
		H = new TextureRegion(letter, 52,210,51,64);
		I = new TextureRegion(letter, 206,0,22,64);
		J = new TextureRegion(letter, 247,199,42,66);
		K = new TextureRegion(letter, 123,130,53,64);
		L = new TextureRegion(letter, 202,199,44,64);
		M = new TextureRegion(letter, 77,0,66,64);
		N = new TextureRegion(letter, 177,132,53,64);
		O = new TextureRegion(letter, 0,130,60,66);
		P = new TextureRegion(letter, 0,330,48,65);
		Q = new TextureRegion(letter, 62,65,60,79);
		R = new TextureRegion(letter, 0,264,51,65);
		S = new TextureRegion(letter, 155,197,46,66);
		T = new TextureRegion(letter, 231,134,52,64);
		U = new TextureRegion(letter, 0,197,51,66);
		V = new TextureRegion(letter, 0,65,61,64);
		W = new TextureRegion(letter, 0,0,76,64);
		X = new TextureRegion(letter, 123,65,58,64);
		Y = new TextureRegion(letter, 61,145,57,64);
		Z = new TextureRegion(letter, 52,275,50,64);
		
		playerLoad();
		
		items = null;
		store1 = null;
		UI = null;
		UI1 = null;
		menuUI = null;
		DashBoard = null;
		spider1 = null;spider2 = null;spider3 = null;spider4 = null;spider5 = null;spider6 = null;
		alien1 = null;alien2 = null;alien3 = null;alien4 = null;alien5 = null;alien6 = null;
		shield1 = null;shield2 = null;shield3 = null;shield4 = null;shield5 = null;shield6 = null;
		coin1 = null;coin2 = null;coin3 = null;coin4 = null;coin5 = null;coin6 = null;
		letter = null;

	}
	
	public static void playerLoad(){
		//load Girl
		Texture player = catch_d_stars.manager.get("data/images/CharacterSpriteSheet/NinjaGirl/ninjaGirl.png");
		TextureRegion player1,player2,player3,player4,player5,player6,player7,player8,player9,player10;
		player1 = new TextureRegion(player,964,267,167,243);
		player2 = new TextureRegion(player,1398,0,166,252);
		player3 = new TextureRegion(player,783,748,163,251);
		player4 = new TextureRegion(player,783,267,180,238);
		player5 = new TextureRegion(player,1044,0,181,241);
		player6 = new TextureRegion(player,783,506,167,241);
		player7 = new TextureRegion(player,1132,242,165,251);
		player8 = new TextureRegion(player,1565,0,161,256);
		player9 = new TextureRegion(player,951,511,163,253);
		player10 = new TextureRegion(player,1226,0,171,238);
		
		girlRunAnimation = new Animation(0.08f, new  TextureRegion[]{player1,player2,player3,player4,player5,player6,player7,player8,player9,player10});
		girlRunAnimation.setPlayMode(PlayMode.LOOP);
		//Jump Animation
		player1 = new TextureRegion(player,1277,505,152,237);
		player2 = new TextureRegion(player,1298,253,155,251);
		player3 = new TextureRegion(player,1115,740,145,238);
		player4 = new TextureRegion(player,1601,257,144,237);
		player5 = new TextureRegion(player,1881,0,144,237);
		player6 = new TextureRegion(player,1430,505,144,237);
		player7 = new TextureRegion(player,947,765,155,234);
		player8 = new TextureRegion(player,1454,257,146,230);
		player9 = new TextureRegion(player,1115,511,161,228);
		player10 = new TextureRegion(player,1727,0,153,228);

		girlJumpAnimation = new Animation(0.15f, new  TextureRegion[]{player1,player2,player3,player4,player5,player6,player7,player8,player9,player10});
		girlJumpAnimation.setPlayMode(PlayMode.LOOP);
		//Dead Animation
		player1 = new TextureRegion(player,0,0,260,266);
		player2 = new TextureRegion(player,261,0,260,266);
		player3 = new TextureRegion(player,522,0,260,266);
		player4 = new TextureRegion(player,0,267,260,266);
		player5 = new TextureRegion(player,261,267,260,266);
		player6 = new TextureRegion(player,0,534,260,266);
		player7 = new TextureRegion(player,522,267,260,266);
		player8 = new TextureRegion(player,261,534,260,266);
		player9 = new TextureRegion(player,522,534,260,266);
		player10 = new TextureRegion(player,783,0,260,266);

		girlDeadAnimation = new Animation(0.08f, new  TextureRegion[]{player1,player2,player3,player4,player5,player6,player7,player8,player9,player10});
		
		
		//Load Boy
		
		player = catch_d_stars.manager.get("data/images/CharacterSpriteSheet/CowBoy/cowBoy.png");
		//RunAnimation
		player1 = new TextureRegion(player,933,752,172,237);
		player2 = new TextureRegion(player,1323,243,181,245);
		player3 = new TextureRegion(player,1446,0,184,242);
		player4= new TextureRegion(player,1244,0,201,231);
		player5 = new TextureRegion(player,933,278,201,235);
		player6 = new TextureRegion(player,1807,0,170,235);
		player7 = new TextureRegion(player,1631,0,175,245);
		player8 = new TextureRegion(player,933,514,183,237);
		player9 = new TextureRegion(player,1117,514,179,223);
		player10 = new TextureRegion(player,1135,278,187,231);

		boyRunAnimation = new Animation(0.08f, new  TextureRegion[]{player1,player2,player3,player4,player5,player6,player7,player8,player9,player10});
		boyRunAnimation.setPlayMode(PlayMode.LOOP);

		//JumpAnimation
		player1 = new TextureRegion(player,1297,510,167,235);
		player2 = new TextureRegion(player,1117,738,164,243);
		player3 = new TextureRegion(player,1505,246,161,223);
		player4 = new TextureRegion(player,1667,246,161,222);
		player5 = new TextureRegion(player,1505,470,161,222);
		player6 = new TextureRegion(player,1282,746,161,222);
		player7 = new TextureRegion(player,1829,461,156,241);
		player8 = new TextureRegion(player,1444,746,158,239);
		player9 = new TextureRegion(player,1829,236,160,224);
		player10 = new TextureRegion(player,1667,469,160,224);

		
		boyJumpAnimation = new Animation(0.03f, new  TextureRegion[]{player1,player2,player3,player4,player5,player6,player7,player8,player9,player10});
		boyJumpAnimation.setPlayMode(PlayMode.LOOP);
		
		//DeadAnimation
		player1 = new TextureRegion(player,0,0,310,277);
		player2 = new TextureRegion(player,311,0,310,277);
		player3 = new TextureRegion(player,622,0,310,277);
		player4 = new TextureRegion(player,0,278,310,277);
		player5 = new TextureRegion(player,0,556,310,277);
		player6 = new TextureRegion(player,311,278,310,277);
		player7 = new TextureRegion(player,311,556,310,277);
		player8 = new TextureRegion(player,622,278,310,277);
		player9 = new TextureRegion(player,622,556,310,277);
		player10 = new TextureRegion(player,933,0,310,277);

		boyDeadAnimation = new Animation(0.08f, new  TextureRegion[]{player1,player2,player3,player4,player5,player6,player7,player8,player9,player10});
		
		player = null;
		player1 = null;player2 = null;player3 = null;player4 = null;player5 = null;player6 = null;player7 = null;player8 = null;player9 = null;player10 = null;
	}
	
	public static void dispose(){
		basicPlatform.dispose();
		
		supportPlatform.dispose();
		
		
		basicPlatform = null;supportPlatform=null;;
		
			
		boyRunAnimation = null;boyJumpAnimation = null;boyDeadAnimation=null;girlRunAnimation = null;girlJumpAnimation = null;girlDeadAnimation=null;
		starAnimation=null;
		
		shieldAnimation = null;
		alarmAnimation = null;
		alienAnimation = null;
		
		starAnimation = null;
		coin1 = null;coin2 = null;coin3 = null;coin4 = null;coin5 = null;coin6 = null;
		coinAnimation = null;
		cometAnimation = null;
		spiderAnimation = null;
	}
}