package com.jakkarrlgames.scroller;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.jakkarrlgames.assetloader.AssetLoader;

public class Word_Char extends Spawnable {

	private char[] word;
	private int wordPosition = 0;
//	private BitmapFont font;
	
	
	public Word_Char(float x, float y, int width, int height,
			float scrollSpeed, ScrollHandler scrollHandler,char[] word) {
		super(x, y, width, height, scrollSpeed, scrollHandler);
		this.word = word;
		wordPosition = 0;
//		font = new BitmapFont();
		// TODO Auto-generated constructor stub
	}
	
	public void resetWord(char[] word){
		this.word = word;
		wordPosition = 0;
	}
	
	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub
		super.update(delta);
	}
	

	public void render(SpriteBatch batch) {
		// TODO Auto-generated method stub
		//font.draw(batch, ""+word[wordPosition], position.x, position.y);
		TextureRegion letter = null;
		switch(Character.toString(word[wordPosition]).toLowerCase()){
		 case "a" : letter = AssetLoader.A; break;
		 case "b" : letter = AssetLoader.B; break;
		 case "c" : letter = AssetLoader.C; break;
		 case "d" : letter = AssetLoader.D; break;
		 case "e" : letter = AssetLoader.E; break;
		 case "f" : letter = AssetLoader.F; break;
		 case "g" : letter = AssetLoader.G; break;
		 case "h" : letter = AssetLoader.H; break;
		 case "i" : letter = AssetLoader.I; break;
		 case "j" : letter = AssetLoader.J; break;
		 case "k" : letter = AssetLoader.K; break;
		 case "l" : letter = AssetLoader.L; break;
		 case "m" : letter = AssetLoader.M; break;
		 case "n" : letter = AssetLoader.N; break;
		 case "o" : letter = AssetLoader.O; break;
		 case "p" : letter = AssetLoader.P; break;
		 case "q" : letter = AssetLoader.Q; break;
		 case "r" : letter = AssetLoader.R; break;
		 case "s" : letter = AssetLoader.S; break;
		 case "t" : letter = AssetLoader.T; break;
		 case "u" : letter = AssetLoader.U; break;
		 case "v" : letter = AssetLoader.V; break;
		 case "w" : letter = AssetLoader.W; break;
		 case "x" : letter = AssetLoader.X; break;
		 case "y" : letter = AssetLoader.Y; break;
		 case "z" : letter = AssetLoader.Z; break;
		}
		if(!isCountMatch()){
			batch.draw(letter,position.x, position.y,getWidth(),getHeight());
		}

//		font.draw(batch, "Total Count " + word.length, position.x + 20, position.y + 20);
//		font.draw(batch, "Current Count" + wordPosition, position.x+20, position.y);
//		font.draw(batch, "IsMatchCount ?" + isCountMatch(), 300, 400);
	}
	
	public void setWordPosition(int pos){this.wordPosition = pos;}
	public int getWordPosition(){return wordPosition;}
	
	public boolean isCountMatch(){return (word.length) >= wordPosition ? false:true;}
	public int totalWordCount(){return word.length;}
	public int getCurrentPosition(){return wordPosition;}
	public char getCurrentLetter(){return word[wordPosition];}
 
}
