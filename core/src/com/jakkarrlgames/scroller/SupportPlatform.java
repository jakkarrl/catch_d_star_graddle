package com.jakkarrlgames.scroller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.jakkarrlgames.assetloader.AssetLoader;


public class SupportPlatform extends Scroller{

	private Rectangle cloudTop;
	
	public SupportPlatform(float x, float y, int width, int height, float scrollSpeed) {
		super(x, y, width, height, scrollSpeed);
		cloudTop = new Rectangle(x,y-height,width,10);
	}
	
	@Override
	public void update(float delta) {
		super.update(delta);
		boxPolygon.setVertices(getVertices());
		cloudTop.setPosition(getX(),getY()+getHeight());
	}
	
	public void reset(float newX,float newY) {
		super.reset(newX);
		position.y = newY;
	}
	
	
	public void render(SpriteBatch batch){
		batch.draw(AssetLoader.supportPlatform, getX(), getY(), getWidth(),getHeight());
	}
	
	public Rectangle getRectangle(){return cloudTop;}
}
