package com.jakkarrlgames.scroller;

import java.util.Iterator;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Collectable_Item extends Scroller {
	protected Vector2 yAcceleration;
	protected float keyFrame;
	private ScrollHandler scrollHandler;
	protected Rectangle boxRectangle;
	private Animation starAnimation;
	public Collectable_Item(float x, float y, int width, int height, float scrollSpeed,Animation starAnimation,ScrollHandler scrollHandler) {
		super(x, y, width, height, scrollSpeed);
		yAcceleration = new Vector2(0,-325);
		this.scrollHandler = scrollHandler;
		this.starAnimation = starAnimation;
		boxPolygon = new Polygon(new float[]{x,y,x+width,y,x+width,y+height,x,y+height});
		boxRectangle = new Rectangle(x,y,width,height);
	}
	
	public void dispose(){
		yAcceleration = null;
		scrollHandler = null;
		starAnimation = null;
		boxRectangle = null;
	}
	
	@Override
	public void update(float delta) {
		keyFrame += delta;
		
//		velocity.add(yAcceleration).cpy().scl(delta);
//		if(velocity.y > 200) velocity.y = 200;
		boxRectangle.setPosition(position);
		boxPolygon.setVertices(getVertices());
		
		if(!cloudPlatformCollision() && !cloudCollision()){
			if(yAcceleration.y > 325) yAcceleration.y = 325;
			position.add(yAcceleration.cpy().scl(delta));
			boxPolygon.setVertices(getVertices());
			boxRectangle.setPosition(position);
		}else{
			position.add(acceleration.cpy().scl(delta));
			if(position.x+width < 0){
				setScrolledLeft(true);
			}
		}
	}
	
	public void render(SpriteBatch batch) {
//		batch.begin();
		batch.draw(starAnimation.getKeyFrame(keyFrame), getX(), getY(), width, height);
//		batch.end();
	}
	
	public void reset(float newX,float newY){
		super.reset(newX);
		position.y = newY;
		
	}

	
	//Detect Plan vs Ground Collision and stop plane falling down further
	public boolean cloudPlatformCollision(){
		Iterator<Platform> it = scrollHandler.getCloudPlatform().iterator();
		while(it.hasNext()){
			Platform cloud = it.next();
			if(Intersector.overlapConvexPolygons(cloud.getPolygon(),boxPolygon)){
				return true;
			}
		}
		return false;
	}

	public boolean cloudCollision(){
		Iterator<SupportPlatform> it = scrollHandler.getCloud().iterator();
		while(it.hasNext()){
			SupportPlatform cld = it.next();
			if(Intersector.overlaps(cld.getRectangle(), boxRectangle)){
				return true;
			}
		}
		return false;
	}	
	
	public Rectangle getStarRectangle(){return boxRectangle;}
	
	public void setYAcceleration(float yValue){yAcceleration.y = yValue;}
}
