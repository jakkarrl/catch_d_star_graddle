package com.jakkarrlgames.scroller;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Polygon;
import com.jakkarrlgames.assetloader.AssetLoader;

public class Platform extends Scroller {

	
	public Platform(float x, float y, int width, int height,
			float scrollSpeed) {
		super(x, y, width, height, scrollSpeed);
}
	
	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub
		super.update(delta);
		boxPolygon.setVertices(getVertices());
	}
	
	public float[] getVertices() {
		return new float[]{getX(),getY(),(getX()+width),getY(),(getX()+width),(getY()+height),getX(),getY()+height};
	}
	
	public void render(SpriteBatch batch){
		TextureRegion region = new TextureRegion(AssetLoader.basicPlatform,0,47,getWidth(),103);
		batch.draw(region, getX(),getY()-94,getWidth(),103);
		region = new TextureRegion(AssetLoader.basicPlatform,0,0,800,47);
		batch.draw(region, getX(), getY(), getWidth(),getHeight());
}
	
	
	public void reset(float newX,float newY) {
		super.reset(newX);
		position.y = newY;
	}
}
