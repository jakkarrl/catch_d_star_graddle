package com.jakkarrlgames.scroller;

import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;

public class Scroller {
	protected Vector2 position;
	protected Vector2 acceleration;
	protected int width,height;
	protected Polygon boxPolygon;
	private float scrollSpeed;
	private boolean isScrolledLeft;
	
	public void dispose(){
		position = null;
		acceleration = null;
		boxPolygon = null;
	}
	public Scroller(float x,float y,int width,int height,float scrollSpeed){
		position = new Vector2(x,y);
		this.scrollSpeed = scrollSpeed;
		acceleration = new Vector2(scrollSpeed,0);
		this.width = width;this.height = height;
		isScrolledLeft = false;
		boxPolygon = new Polygon(new float[]{x,y,(x+width),y,(x+width),(y+height),x,(y+height)});
	}
	
	public void update(float delta){
		position.add(acceleration.cpy().scl(delta));
		if((position.x + width) < -100){
			isScrolledLeft = true;
		}
	}
	
	public float[] getVertices() {
		return new float[]{getX(),getY(),(getX()+width),getY(),(getX()+width),(getY()+height),getX(),getY()+height};
	}
	
	public void reset(float newX){
		position.x = newX;
		isScrolledLeft = false;
	}
	
	public void reset(float newX,float newY){
		position.x = newX;position.y=newY;isScrolledLeft = false;
	}

	public void stop(){acceleration.x = 0;}
	public void start(){acceleration.x = scrollSpeed;}
	public boolean isScrolledLeft() {
		return isScrolledLeft;
	}

	public void setScrolledLeft(boolean flag){
		isScrolledLeft = flag;
	}
	public float getTailX() {
		return position.x + width;
	}

	public float getX() {
		return position.x;
	}

	public float getY() {
		return position.y;
	}

	public void setPosition(Vector2 pos){this.position = pos;}
	public void setPosition(float x,float y){this.position = new Vector2(x,y);}
	
	public void setAcceleration(Vector2 acc){this.acceleration = acc;}
	
	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	public Polygon getPolygon(){return boxPolygon;}
	public void setGameSpeed(float gameSpeed){
		this.scrollSpeed = gameSpeed;
		acceleration.set(scrollSpeed, 0);
		}

}
