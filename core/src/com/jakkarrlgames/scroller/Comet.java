package com.jakkarrlgames.scroller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.jakkarrlgames.assetloader.AssetLoader;

public class Comet extends UnCollectable_Item {
	private ParticleEffect effect;
	public Comet(float x, float y, int width, int height, float scrollSpeed,
			Animation starAnimation, ScrollHandler scrollHandler) {
		super(x, y, width, height, scrollSpeed, starAnimation, scrollHandler);
		effect = new ParticleEffect();
		effect.load(Gdx.files.internal("data/effect/effect.p"),Gdx.files.internal("data/images/"));
		effect.scaleEffect(0.25f);
		
		effect.start();
		// TODO Auto-generated constructor stub
//		setYAcceleration(-500);
	}
	
	@Override
	public void update(float delta) {
		// TODO Auto-generated method stub
		super.update(delta);
		if(!cloudPlatformCollision() && !cloudCollision()){
			position.set((float) (position.x + Math.sin(90) + Math.sin(90)) ,position.y);
			boxPolygon.setVertices(getVertices());
			boxRectangle.setPosition(position);
		}
		effect.setPosition(position.x + getWidth() / 2,position.y + getHeight() /2);
		effect.setFlip(true, true);
		effect.update(delta);
	}
	
	@Override
	public void render(SpriteBatch batch) {
		// TODO Auto-generated method stub
		super.render(batch);
		effect.draw(batch);
	}

}
