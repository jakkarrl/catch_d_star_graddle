package com.jakkarrlgames.scroller;

import java.util.Iterator;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.jakkarrlgames.assetloader.AssetLoader;

public class Spawnable extends Scroller{
	private Vector2 yAcceleration;
	private ScrollHandler scrollHandler;
	private Rectangle boxRectangle;
	private float keyFrame;
	public void dispose(){
		yAcceleration = null;
		scrollHandler = null;
		boxRectangle = null;
	}
	
	public Spawnable(float x, float y, int width, int height, float scrollSpeed,ScrollHandler scrollHandler) {
		super(x, y, width, height, scrollSpeed);
		yAcceleration = new Vector2(0,-125);
		boxPolygon = new Polygon(new float[]{x,y,x+width,y,x+width,y+height,x,y+height});
		this.scrollHandler = scrollHandler;
		boxRectangle = new Rectangle(x,y,width,height);
	}

	@Override
	public void update(float delta) {
		keyFrame+=delta;
		boxRectangle.setPosition(position);
		boxPolygon.setVertices(getVertices());
		if(!cloudPlatformCollision() && !cloudCollision()){
			if(yAcceleration.y > 325) yAcceleration.y = 325;
			position.add(yAcceleration.cpy().scl(delta));
			boxPolygon.setVertices(getVertices());
			boxRectangle.setPosition(position);
		}else{
			position.add(acceleration.cpy().scl(delta));
			if(position.x+width < 0){
				setScrolledLeft(true);
			}
		}
	}
	
	public void render(Animation animation,SpriteBatch batch) {
//		batch.begin();
		batch.draw(animation.getKeyFrame(keyFrame), getX(), getY(), width, height);
//		batch.end();
	}
	
	public void reset(float newX,float newY){
		super.reset(newX);
		position.y = newY;
		
	}
	
	public boolean cloudPlatformCollision(){
		Iterator<Platform> it = scrollHandler.getCloudPlatform().iterator();
		while(it.hasNext()){
			Platform cloud = it.next();
			if(Intersector.overlapConvexPolygons(cloud.getPolygon(),boxPolygon)){
				return true;
			}
		}
		return false;
	}

	public boolean cloudCollision(){
		Iterator<SupportPlatform> it = scrollHandler.getCloud().iterator();
		while(it.hasNext()){
			SupportPlatform cld = it.next();
			if(Intersector.overlaps(cld.getRectangle(), boxRectangle)){
				return true;
			}
		}
		return false;
	}
}

