package com.jakkarrlgames.scroller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

//import javafx.scene.text.Font;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.jakkarrlgames.assetloader.AssetLoader;
import com.jakkarrlgames.entity.Alien;
import com.jakkarrlgames.entity.FontRenderer;
import com.jakkarrlgames.entity.Spider;
import com.jakkarrlgames.game.catch_d_stars;
import com.jakkarrlgames.helperclass.GameTimer;
import com.jakkarrlgames.helperclass.Timer;
import com.jakkarrlgames.helperclass.UnCollectable_Item_Timer;
import com.jakkarrlgames.scroller.collectableItem.Candy;
import com.jakkarrlgames.scroller.collectableItem.Coin;
import com.jakkarrlgames.scroller.collectableItem.CupCake;
import com.jakkarrlgames.scroller.collectableItem.Fruit;
import com.jakkarrlgames.scroller.collectableItem.Star;
import com.jakkarrlgames.scroller.word.Word;

public class ScrollHandler {
	private ArrayList<Platform> basicPlatform;
	private Platform basicPlatform1,basicPlatform2,basicPlatform3;

	private SupportPlatform supportPlatform1,supportPlatform2,supportPlatform3,supportPlatform4,supportPlatform5,supportPlatform6,supportPlatform7,supportPlatform8,supportPlatform9,supportPlatform10;
	private ArrayList<SupportPlatform> supportPlatform;
	
	private ArrayList<Collectable_Item> collectableItem;

	private UnCollectable_Item_Timer spiderReposition,cometReposition,alienReposition;
	
	private ArrayList<UnCollectable_Item> unCollectableItem;
	private Comet comet1;
	private Spider spider1;
	private Alien alien1;
	
	private float yPos;
	//private float SCROLL_SPEED = -100;
	private float SCROLL_SPEED = -(180*  (Gdx.graphics.getWidth()/Gdx.graphics.getHeight()));
	
	
	
	private Random random;
	private float CONSTANT_GAP_X = 90;
	private Alarm alarm;
	private PowerUp magneticPowerUp;
	private Word_Char word;
	private Timer alarmReposition;
	private Timer magnetPowerUpReposition;
	private Timer wordCharReposition;
		
	private String[] collectableItemList = {"star","candy"};
	
	
	private boolean cldPlatform = true;
	
	public static GameTimer gameTimer;
	
	private int screenWidth;
	
	public ScrollHandler(float yPos){
		
		this.yPos = yPos;
		random = new Random();
		screenWidth = Gdx.graphics.getWidth();
		
		
		basicPlatform = new ArrayList<Platform>();
		basicPlatform1 = new Platform(0, 0, 800, 50, SCROLL_SPEED);
		basicPlatform2 = new Platform(basicPlatform1.getTailX(), 0, 800, 50, SCROLL_SPEED);
		basicPlatform3 = new Platform(basicPlatform2.getTailX(), 0, 800, 50, SCROLL_SPEED);
		basicPlatform.add(basicPlatform1);
		basicPlatform.add(basicPlatform2);
		basicPlatform.add(basicPlatform3);
		
		
		supportPlatform = new ArrayList<SupportPlatform>();
		supportPlatform1 = new SupportPlatform(800,(getMaxPlatform()+ random.nextInt((int)((Gdx.graphics.getHeight() / 2)/1.75f)) + 20),70,35,SCROLL_SPEED);
		supportPlatform2 = new SupportPlatform(supportPlatform1.getTailX() + CONSTANT_GAP_X,(getMaxPlatform()+ random.nextInt((int)((Gdx.graphics.getHeight()/2)/1.75f)) + 20),70,35,SCROLL_SPEED);
		supportPlatform3 = new SupportPlatform(supportPlatform2.getTailX()+ CONSTANT_GAP_X,(getMaxPlatform()+ random.nextInt((int)((Gdx.graphics.getHeight()/2)/1.75f)) + 20),70,35,SCROLL_SPEED);
		supportPlatform4 = new SupportPlatform(supportPlatform3.getTailX()+ CONSTANT_GAP_X,(getMaxPlatform()+ random.nextInt((int)((Gdx.graphics.getHeight()/2)/1.75f)) + 20),70,35,SCROLL_SPEED);
		supportPlatform5 = new SupportPlatform(supportPlatform4.getTailX()+ CONSTANT_GAP_X,(getMaxPlatform()+ random.nextInt((int)((Gdx.graphics.getHeight()/2)/1.75f)) + 20),70,35,SCROLL_SPEED);
		supportPlatform6 = new SupportPlatform(supportPlatform5.getTailX()+ CONSTANT_GAP_X,(getMaxPlatform()+ random.nextInt((int)((Gdx.graphics.getHeight()/2)/1.75f)) + 20),70,35,SCROLL_SPEED);
		supportPlatform7 = new SupportPlatform(supportPlatform6.getTailX()+ CONSTANT_GAP_X,(getMaxPlatform()+ random.nextInt((int)((Gdx.graphics.getHeight()/2)/1.75f)) + 20),70,35,SCROLL_SPEED);
		supportPlatform8 = new SupportPlatform(supportPlatform7.getTailX()+ CONSTANT_GAP_X,(getMaxPlatform()+ random.nextInt((int)((Gdx.graphics.getHeight()/2)/1.75f)) + 20),70,35,SCROLL_SPEED);
		supportPlatform9 = new SupportPlatform(supportPlatform8.getTailX()+ CONSTANT_GAP_X,(getMaxPlatform()+ random.nextInt((int)((Gdx.graphics.getHeight()/2)/1.75f)) + 20),70,35,SCROLL_SPEED);
		supportPlatform10 = new SupportPlatform(supportPlatform9.getTailX()+ CONSTANT_GAP_X,(getMaxPlatform()+ random.nextInt((int)((Gdx.graphics.getHeight()/2)/1.75f)) + 20),70,35,SCROLL_SPEED);
		
		supportPlatform.add(supportPlatform1);
		supportPlatform.add(supportPlatform2);
		supportPlatform.add(supportPlatform3);
		supportPlatform.add(supportPlatform4);
		supportPlatform.add(supportPlatform5);
		supportPlatform.add(supportPlatform6);
		supportPlatform.add(supportPlatform7);
		supportPlatform.add(supportPlatform8);
		supportPlatform.add(supportPlatform9);
		supportPlatform.add(supportPlatform10);
		
		collectableItem = new ArrayList<Collectable_Item>();
		unCollectableItem = new ArrayList<UnCollectable_Item>();
		
		fillList(collectableItemList);
		
		alarm = new Alarm(random.nextInt(50) + 700, Gdx.graphics.getHeight(), 30, 30, SCROLL_SPEED, this);
		alarmReposition = new Timer(alarm,30);

		
		
		magneticPowerUp = new PowerUp(-100,Gdx.graphics.getHeight(),30,30,SCROLL_SPEED,this);
		magnetPowerUpReposition = new Timer(magneticPowerUp,58);
		
		word = new Word_Char(random.nextInt(50) + 700, Gdx.graphics.getHeight() , 30, 30, SCROLL_SPEED, this,Word.getWord());

		wordCharReposition = new Timer(word,25);
//		superPowerUp = new PowerUp(500,yPos,30,20,SCROLL_SPEED,this);
//		superPowerReposition = new Timer(superPowerUp, 75);
		
		
		//Load UnCollectable Item1 - Comet
		comet1 = new Comet(-Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 25, 25, SCROLL_SPEED, AssetLoader.cometAnimation, this);
		cometReposition = new UnCollectable_Item_Timer(comet1,12);
		unCollectableItem.add(comet1);
				
		//Load UnCollectable Item2 - Alien
		spider1 = new Spider(Gdx.graphics.getWidth() + 200, Gdx.graphics.getHeight(), 30, 30, SCROLL_SPEED, AssetLoader.spiderAnimation, this,(random.nextInt(300) + getMaxPlatform()));
		
		spiderReposition = new UnCollectable_Item_Timer(spider1,5);
//		unCollectableItem.add(spider1);
				
		//Load UnCollectable Item3 - Spider
		alien1 = new Alien(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 70, 80, SCROLL_SPEED, AssetLoader.alienAnimation, this);
		alienReposition = new UnCollectable_Item_Timer(comet1,15);
//		unCollectableItem.add(alien1);
		
		//GameTimer
		gameTimer = new GameTimer();
	}
	
	public void resetToOld(){
		basicPlatform1.reset(0, 0);
		basicPlatform2.reset(basicPlatform1.getTailX(), 0);
		basicPlatform3.reset(basicPlatform2.getTailX(), 0);

		supportPlatform1.reset(800,(getMaxPlatform()+ random.nextInt((int)((Gdx.graphics.getHeight() / 2)/1.75f)) + 20));
		supportPlatform2.reset(supportPlatform1.getTailX() + CONSTANT_GAP_X,(getMaxPlatform()+ random.nextInt((int)((Gdx.graphics.getHeight()/2)/1.75f)) + 20));
		supportPlatform3.reset(supportPlatform2.getTailX() + CONSTANT_GAP_X,(getMaxPlatform()+ random.nextInt((int)((Gdx.graphics.getHeight()/2)/1.75f)) + 20));
		supportPlatform4.reset(supportPlatform3.getTailX() + CONSTANT_GAP_X,(getMaxPlatform()+ random.nextInt((int)((Gdx.graphics.getHeight()/2)/1.75f)) + 20));
		supportPlatform5.reset(supportPlatform4.getTailX() + CONSTANT_GAP_X,(getMaxPlatform()+ random.nextInt((int)((Gdx.graphics.getHeight()/2)/1.75f)) + 20));
		supportPlatform6.reset(supportPlatform5.getTailX() + CONSTANT_GAP_X,(getMaxPlatform()+ random.nextInt((int)((Gdx.graphics.getHeight()/2)/1.75f)) + 20));
		supportPlatform7.reset(supportPlatform6.getTailX() + CONSTANT_GAP_X,(getMaxPlatform()+ random.nextInt((int)((Gdx.graphics.getHeight()/2)/1.75f)) + 20));
		supportPlatform8.reset(supportPlatform7.getTailX() + CONSTANT_GAP_X,(getMaxPlatform()+ random.nextInt((int)((Gdx.graphics.getHeight()/2)/1.75f)) + 20));
		supportPlatform9.reset(supportPlatform8.getTailX() + CONSTANT_GAP_X,(getMaxPlatform()+ random.nextInt((int)((Gdx.graphics.getHeight()/2)/1.75f)) + 20));
		supportPlatform10.reset(supportPlatform9.getTailX() + CONSTANT_GAP_X,(getMaxPlatform()+ random.nextInt((int)((Gdx.graphics.getHeight()/2)/1.75f)) + 20));

		collectableItem.clear();
		collectableItemList = new String[]{"star","candy"};
		fillList(collectableItemList);



		unCollectableItem.clear();
		unCollectableItem.add(comet1);
		comet1.reset(-Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		spider1.reset(Gdx.graphics.getWidth() + 200, Gdx.graphics.getHeight());
		alien1.reset(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		alarm.reset(-Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		//alramReposition.resetTimer(30);
		magneticPowerUp.reset(-100,Gdx.graphics.getHeight());
		//magnetPowerUpReposition.resetTimer(58);

		word.resetWord(Word.getWord());
		word.reset(random.nextInt(50) + 700, Gdx.graphics.getHeight());
		//wordCharReposition.resetTimer(40);

		gameTimer.resetTimer();
	}

	public void update(float delta){
		//GameTiming update
		gameTimer.update(delta);
		if(gameTimer.getMin() >=1){
			if(!unCollectableItem.contains(spider1)){
				spider1.setGameSpeed(SCROLL_SPEED);
				unCollectableItem.add(spider1);}
		}
		if(gameTimer.getMin() >=2){
			if(!unCollectableItem.contains(alien1)){
				alien1.setGameSpeed(SCROLL_SPEED);
				unCollectableItem.add(alien1);}
		}
		
		//Star Update
		Iterator<Collectable_Item> item = collectableItem.iterator();
		while(item.hasNext()){
			Collectable_Item itemTmp =item.next(); 
			itemTmp.update(delta);
			
			if(itemTmp.isScrolledLeft()){
				itemTmp.reset(random.nextInt(screenWidth), Gdx.graphics.getHeight());
			}else if(itemTmp.getY() < 0){
				System.out.println("Star out of range");
				itemTmp.reset(itemTmp.getX(), Gdx.graphics.getHeight());
			}
		}

		
		Iterator<UnCollectable_Item> uItem = unCollectableItem.iterator();
		while(uItem.hasNext()){
			UnCollectable_Item uItemTmp = uItem.next();
			uItemTmp.update(delta);
			
			if(uItemTmp instanceof Spider){
				Spider spider = (Spider)uItemTmp;
				if(spiderReposition.isReset()){
					spider.reset(random.nextInt(screenWidth) + 100,Gdx.graphics.getHeight(),(random.nextInt(300) + getMaxPlatform()));
					spiderReposition.setReset(false);
				}
				if(spider.isScrolledLeft()){spiderReposition.update(delta);}
				else if(spider.getY() < 0){
					spider.reset(random.nextInt(screenWidth) + 100,Gdx.graphics.getHeight(),(random.nextInt(300) + getMaxPlatform()));
				}
			}else if(uItemTmp instanceof Comet){
				Comet comet = (Comet) uItemTmp;
				if(cometReposition.isReset()){
					comet.reset(random.nextInt(screenWidth) + 100, Gdx.graphics.getHeight());
					cometReposition.setReset(false);
				}
				
				if(comet.isScrolledLeft()){cometReposition.update(delta);}
				else if(comet.getY() < 0){
					comet.reset(comet1.getX(), Gdx.graphics.getHeight());
				}
			}else if(uItemTmp instanceof Alien){
				Alien alien = (Alien) uItemTmp;
				if(alienReposition.isReset()){
					alien.reset(random.nextInt(screenWidth) + Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight());
					alienReposition.setReset(false);
				}
				if(alien.isScrolledLeft()){
					alienReposition.update(delta);
				}else if(alien.getY() < 0){
					alien.reset(random.nextInt(screenWidth) + Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight());
				}
			}
		}
		

		//Alarm Update
		alarm.update(delta);
		alarmReposition.update(delta);
		if(alarm.getY() < 0){
			alarm.reset(alarm.getX(), Gdx.graphics.getHeight());
		}
		
		//PowerUp Update
		//Magnetic Power up
		magneticPowerUp.update(delta);
		magnetPowerUpReposition.update(delta);
		if(magneticPowerUp.getY() < 0) {
			magneticPowerUp.reset(magneticPowerUp.getX(), Gdx.graphics.getHeight());
		}
		
		//WordCount 
		if(word != null && !word.isCountMatch()){
			word.update(delta);
			wordCharReposition.update(delta);
			if(word.getY() < 0){
				word.reset(word.getX(),Gdx.graphics.getHeight());
			}
		}
		//SuperPower Up
//		superPowerUp.update(delta);
//		superPowerReposition.update(delta);
//		if(superPowerUp.getY() < 0){
//			superPowerUp.reset(superPowerUp.getX(),Gdx.graphics.getHeight());
//		}
		
		

		//Cloud Platform Update
		basicPlatform1.update(delta);
		basicPlatform2.update(delta);
		basicPlatform3.update(delta);
		if(basicPlatform1.isScrolledLeft()){
			cloudPlatformerReset(basicPlatform1, basicPlatform3);
		}
			
		if(basicPlatform2.isScrolledLeft()){
			cloudPlatformerReset(basicPlatform2, basicPlatform1);
		}
		
		if(basicPlatform3.isScrolledLeft()){
			cloudPlatformerReset(basicPlatform3, basicPlatform2);
		}
		
		//Support_Platform Update
		Iterator<SupportPlatform> platform = supportPlatform.iterator();
		while(platform.hasNext()){
			platform.next().update(delta);
		}
		if(supportPlatform1.isScrolledLeft()){
			cloudReset(supportPlatform1, supportPlatform10);
		}
		if(supportPlatform2.isScrolledLeft()){
			cloudReset(supportPlatform2,supportPlatform1);
		}
		if(supportPlatform3.isScrolledLeft()){
			cloudReset(supportPlatform3,supportPlatform2);
		}
		if(supportPlatform4.isScrolledLeft()){
			cloudReset(supportPlatform4,supportPlatform3);
		}
		if(supportPlatform5.isScrolledLeft()){
			cloudReset(supportPlatform5,supportPlatform4);
		}
		if(supportPlatform6.isScrolledLeft()){
			cloudReset(supportPlatform6,supportPlatform5);
		}
		if(supportPlatform7.isScrolledLeft()){
			cloudReset(supportPlatform7,supportPlatform6);
		}
		if(supportPlatform8.isScrolledLeft()){
			cloudReset(supportPlatform8,supportPlatform7);
		}
		if(supportPlatform9.isScrolledLeft()){
			cloudReset(supportPlatform9,supportPlatform8);
		}
		if(supportPlatform10.isScrolledLeft()){
			cloudReset(supportPlatform10,supportPlatform9);
		}
		
		
		//-WOrd COunt Removing from world--
//		if(word != null && word.isCountMatch()){
//			word = null;
//		}
	}
	
	public void render(SpriteBatch batch){
		basicPlatform1.render(batch);
		basicPlatform2.render(batch);
		basicPlatform3.render(batch);
		supportPlatform1.render(batch);
		supportPlatform2.render(batch);
		supportPlatform3.render(batch);
		supportPlatform4.render(batch);
		supportPlatform5.render(batch);
		supportPlatform6.render(batch);
		supportPlatform7.render(batch);
		supportPlatform8.render(batch);
		supportPlatform9.render(batch);
		supportPlatform10.render(batch);

		alarm.render(AssetLoader.alarmAnimation,batch);
		
		magneticPowerUp.render(AssetLoader.magnetAnimation, batch);
		if(word != null)word.render(batch);
//		superPowerUp.render(AssetLoader.superPowerAnimation, batch);
		//Rendering Star in the screen
		Iterator<Collectable_Item> item = collectableItem.iterator();
		while(item.hasNext()){
			item.next().render(batch);
		}

		//Renndering UnCollectable Items - Comet & Spider & Alien
		Iterator<UnCollectable_Item> uitem = unCollectableItem.iterator();
		while(uitem.hasNext()){
			uitem.next().render(batch);
		}
		
		magnetPowerUpReposition.render(batch);
		
	}
	
	
	
	public void stopScrolling(){
		basicPlatform1.stop();
		basicPlatform2.stop();
		basicPlatform3.stop();
		supportPlatform1.stop();
		supportPlatform2.stop();
		supportPlatform3.stop();
		supportPlatform4.stop();
		supportPlatform5.stop();
		supportPlatform6.stop();
		supportPlatform7.stop();
		supportPlatform8.stop();
		supportPlatform9.stop();
		supportPlatform10.stop();

		//Code to Stop Scrolling Stars
		alarm.stop();
		
		Iterator<Collectable_Item> it = collectableItem.iterator();
		while(it.hasNext()){
			Collectable_Item itm = it.next();
			itm.stop();
		}
		
		Iterator<UnCollectable_Item> uit = unCollectableItem.iterator();
		while(uit.hasNext()){
			UnCollectable_Item ui = uit.next();
			ui.stop();
		}
		
		magneticPowerUp.stop();
		if(word != null) word.stop();
//		superPowerUp.stop();
		
		
		//Stoping the Timers
		spiderReposition.stop();
		cometReposition.stop();
		alienReposition.stop();


		alarmReposition.stop();
		magnetPowerUpReposition.stop();
		wordCharReposition.stop();
//		superPowerReposition.stop();

		gameTimer.stop();
	}
	
	public void startScrolling(){
		basicPlatform1.start();
		basicPlatform2.start();
		basicPlatform3.start();
		supportPlatform1.start();
		supportPlatform2.start();
		supportPlatform3.start();
		supportPlatform4.start();
		supportPlatform5.start();
		supportPlatform6.start();
		supportPlatform7.start();
		supportPlatform8.start();
		supportPlatform9.start();
		supportPlatform10.start();

		//Code to start Scrolling Stars
		alarm.start();
		
		Iterator<Collectable_Item> it = collectableItem.iterator();
		while(it.hasNext()){
			Collectable_Item itm = it.next();
			itm.start();
		}
		
		Iterator<UnCollectable_Item> uit = unCollectableItem.iterator();
		while(uit.hasNext()){
			UnCollectable_Item ui = uit.next();
			ui.start();
		}
		
		magneticPowerUp.start();
		if(word != null) word.start();
//		superPowerUp.start();
		
		
		//starting the Timers
		spiderReposition.start();
		cometReposition.start();
		alienReposition.start();


		alarmReposition.start();
		magnetPowerUpReposition.start();
		wordCharReposition.start();
//		superPowerReposition.start();

		gameTimer.start();

	}
	

	public void dipose(){
		alarm.dispose();
		supportPlatform1.dispose();
		supportPlatform2.dispose();
		basicPlatform1.dispose();
		basicPlatform2.dispose();
		basicPlatform3.dispose();
	}
	
	
	//To Reset the cloud platform when it scroll left to the screen
	public void cloudPlatformerReset(Platform cld1,Platform cld2){
//		int rnd = random.nextInt(50);
		int rnd = random.nextInt(cld1.getHeight()/2);
//		if(cldPlatform){
//			cldPlatform = false;
//			cld1.reset(cld2.getTailX() + 100,(float)(cld2.getY() + rnd));
//		}
//		else{
//			cldPlatform = true;
			if( cld2.getY() - rnd < 0){
				cld1.reset(cld2.getTailX() + 100,(float)(cld2.getY() + rnd));
			}
			else
				cld1.reset(cld2.getTailX() + 100,(float)(cld2.getY() - rnd));
//		}
	}
	
	public void cloudReset(SupportPlatform cloud1,SupportPlatform cloud2){
		cloud1.reset(cloud2.getTailX()+ CONSTANT_GAP_X,(getMaxPlatform()+ random.nextInt((int)((yPos/2)/1.75f)) + 20));
	}
	
	//To get the height to draw small clouds above cloud platform 
	public int getMaxPlatform(){
		return (int) ((basicPlatform1.getY() > basicPlatform2.getY()) ? (basicPlatform1.getY() + basicPlatform1.getHeight()) : (basicPlatform2.getY()) + basicPlatform2.getHeight());
//		return (int) ((basicPlatform1.getY() > basicPlatform2.getY()) ? (basicPlatform1.getY() + basicPlatform1.getHeight()) : (basicPlatform2.getY()) + basicPlatform2.getHeight());
		
//		if(basicPlatform1.getY() > basicPlatform2.getY()){
//			if(basicPlatform2.getY() > basicPlatform3.getY()){
//				return (int) (basicPlatform1.getY() + basicPlatform1.getHeight());
//			}else{
//				if(basicPlatform1.getY() > basicPlatform3.getY()){
//					return (int) (basicPlatform1.getY() + basicPlatform1.getHeight());
//				}else{
//					return (int) (basicPlatform3.getY() + basicPlatform3.getHeight());
//				}
//			}
//		}else{
//			if(basicPlatform1.getY() > basicPlatform3.getY()){
//				return (int) (basicPlatform2.getY() + basicPlatform2.getHeight());
//			}else{
//				if(basicPlatform2.getY() > basicPlatform3.getY()){
//					return (int) (basicPlatform2.getY() + basicPlatform2.getHeight());
//				}else{
//					return (int) (basicPlatform3.getY() + basicPlatform3.getHeight());
//				}
//			}
//		}
	}
	
	public int getMinPlatform(){
		return (int) ((basicPlatform1.getY() < basicPlatform2.getY()) ? (basicPlatform1.getY() + basicPlatform1.getHeight()) : (basicPlatform2.getY()) + basicPlatform2.getHeight());
//		if(basicPlatform1.getY() < basicPlatform2.getY()){
//			if(basicPlatform2.getY() < basicPlatform3.getY()){
//				return (int) (basicPlatform1.getY() + basicPlatform1.getHeight());
//			}else{
//				if(basicPlatform1.getY() < basicPlatform3.getY()){
//					return (int) (basicPlatform1.getY() + basicPlatform1.getHeight());
//				}else{
//					return (int) (basicPlatform3.getY() + basicPlatform3.getHeight());
//				}
//			}
//		}else{
//			if(basicPlatform1.getY() < basicPlatform3.getY()){
//				return (int) (basicPlatform2.getY() + basicPlatform2.getHeight());
//			}else{
//				if(basicPlatform2.getY() < basicPlatform3.getY()){
//					return (int) (basicPlatform2.getY() + basicPlatform2.getHeight());
//				}else{
//					return (int) (basicPlatform3.getY() + basicPlatform3.getHeight());
//				}
//			}
//		}
	}
	
	public Spider getSpider(){return spider1;}
	public ArrayList<Platform> getCloudPlatform(){return basicPlatform;}
	public ArrayList<SupportPlatform> getCloud(){return supportPlatform;}
	public ArrayList<Collectable_Item> getCollectableItem(){return collectableItem;}
	public ArrayList<UnCollectable_Item> getUnCollectableItem(){return unCollectableItem;}
	public Alarm getAlarm(){return alarm;}
	public String[] getcollectableItemList(){return collectableItemList;}
	public void setcollectableItemList(String[] list){this.collectableItemList = list;}
	
	public PowerUp getMagneticPower(){return magneticPowerUp;}
	public Word_Char getwordChar(){return word;}
//	public PowerUp getSuperPower(){return superPowerUp;}
	
	public void fillList(String listArg[]){
		collectableItem.clear();
		int length = listArg.length,tmp = 0;
		for(int i=0;i<10;i++){
			if(collectableItem.size() == 10) break;
			else{
				addToList(listArg[tmp]);
				if(collectableItem.size() == 10) break;
				if(tmp == length -1) tmp =0;
				else tmp++;
			}
		}
	}


	private void addToList(String str) {
		switch(str.toLowerCase()){
		case "star": collectableItem.add(new Star(random.nextInt(screenWidth), Gdx.graphics.getHeight(), 30, 30, SCROLL_SPEED,AssetLoader.starAnimation, this));break;
		case "candy": collectableItem.add(new Candy(random.nextInt(screenWidth), Gdx.graphics.getHeight(), 30, 30, SCROLL_SPEED,AssetLoader.candyAnimation, this));break;
		case "coin": collectableItem.add(new Coin(random.nextInt(screenWidth), Gdx.graphics.getHeight(), 30, 30, SCROLL_SPEED,AssetLoader.coinAnimation, this));break;
		case "cupcake":	collectableItem.add(new CupCake(random.nextInt(screenWidth), Gdx.graphics.getHeight(), 30, 30, SCROLL_SPEED,AssetLoader.cupCakeAnimation, this));break;
		case "spaceship": collectableItem.add(new Fruit(random.nextInt(screenWidth), Gdx.graphics.getHeight(), 30, 30, SCROLL_SPEED,AssetLoader.fruitAnimation, this));break;
		}
		
	}
	
	public void increaseGameSpeed(){
		SCROLL_SPEED -= 10;
		//Collectable Update
		Iterator<Collectable_Item> item = collectableItem.iterator();
		while(item.hasNext()){
			item.next().setGameSpeed(SCROLL_SPEED);
		}

		//UnCollectable Items - Comet & Spider & Alien
		Iterator<UnCollectable_Item> uitem = unCollectableItem.iterator();
		while(uitem.hasNext()){
			uitem.next().setGameSpeed(SCROLL_SPEED);
		}
		
		//Support Platform
		Iterator<SupportPlatform> platform = supportPlatform.iterator();
		while(platform.hasNext()){
			platform.next().setGameSpeed(SCROLL_SPEED);
		}
		
		//BasicPlatform
		basicPlatform1.setGameSpeed(SCROLL_SPEED);
		basicPlatform2.setGameSpeed(SCROLL_SPEED);
		basicPlatform3.setGameSpeed(SCROLL_SPEED);
		
		//alarm
		alarm.setGameSpeed(SCROLL_SPEED);
		
		//powerups
		magneticPowerUp.setGameSpeed(SCROLL_SPEED);
		
		//word
		if(word != null)
		word.setGameSpeed(SCROLL_SPEED);
//		superPowerUp.setGameSpeed(SCROLL_SPEED);
		
	}
	
	public void setWord(){if(this.word!=null)this.word = null;}
}

