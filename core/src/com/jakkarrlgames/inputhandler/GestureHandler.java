package com.jakkarrlgames.inputhandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Json;
import com.jakkarrlgames.entity.Player;
import com.jakkarrlgames.game.GamePlay;
import com.jakkarrlgames.helperclass.SaveGame;
import com.jakkarrlgames.persistant.Files;

public class GestureHandler implements GestureListener {

	Player player;
	GamePlay gamePlay;
	public GestureHandler(Player player,GamePlay gameplay) {
		this.player =  player;
		this.gamePlay = gamePlay;
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		//player.onClick();// TODO Auto-generated method stub
		System.out.println("tap - " + count);
		
		if(count <= 1){
//			player.onClick();
		}else if(count > 1){
			SaveGame saveGame = Files.getSaveGame();
			if(saveGame.getSuperPowerCount() > 0){
				if(!player.isSuperPower()){
					player.superPowerCollision();
					System.out.println("Before : " + saveGame.getSuperPowerCount());
					//Reduce the super power
					SaveGame saveToFile = saveGame;
					saveToFile.setSuperPowerCount((saveGame.getSuperPowerCount() - 1));
					Json json = new Json();
					FileHandle file = Gdx.files.local("bin/sampleJson.json");
					file.writeString(json.toJson(saveToFile),false);
					saveGame = Files.getSaveGame();
					System.out.println("After : " + saveGame.getSuperPowerCount());
				}
			}
		}
			
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		// TODO Auto-generated method stub
		System.out.println("Long press");
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		// TODO Auto-generated method stub
		if(velocityY < 0 ){
			player.onClick();
		}else{
			player.slide();
		}
		return true;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		// TODO Auto-generated method stub
		System.out.println("Pan");
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub
		System.out.println("Pan stop");
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		// TODO Auto-generated method stub
		System.out.println("Zoom");
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2,
			Vector2 pointer1, Vector2 pointer2) {
		// TODO Auto-generated method stub
		System.out.println("Pinch");
		return false;
	}

}
