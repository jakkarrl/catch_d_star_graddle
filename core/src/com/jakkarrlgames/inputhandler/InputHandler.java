package com.jakkarrlgames.inputhandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Rectangle;
import com.jakkarrlgames.entity.Player;
import com.jakkarrlgames.game.GamePlay;

public class InputHandler implements InputProcessor {
	private Player player;
	private GamePlay gamePlay;
	public InputHandler(Player player,GamePlay gamePlay){
		this.player = player;
		this.gamePlay = gamePlay;
	}
	@Override
	public boolean keyDown(int keycode) {
		if(Gdx.input.isKeyPressed(Keys.SPACE)){
			player.onClick();
			return true;
		}
		
		if(Gdx.input.isKeyPressed(Keys.ESCAPE)){
			gamePlay.getGameController().setScreen(gamePlay.getGameController().getGameOut());
			return true;
		}
		
		if(Gdx.input.isKeyPressed(Keys.X)){
			player.createBullet();
			return true;
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	
	
	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
	
//		if(gamePlay.getJumpEntity().istouchDown()){
////		if(gamePlay.getJumpEntity().isPressed()){
////			gamePlay.getJumpEntity().setSize(65, 50);
//			player.createBullet();
//			return true;
//		}
//		
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		//gamePlay.getGameController().setScreen(gamePlay.getGameController().getGameOut());
		return true;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public void dispose(){
		player = null;
		gamePlay = null;
	}

}
