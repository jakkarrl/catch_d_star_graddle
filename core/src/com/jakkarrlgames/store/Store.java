package com.jakkarrlgames.store;

import java.util.*;
public class Store{
	private int numberOfCategory;
	private ArrayList<Category> list;
	public Store(int size){
		this.numberOfCategory = size;
		list = new ArrayList<Category>();
	}
	
	public void addCategory(Category category){
		list.add(category);
	}
	public ArrayList<Category> getCategoryList(){return list;}
	public int getMaximumCategory(){return numberOfCategory;}
}
