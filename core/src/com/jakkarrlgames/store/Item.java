package com.jakkarrlgames.store;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.jakkarrlgames.assetloader.AssetLoader;

public class Item implements Json.Serializable {

	
	private String name;
	private String description;
	transient private TextureRegion item;
	private boolean locked;
	private String cItem;
	private int checkAmount;

	public Item(String name,String description,TextureRegion item,boolean locked,String cItem,int checkAmount){
		this.name = name;
		this.description = description;
		this.item = item;
		this.locked = locked;
		this.cItem = cItem;
		this.checkAmount = checkAmount;
	}
	
	public Item(){}
	
	public void draw(SpriteBatch batch,BitmapFont font,float x,float y,float width,float height){
		//font.draw(batch, name, x, y);
		font.draw(batch, description, 350, 80);
		batch.draw(item,x,y,width,height);
		if(locked == true){
			batch.draw(StoreAssets.lock, x, y,30,30);
		}
	}
	
	public void setLock(boolean lock){this.locked = lock;}
	public boolean isLocked(){return locked;}
	
	public String getName() { return this.name; }
	public void setName(String name) {this.name = name;}
	
	public String getDescription(){return this.description;}
	public void setDescription(String description){this.description = description;}
	
	public void setTexture(TextureRegion texture){this.item = texture;}
	public TextureRegion getTexture(){return item;}
	
	public void setCheckAmount(int amount){this.checkAmount = amount;}
	public int getCheckAmount(){return checkAmount;}
	
	public void setCItem(String item){this.cItem = item;}
	public String getCItem(){return cItem;}


	@Override
	public void write(Json json) {
		json.writeValue("name",name);
		json.writeValue("locked",locked);
		json.writeValue("cItem",cItem);
		json.writeValue("checkAmount",checkAmount);
		json.writeValue("description", description);
	}

	@Override
	public void read(Json json, JsonValue jsonData) {
		name = jsonData.getString("name");
		locked = jsonData.getBoolean("locked", true);
		cItem = jsonData.getString("cItem");
		checkAmount = jsonData.getInt("checkAmount");
		description = jsonData.getString("description");
	}
	
}
