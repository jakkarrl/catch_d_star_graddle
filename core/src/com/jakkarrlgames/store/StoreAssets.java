package com.jakkarrlgames.store;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.jakkarrlgames.assetloader.AssetLoader;
import com.jakkarrlgames.game.catch_d_stars;

public class StoreAssets {

	
	public static TextureRegion powerUp1,ninjaGirl,storeIcon,cowBoy,lock;
	public static TextureRegion select,buy;
	public static void loadAsset(){
		
		powerUp1 = AssetLoader.storeSuperPower;
		
		cowBoy = AssetLoader.storeBoy;
		ninjaGirl = AssetLoader.storeGirl;
		lock = AssetLoader.lock;
		
		select = AssetLoader.select;
		buy = AssetLoader.buy;
	}

	
	
}
   