package com.jakkarrlgames.store;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import java.util.*;
public class Category {
	
		private String name;
		private TextureRegion region;
		private ArrayList<Item> list;
		private Iterator<Item> items;
		
		public Category(String name,String path){
			this(name, new TextureRegion(new Texture(path)));
		}
		
		public Category(String name, TextureRegion region){
			this.name = name;
			this.region = region;
			list = new ArrayList<Item>();
		}
		
		public void addItem(Item item){
			list.add(item);
		}
		
		public void draw(SpriteBatch batch,BitmapFont font,float x,float y,float width,float height){
//			font.draw(batch, name, x, y  );
			batch.draw(region,x,y,width,height);
		}
	
		public String getName(){return name;}
		public ArrayList<Item> getItems(){return list;}
	}


