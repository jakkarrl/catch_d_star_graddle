package com.jakkarrlgames.game;

import java.awt.Font;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.jakkarrlgames.assetloader.AssetLoader;
import com.jakkarrlgames.entity.Entity;
import com.jakkarrlgames.entity.FontRenderer;
import com.jakkarrlgames.entity.GUIEntity;
import com.jakkarrlgames.entity.Player;
import com.jakkarrlgames.helperclass.ClockCount;
import com.jakkarrlgames.helperclass.SaveGame;
import com.jakkarrlgames.inputhandler.GestureHandler;
import com.jakkarrlgames.inputhandler.InputHandler;
import com.jakkarrlgames.persistant.Files;
import com.jakkarrlgames.scroller.SupportPlatform;
import com.jakkarrlgames.scroller.ScrollHandler;

public class GamePlay implements Screen{

	private catch_d_stars catchStars;
	private SpriteBatch batch;
	private float screenWidth,screenHeight;
	private ScrollHandler scrollHandler;
	private Player player;

	private ClockCount clockCount,magnetBar,superPowerBar;
	private GUIEntity shoot;
	private GUIEntity pause,musicEntity;
	private GUIEntity exit;
	private OrthographicCamera camera;
	private Viewport viewPort;
	private InputMultiplexer multiplexer;
	private InputHandler input;
	private GestureHandler gesture;
	private GestureDetector gesture1;
	private float alpha;
	private FontRenderer fontRenderer;
	private Music music;
	public static SaveGame saveGame;
	private enum State {
		RUNNING,
		PAUSED,
		RESUMED
	}
	//ClockPower code
	private State state;
	//---------
	
	public static String OUT_REASON = "";
	
	
	float width,height;
	Entity faderEntity;
	Texture faderTexture;
	Sprite faderSprite;
	public static boolean isPlaying = false;
	
	public GamePlay(catch_d_stars catchStars){
		this.catchStars = catchStars;
	}
	@Override
	public void show() {
		state = State.RUNNING;
		
		width = Gdx.graphics.getWidth();
		height = Gdx.graphics.getHeight();
		fontRenderer = catchStars.getObject().getFontRenderer();
		fontRenderer.setFonts("data/font/kenvector_future_thin.ttf", 40);
		alpha = 1.0f; 	
		batch = catchStars.getObject().getBatch();
		shoot = catchStars.getObject().getShoot();
		pause = catchStars.getObject().getPause();
		exit  = catchStars.getObject().getGamePlayExit();
		screenWidth = Gdx.graphics.getWidth();
		screenHeight = Gdx.graphics.getHeight();
		
		//Creating ScrollHandler
		scrollHandler = catchStars.getObject().getScrollHandler();
		scrollHandler.resetToOld();
		scrollHandler.startScrolling();
		
		clockCount = catchStars.getObject().getClockCount();
		clockCount.resetToOld(100);
		magnetBar = catchStars.getObject().getMagnetBar();
		magnetBar.resetToOld(30);
		superPowerBar = catchStars.getObject().getSuperPowerBar();
		superPowerBar.resetToOld(30);
		
	
		//Camera setup
		camera = catchStars.getObject().getCam();
		camera.setToOrtho(false,screenWidth,screenHeight);
		

				
		music = AssetLoader.gamePlayMusic;

		//Creating new Player
		player = catchStars.getObject().getPlayer();
		player.resetToOld();
		
		//Set InputHandler
		setInputs();
		
		
		//viewPort
		viewPort = new ScreenViewport();
		shoot.setSize(65,65);
		shoot.setPosition(10, 10);
//		
		pause.setSize(65, 65);
		pause.setPosition(screenWidth-60,10);
//		
//		musicEntity = new GUIEntity(AssetLoader.musicEnabled,AssetLoader.musicPressed);
		musicEntity = catchStars.getObject().getMusic();
		musicEntity.setPosition(screenWidth/2.75f,screenHeight/2f);
		musicEntity.setSize(50, 50);
//		
		exit.setSize(50,50);
		
		faderEntity = catchStars.getObject().getFaderEntity();
		faderTexture = new Texture(Gdx.files.internal("data/images/background/fader.png"));
		faderEntity.getRegionFromTexture(faderTexture, 0, 0, faderTexture.getWidth(), faderTexture.getHeight());
		faderSprite = faderEntity.createSprite(new Vector2(Gdx.graphics.getWidth(),Gdx.graphics.getHeight()),new Vector2(0,0));
		
		//clockPower code
		
		//------
		saveGame = Files.getSaveGame();
	}
	
	
	
	public void update(float delta){
		if(alpha>0.01f) {
			alpha-=0.02f;
		}
		else {
			//alpha=0.0f;
		}
		clockCount.update(delta);
		player.update(delta);
		scrollHandler.update(delta);
		catch_d_stars.manager.update();

		
//		if(!(jump.istouchDown())) {
//			System.out.println("NOT TOUCHED");
//			jump.setSize(75,60);
//		}
		
		//Game End logic
		if(clockCount.getMeterReading() < 1){
			OUT_REASON = "TIMES_UP";
//			player.setKeyFrame(0);
			player.setAnimation(PreGamePlay.playerDeadAnimation);
			switchScreen(batch);
		}else if(player.getPosition().y < -10){
			OUT_REASON = "FELL_DOWN";
//			player.setKeyFrame(0);
			player.setAnimation(PreGamePlay.playerDeadAnimation);
			
			switchScreen(batch);
		}else if( player.isGameOverCollision()){
			OUT_REASON = "SPIDER";
//			player.setKeyFrame(0);
			player.setAnimation(PreGamePlay.playerDeadAnimation);
			switchScreen(batch);
			}
			
		
		if(shoot.isPressed()){
			player.createBullet();
		}
		if(saveGame.isMusicOn()) {
			if(!music.isPlaying()) {
				music.play();
			}
		}
		else {
			music.stop();
		}
		
	}
	
	
	private void switchScreen(SpriteBatch batch){
		float tempX = (screenWidth - AssetLoader.promptBG.getWidth())/2;
		float tempY = (screenHeight - AssetLoader.promptBG.getHeight())/2;
		
//		player.setAnimation(PreGamePlay.playerDeadAnimation);
//		
//		if(player.canSwitchScreen()){
//			batch.begin();
//			batch.draw(AssetLoader.promptBG, tempX,tempY);
//			String text = "Game Over, Tap to Continue";
//			int len = text.length();
//			font.draw(batch, text, (tempX + AssetLoader.promptBG.getWidth())/2 - len, 100);
//			batch.end();
//		}
//			clockCount.setStop(true);
//			player.stopGame();
//			
//			

			
//			player.setAnimation(PreGamePlay.playerDeadAnimation);
			
//			if(player.canSwitchScreen()){
//				batch.begin();
//				batch.draw(AssetLoader.promptBG, tempX,tempY);
//				String text = "Game Over, Tap to Continue";
//				int len = text.length();
//				font.draw(batch, text, (tempX + AssetLoader.promptBG.getWidth())/2 - len, 100);
//				batch.end();//catchStars.setScreen(catchStars.getGameOut());
//			}
			player.stopGame();
			clockCount.setStop(true);
			
	}
	
	@Override
	public void render(float delta) {
		if(isPlaying) {		
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.setProjectionMatrix(camera.combined);
//		Integer fps = Gdx.graphics.getFramesPerSecond();
//		Gdx.app.log("FPS", fps.toString());
		if(pause.isPressed()) {
			if(state== State.RUNNING) {
				this.state = State.PAUSED;
			}
			else {
				pause.setPosition(screenWidth-60,10);
				pause.setSize(65, 65);
				this.state = State.RUNNING;
			}
		}
		
		if(state==State.RUNNING) {
		
			update(delta);
		
		}
			
		
		batch.begin();
		if(clockCount.getMeterReading() < 1){
			player.setAnimation(PreGamePlay.playerDeadAnimation);
			switchScreen(batch);
		}else if(player.getPosition().y < -10){
			player.setAnimation(PreGamePlay.playerDeadAnimation);
			switchScreen(batch);
			
		}else if( player.isGameOverCollision()){
			player.setAnimation(PreGamePlay.playerDeadAnimation);
			switchScreen(batch);
		}
		batch.end();
		
		batch.begin();
	
		batch.draw(AssetLoader.background, 0, 0, screenWidth,screenHeight);
//		batch.end();
		
		
		//Alert Player about ClockCount
		clockCount.render(batch);
		
		batch.draw(AssetLoader.stopClockDashBoard,clockCount.getTimerX() - 45, clockCount.getTimerY() - 35,35,35 );
		
		if(player.isMagnetPower()) { batch.draw(AssetLoader.magnetDashBoard,magnetBar.getTimerX() - 45, magnetBar.getTimerY() - 35,35,35 );magnetBar.render(batch);}
		if(player.isSuperPower()) { batch.draw(AssetLoader.superPowerDashBoard,superPowerBar.getTimerX() - 45, superPowerBar.getTimerY() - 35,35,35 ); superPowerBar.render(batch); }
//		batch.draw(AssetLoader.alarmAnimation.getKeyFrame(delta), screenWidth - 40 - AssetLoader.timerLayout.getRegionWidth(),screenHeight-70,30,30);
//		batch.draw(AssetLoader.timerMeter,  screenWidth -  AssetLoader.timerMeter.getRegionWidth() -5 ,screenHeight-65,clockCount.getMeterReading(),25);
//		batch.draw(AssetLoader.timerLayout,  screenWidth -  AssetLoader.timerLayout.getRegionWidth() -5 ,screenHeight-65,100,25);
//		//font.draw(batch, "RAGHURAM", 100, 200);
		batch.end();
		
		
		
//		if(catch_d_stars.DEBUG_ON) {
//		shape.begin(ShapeType.Line);
//		Iterator<SupportPlatform> it = scrollHandler.getCloud().iterator();
//		while(it.hasNext()){
//			SupportPlatform sprtPlatform = it.next();
//			shape.rect(sprtPlatform.getX(),sprtPlatform.getY()+sprtPlatform.getHeight(),sprtPlatform.getWidth(),10);
//		}
//		shape.rect(player.getPosition().x,player.getPosition().y,player.getWidth(),5);
//		shape.end();
//		}
		
		batch.begin();
		fontRenderer.font.setColor(Color.BLUE);
		fontRenderer.render(batch, "GameTime" + (int)ScrollHandler.gameTimer.getHour() + ":" + (int)ScrollHandler.gameTimer.getMin() +":" + (int)ScrollHandler.gameTimer.getSec(),60,668);
		fontRenderer.render(batch, "Score : " + Player.SCORE,60,618);
		
		batch.setProjectionMatrix(camera.combined);
		if(isPlaying) {
		player.render(batch,camera);
		}
		if(isPlaying) {
		scrollHandler.render(batch);
		}
		
		fontRenderer.render(batch, " ", 0, 0); // this is dummy drawing
		player.renderPrompt(batch);
		
		batch.end();
		if(isPlaying) {
			shoot.render(batch, camera);
			pause.render(batch, camera);
			batch.setProjectionMatrix(camera.combined);
			batch.begin();
			faderSprite.setAlpha(alpha);
			faderSprite.draw(batch);
			batch.end();
		batch.begin();
		if(clockCount.getMeterReading() < 1){
			player.setAnimation(PreGamePlay.playerDeadAnimation);
					switchScreen(batch);
		}else if(player.getPosition().y < -10){
			player.setAnimation(PreGamePlay.playerDeadAnimation);
			switchScreen(batch);
			
		}else if( player.isGameOverCollision()){
			player.setAnimation(PreGamePlay.playerDeadAnimation);
			switchScreen(batch);
			
		}
		batch.end();
			if(state==State.PAUSED) {
				batch.begin();
				fontRenderer.render(batch, " ", 0, 0); // this is dummy drawing
				batch.end();
				pauseMenuRender();
			}
		}
	
		}
		else {
			Gdx.gl.glClearColor(0, 0, 0, 1);
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
			checkStart();
			batch.begin();
			fontRenderer.render(batch, "TAP TO START", FontRenderer.CAMWIDTH/3, FontRenderer.CAMHEIGHT/2);
			//batch.draw(img,0, 0,screenWidth,screenHeight);
			//batch.draw(start, 0, 0);
			batch.end();
		}

	}

	private void pauseMenuRender() {
		
		float tempX = (FontRenderer.CAMWIDTH - AssetLoader.promptBG.getWidth())/2;
		float tempY = (FontRenderer.CAMHEIGHT - AssetLoader.promptBG.getHeight())/2;
		pause.setPosition(screenWidth/2.75f,screenHeight/3f);
		pause.setSize(50, 50);
		exit.setPosition(screenWidth/1.90f,screenHeight/3f);
		batch.begin();
		batch.draw(AssetLoader.pauseBG,tempX,tempY,AssetLoader.promptBG.getWidth(),AssetLoader.promptBG.getHeight());
		batch.end();
		pause.render(batch, camera);
		exit.render(batch, camera);
		musicEntity.render(batch, camera);
		if(exit.isPressed()) {
			if(music.isPlaying()) music.stop();
			catchStars.setScreen(catchStars.getMainScreen());
		}
		
		if(musicEntity.isPressed()){
			if(saveGame.isMusicOn()) {
				saveGame.setMusicOn(false);
				catch_d_stars.isMusicOn = false;
				if(music.isPlaying()) music.stop();
			}
			else{
				saveGame.setMusicOn(true);
				catch_d_stars.isMusicOn = true;
				if(!music.isPlaying()) music.play();
			}
			Files.saveGameStatus(saveGame);
		}
		if(saveGame.isMusicOn()) {
			musicEntity.setDefaultTexture(AssetLoader.musicEnabled);
		}
		else {
			musicEntity.setDefaultTexture(AssetLoader.musicDisabled);
		}
		
	}
	@Override
	public void resize(int width, int height) {
		viewPort.update(width, height, true);
	}
	
	@Override
	public void hide() {
		//dispose();		
	}

	@Override
	public void pause() {
		this.state = State.PAUSED;
		
	}

	@Override
	public void resume() {
		if(pause.isPressed()) {
		pause.setPosition(screenWidth-60,10);
		pause.setSize(65, 65);
		this.state = State.RUNNING;
		}
		
	}

	@Override
	public void dispose() {
		
		//fontRenderer.dispose();
//		shape.dispose();
		batch.dispose();
		shoot.dispose();
		pause.dispose();
		exit.dispose();
		scrollHandler.dipose();
		System.out.println("DEBUG  = DISPOSE CALLED");
		clockCount = null;
		magnetBar =  null;
		superPowerBar = null;
		camera = null;
		music.dispose();
		player.dispose();
		viewPort = null;
		musicEntity.dispose();
		faderEntity.disposeEntity();
		faderTexture.dispose();
		faderSprite = null;
		saveGame = null;
		System.gc();
	}
	
	public catch_d_stars getGameController(){return catchStars;}
	public GUIEntity getShootEntity(){return shoot;}
	
	public void setInputs() {
		input = new InputHandler(player,this);
		gesture = new GestureHandler(player,this);
		gesture1 = new GestureDetector(gesture);
		multiplexer = new InputMultiplexer();
		multiplexer.addProcessor(gesture1);
		multiplexer.addProcessor(input);
		
		Gdx.input.setInputProcessor(multiplexer);
		
	}
	
	public Player getPlayer(){return player;}
	
public void checkStart() {
		
		if(Gdx.input.isButtonPressed(Input.Buttons.LEFT))  {
			if(!isPlaying) {
				isPlaying=true;
				System.out.println(isPlaying);
			}
			player.onClick();
		}
	}
}
