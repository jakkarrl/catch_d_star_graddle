package com.jakkarrlgames.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.utils.FirstPersonCameraController;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.jakkarrlgames.assetloader.AssetLoader;
import com.jakkarrlgames.entity.GUIEntity;
import com.jakkarrlgames.helperclass.SaveGame;
import com.jakkarrlgames.persistant.Files;
import com.jakkarrlgames.store.Category;
import com.jakkarrlgames.store.Item;
import com.jakkarrlgames.store.Store;
import com.jakkarrlgames.store.StoreAssets;

public class DemoDialog implements Screen{

	private catch_d_stars 	game;
	private BitmapFont 		font;
	private SpriteBatch 	batch;
	private Store 			store;
	private Viewport 		viewPort;
	private Category 		powerUp;
	private Category 		character;
	private Category 		category;
	private Item 			powerUp1;
	private Item 			char1;
	private Item 			char2;
	
	private GUIEntity 		itemLeftTouch;
	private GUIEntity 		itemRightTouch;
	private GUIEntity 		categoryLeftTouch;
	private GUIEntity 		categoryRightTouch;
	private GUIEntity 		goBack;
	private GUIEntity		select;
	private Item 			item;
	private float 			width;
	private float 				height;
	private Texture 			storBackground;
	private OrthographicCamera  cam;
	
	
	private String selectedOutput = "";
	
	//To display the SavedGame Stats
	private SaveGame saveGameFromFile;
	private String saveGameStats;
	private float statsX =100,statsY = Gdx.graphics.getHeight() - 50;
	
//	private ConfirmDialog dialog;
	String currentCategory;
	String currentItem;
	private int pointerToCategory,pointerToItem;
	
	Stage stage;
	Skin skin;
	
public DemoDialog(catch_d_stars game) {
		
		this.game = game;
	}
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		
		update(delta);
		batch.setProjectionMatrix(cam.combined);
		
		batch.begin();
		batch.draw(storBackground, 0, 0,width,height); 
		font.draw(batch, selectedOutput,(width/2) - (selectedOutput.length() * 2), (height/2));
		font.draw(batch, saveGameStats,statsX,statsY);
		
		//Display the CItem and CheckAMount
		if(!item.isLocked() ){
			
		}else{
			font.draw(batch, "Need " + item.getCheckAmount() + item.getCItem() + " to use this.", 150, 100);
		}
//		
		category.draw(batch,font,(width/2-width/10),height/4,width/4,height/4);
		item.draw(batch,font,(width/2-width/10),height/2,width/4,height/4);
//		
		batch.end();
		renderGUI();
		stage.act();
		stage.draw();
	}

	private void renderGUI() {
		select.setPosition(width-60,5);
		select.setSize(50, 50);
		select.render(batch, cam);
	
		goBack.setPosition(10, 5);
		goBack.setSize(50, 50);
		goBack.render(batch, cam);
		if(pointerToItem > 0){
			itemLeftTouch.setPosition(width/4,height/1.8f);
			itemLeftTouch.setSize(width/10.24f,height/7.68f);
			itemLeftTouch.render(batch, cam);
		}
		
		if(pointerToItem < category.getItems().size()-1){
			itemRightTouch.setPosition((width*.65f),height/1.8f);
			itemRightTouch.setSize(width/10.24f,height/7.68f);
			itemRightTouch.render(batch, cam);
		}
		
		if(pointerToCategory > 0){
			categoryLeftTouch.setPosition(width/3.5f,height/4);
			categoryLeftTouch.setSize(width/12.8f, height/9.6f);
			categoryLeftTouch.render(batch, cam);
		}
		
		if(pointerToCategory < store.getMaximumCategory()-1 ){
			categoryRightTouch.setPosition((width*.62f),height/4);
			categoryRightTouch.setSize(width/12.8f, height/9.6f);
			categoryRightTouch.render(batch, cam);
		}
	
		
	}
	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		//Load StoreAsset
		StoreAssets.loadAsset();
		
		width = Gdx.graphics.getWidth();
		height= Gdx.graphics.getHeight();
				
//		selectedItem = "None";
		currentItem = "None";
		store = new Store(2);
		storBackground =  new Texture(Gdx.files.internal("data/images/background/storeBack.png"));
		itemLeftTouch = new GUIEntity(AssetLoader.left,AssetLoader.left);
		itemRightTouch = new GUIEntity(AssetLoader.right,AssetLoader.right);
		
		categoryLeftTouch = new GUIEntity(AssetLoader.left,AssetLoader.left);
		categoryRightTouch = new GUIEntity(AssetLoader.right,AssetLoader.right);
		goBack = new GUIEntity(AssetLoader.back,AssetLoader.back);
		select = new GUIEntity(AssetLoader.select,AssetLoader.select);
		
		
		powerUp    	= new Category("PowerUp",AssetLoader.powerupText);
		character  	= new Category("Character",AssetLoader.characterText);
		
//		Json json = new Json();
//		FileHandle file = Gdx.files.local("bin/data/jsonFiles/Powerups/powerUp1.json");
//		Item itm = json.fromJson(Item.class, file);
//		itm.setTexture(StoreAssets.powerUp1);
//		powerUp1    = itm;
		powerUp1 = Files.getPowerUp1();
		powerUp1.setTexture(AssetLoader.storeSuperPower);
		
//		file = Gdx.files.local("bin/data/jsonFiles/Powerups/powerUp2.json");
//		itm = json.fromJson(Item.class, file);
//		itm.setTexture(StoreAssets.powerUp2);
//		powerUp2	= itm;
//		powerUp2 = Files.getPowerUp2();
//		powerUp2.setTexture(StoreAssets.powerUp2);
		
//		file = Gdx.files.local("bin/data/jsonFiles/Characters/ninjaGirl.json");
//		itm = json.fromJson(Item.class, file);
//		itm.setTexture(StoreAssets.ninjaGirl);
//		char1		= itm;
		char1 = Files.getNinjaGirl();
		char1.setTexture(AssetLoader.storeGirl);
		
//		file = Gdx.files.local("bin/data/jsonFiles/Characters/cowBoy.json");
//		itm = json.fromJson(Item.class, file);
//		itm.setTexture(StoreAssets.cowBoy);
//		char2		= new Item("Boy",StoreAssets.cowBoy,true);
		char2 = Files.getCowBoy();
		char2.setTexture(AssetLoader.storeBoy);
		
		store.addCategory(powerUp);
		store.addCategory(character);
		powerUp.addItem(powerUp1);
//		powerUp.addItem(powerUp2);
		
		character.addItem(char2);
		character.addItem(char1);
		font  		= new BitmapFont();
		batch 		= new SpriteBatch();
		pointerToItem = 0;
		pointerToCategory = 0;
		cam = new OrthographicCamera();
		
		cam.setToOrtho(false,width,height);
	
		// TODO Auto-generated method stub
		stage = new Stage(new FitViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
		skin = new Skin(Gdx.files.internal("data/skin/uiskin.json"));
		new Dialog("Demo", skin){
			@Override
			protected void result(Object object) {
				System.out.println("Dialog");
			}
		}.text("Demo").button("Yes", true).button("No", false).show(stage).toFront();
		
		saveGameFromFile = Files.getSaveGame();
		saveGameStats = "Star : " + saveGameFromFile.getStar() + ",Candy : " + saveGameFromFile.getCandy() + ",Coin : " + saveGameFromFile.getCoin() + ",CupCake : " + saveGameFromFile.getCupCake() + ",Fruit : " + saveGameFromFile.getFruit() + ", SuperPower : " + saveGameFromFile.getSuperPowerCount();
	}
	
	public void update(float delta) {

//		stage.act(delta);
		category = store.getCategoryList().get(pointerToCategory);
		item = category.getItems().get(pointerToItem);
		currentCategory = category.getName();
		currentItem = item.getName();
		
		checkTouches();
		
		//select or Buy
		if(!item.isLocked() && currentCategory != "PowerUp"){
			select.setDefaultTexture(StoreAssets.select);
		}else{
			select.setDefaultTexture(StoreAssets.buy);
		}
		
	}
	
	private void checkTouches() {
		//update for Touch < > select and back button
			
			
			if(select.isPressed()) {
				selectedOutput = "";
				
				switch(currentCategory){
					case "Character":selectCharacter();break;
					case "PowerUp":selectPowerUP();break;
				}
			}
			
			if(goBack.isPressed()) {
				selectedOutput = "";
				game.setScreen(new MainScreen(game));
			}
			
			if(categoryRightTouch.isPressed()){
				selectedOutput = "";
				if(pointerToCategory < store.getMaximumCategory()-1){
					pointerToCategory++;
					pointerToItem = 0;
				}
			}
			
			if(categoryLeftTouch.isPressed()){
				selectedOutput = "";
				if(pointerToCategory > 0){
					pointerToCategory--;
					pointerToItem = 0;
				}
			}
			
			//----------
			
			if(itemRightTouch.isPressed()){
				selectedOutput = "";
//				System.out.print(category.getItems().size());
				if(pointerToItem != category.getItems().size()-1){
					pointerToItem++;
				}
			}
			
			if(itemLeftTouch.isPressed()){
				selectedOutput = "";
				if(pointerToItem > 0){
					pointerToItem--;
				}
			}
			
		}
	
private void selectCharacter(){
		
		Json json = new Json();
		FileHandle file = Gdx.files.local(("bin/sampleJson.json"));
//		SaveGame saveFromFile = json.fromJson(SaveGame.class, file);
		
		
		if(!item.isLocked()){
			SaveGame saveToFile = saveGameFromFile;			
			saveToFile.setCharacter(currentItem);
			file.writeString(json.toJson(saveToFile),false);
			game.setSaveGame(saveToFile);
			selectedOutput = "Selected Character : " + currentItem;
		}else{
			buyItem(file);
		}
		
//		System.out.println("Selected Character : " + currentItem);
		
	}


private void selectPowerUP(){
//	System.err.println("Sorry powerup inum start e panala :D");
//	selectedOutput = "Selected Category is under implementation";
	Json json = new Json();
	if(!item.isLocked()){
			FileHandle file = Gdx.files.local("bin/data/jsonFiles/Powerups/powerUp1.json");
			FileHandle file2 = Gdx.files.local(("bin/sampleJson.json"));
			buyItem(file);
			
			SaveGame saveToFile = saveGameFromFile;
			saveToFile.setSuperPowerCount((saveToFile.getSuperPowerCount() + 1));
			file2.writeString(json.toJson(saveToFile),false);
			game.setSaveGame(saveToFile);
			saveGameFromFile = Files.getSaveGame();
			selectedOutput = "Successfully Super Power is added";
			saveGameStats = "Star : " + saveGameFromFile.getStar() + ",Candy : " + saveGameFromFile.getCandy() + ",Coin : " + saveGameFromFile.getCoin() + ",CupCake : " + saveGameFromFile.getCupCake() + ",Fruit : " + saveGameFromFile.getFruit() + ", SuperPower : " + saveGameFromFile.getSuperPowerCount();
	}else{
		selectedOutput = "Selected Item is not available to Buy";
	}
}


public void buyItem(FileHandle file){
	Json json = new Json();
	
	int checkAmount = item.getCheckAmount();
	int availableAmount = saveGameFromFile.getValueOf(item.getCItem());
	System.out.println("Item Name : " + item.getCItem());
	System.out.println("CheckAmount : " + checkAmount + "  - availableAmount : " + availableAmount);
	if(availableAmount >= checkAmount){
		int amount = availableAmount - checkAmount;
		SaveGame saveGameToFile = saveGameFromFile;
		saveGameToFile.setValueOf(item.getCItem(), amount);
		file.writeString(json.toJson(saveGameToFile), false);
		game.setSaveGame(saveGameToFile);
		saveGameFromFile = Files.getSaveGame();
		saveGameStats = "Star : " + saveGameFromFile.getStar() + ",Candy : " + saveGameFromFile.getCandy() + ",Coin : " + saveGameFromFile.getCoin() + ",CupCake : " + saveGameFromFile.getCupCake() + ",Fruit : " + saveGameFromFile.getFruit() + ", SuperPower : " + saveGameFromFile.getSuperPowerCount();
		
		
		//item
		item.setLock(false);
		FileHandle itemFile = Files.getFileOf(item.getName());
		itemFile.writeString(json.toJson(item),false);
		selectedOutput = "Successfully Unlocked the file";
	}else{
		selectedOutput = "You dont have enough " + item.getCItem() + " to buy this item";
	}
		
}


	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
