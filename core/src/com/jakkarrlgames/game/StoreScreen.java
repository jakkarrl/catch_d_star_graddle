package com.jakkarrlgames.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.utils.FirstPersonCameraController;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.jakkarrlgames.assetloader.AssetLoader;
import com.jakkarrlgames.entity.FontRenderer;
import com.jakkarrlgames.entity.GUIEntity;
import com.jakkarrlgames.helperclass.SaveGame;
import com.jakkarrlgames.persistant.Files;
import com.jakkarrlgames.store.Category;
import com.jakkarrlgames.store.Item;
import com.jakkarrlgames.store.Store;
import com.jakkarrlgames.store.StoreAssets;

public class StoreScreen implements Screen{

	private catch_d_stars 	game;
//	private BitmapFont 		font;
	private FontRenderer 	fontRenderer;
	private SpriteBatch 	batch;
	private Store 			store;
	private Viewport 		viewPort;
	private Category 		powerUp;
	private Category 		character;
	private Category 		category;
	private Item 			powerUp1;
	private Item 			powerUp2;
	private Item 			char1;
	private Item 			char2;
	
	private GUIEntity 		itemLeftTouch;
	private GUIEntity 		itemRightTouch;
	private GUIEntity 		categoryLeftTouch;
	private GUIEntity 		categoryRightTouch;
	private GUIEntity 		goBack;
	private GUIEntity		select;
	private Item 			item;
	private float 			width;
	private float 				height;
	private OrthographicCamera  cam;

	
	
	private String selectedOutput = "";
	int star,candy,coin,cupcake,fruit,superPower,clockPower;
	
	//To display the SavedGame Stats
	private SaveGame saveGameFromFile;
	private String saveGameStats;
	private float statsX =100,statsY = Gdx.graphics.getHeight() - 50;
	
//	private ConfirmDialog dialog;
	String currentCategory;
	String currentItem;
	private int pointerToCategory,pointerToItem;
	
	Stage stage;
	Skin skin;
	
	private ShapeRenderer lineShape;
	
public StoreScreen(catch_d_stars game) {
		
		this.game = game;
	}
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		
		update(delta);
		batch.setProjectionMatrix(cam.combined);
		
		batch.begin();
		batch.draw(AssetLoader.storeBackground, 0, 0,width,height);
		fontRenderer.font.setColor(Color.BLACK);
		//fontRenderer.font.setScale(1f);
		fontRenderer.render(batch, selectedOutput,(FontRenderer.CAMWIDTH/2) - (selectedOutput.length() * 2), (FontRenderer.CAMHEIGHT/2));
		
		
		
//		font.draw(batch, saveGameStats,statsX,statsY);
		fontRenderer.render(batch, "Star 		: " + star, 10,FontRenderer.CAMHEIGHT - 50);
		fontRenderer.render(batch, "Candy 	: " + candy, 10,FontRenderer.CAMHEIGHT - 100);
		fontRenderer.render(batch, "Coin	 	: " + coin, 10,FontRenderer.CAMHEIGHT - 150 );
		fontRenderer.render(batch, "CupCake	: " + cupcake, 10,FontRenderer.CAMHEIGHT- 200);
		fontRenderer.render(batch, "Fruit 	: " + fruit, 10,FontRenderer.CAMHEIGHT- 250);
		fontRenderer.render(batch, "SuperPower: " + superPower, 10,FontRenderer.CAMHEIGHT- 300);
		fontRenderer.render(batch, "ClockPower: " + clockPower, 10,FontRenderer.CAMHEIGHT- 350);
		
		fontRenderer.font.setColor(Color.PURPLE);
		fontRenderer.font.setScale(1.25f);
		fontRenderer.render(batch, "---> Store <---", FontRenderer.CAMWIDTH/2, FontRenderer.CAMHEIGHT - 20);
		//Display the CItem and CheckAMount
		if(item.isLocked() || currentCategory == "PowerUp"){
			fontRenderer.render(batch, "Need " + item.getCheckAmount() + item.getCItem(),50,250); 
			fontRenderer.render(batch," to use this.", 50, 200);
		}else{
			
		}
		
		category.draw(batch,fontRenderer.font,(FontRenderer.CAMWIDTH/2),FontRenderer.CAMHEIGHT/1.8f,FontRenderer.CAMWIDTH/4,FontRenderer.CAMHEIGHT/8);
		item.draw(batch,fontRenderer.font,(FontRenderer.CAMWIDTH/2),FontRenderer.CAMHEIGHT/4,FontRenderer.CAMWIDTH/4,FontRenderer.CAMHEIGHT/4);
//		
		batch.end();
		renderGUI();
		stage.act();
		stage.draw();
		
		fontRenderer.font.setColor(Color.BLACK); 
		fontRenderer.font.setScale(1f);
		
		lineShape.begin(ShapeType.Line);
		lineShape.setColor(Color.RED);
		lineShape.rect(width/3.5f, height, 1, -height);
		lineShape.end();
		
		
	}

	private void renderGUI() {
		select.setSize(50, 50);
		select.setPosition(width-70,25);
		select.render(batch, cam);
	
		goBack.setPosition(10, 5);
		goBack.setSize(50, 50);
		goBack.render(batch, cam);
		if(pointerToItem > 0){
			itemLeftTouch.setPosition(width/2.85f,height/4);
			itemLeftTouch.setSize(width/12.8f, height/9.6f);
			itemLeftTouch.render(batch, cam);
		}
		
		if(pointerToItem < category.getItems().size()-1){
			itemRightTouch.setPosition((width/1.25f),height/4);
			itemRightTouch.setSize(width/12.8f, height/9.6f);
			itemRightTouch.render(batch, cam);
		}
		
		if(pointerToCategory > 0){
			categoryLeftTouch.setPosition(width/2.85f,height/1.8f);
			categoryLeftTouch.setSize(width/10.24f,height/7.68f);
			categoryLeftTouch.render(batch, cam);
		}
		
		if(pointerToCategory < store.getMaximumCategory()-1 ){
			categoryRightTouch.setPosition((width/1.25f),height/1.8f);
			categoryRightTouch.setSize(width/10.24f,height/7.68f);
			categoryRightTouch.render(batch, cam);
		}
	
		
	}
	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		//Load StoreAsset
		StoreAssets.loadAsset();
		
		width = Gdx.graphics.getWidth();
		height= Gdx.graphics.getHeight();
				
//		selectedItem = "None";
		currentItem = "None";
		store = new Store(2);
		itemLeftTouch = new GUIEntity(AssetLoader.left,AssetLoader.leftPressed);
		itemRightTouch = new GUIEntity(AssetLoader.right,AssetLoader.rightPressed);
		
		categoryLeftTouch = new GUIEntity(AssetLoader.left,AssetLoader.leftPressed);
		categoryRightTouch = new GUIEntity(AssetLoader.right,AssetLoader.rightPressed);
		goBack = new GUIEntity(AssetLoader.back,AssetLoader.backPressed);
		select = new GUIEntity(AssetLoader.select,AssetLoader.selectPressed);
		
		
		powerUp    	= new Category("PowerUp",AssetLoader.powerupText);
		character  	= new Category("Character",AssetLoader.characterText);

		powerUp1 = Files.getPowerUp1();
		powerUp1.setTexture(AssetLoader.storeSuperPower);
//		powerUp1.setDescription("Super power to escape from getting out");
		
		powerUp2 = Files.getPowerUp2();
		powerUp2.setTexture(AssetLoader.storeClock);
//		powerUp2.setDescription("Key to restore after death");
		
		char1 = Files.getNinjaGirl();
		char1.setTexture(AssetLoader.storeGirl);
//		char1.setDescription("ninjaGirl");
		
		char2 = Files.getCowBoy();
		char2.setTexture(AssetLoader.storeBoy);
//		char2.setDescription("CowBoy");
		
		store.addCategory(powerUp);
		store.addCategory(character);
		powerUp.addItem(powerUp1);
		powerUp.addItem(powerUp2);

		
		character.addItem(char2);
		character.addItem(char1);
//		font  		= new BitmapFont();
//		font.setColor(Color.BLACK);
//		
		fontRenderer  = game.getObject().getFontRenderer();
		fontRenderer.setFonts("data/font/SF Slapstick Comic.ttf", 30);
		fontRenderer.font.setColor(Color.BLACK);
		
		batch 		= game.getObject().getBatch();
		pointerToItem = 0;
		pointerToCategory = 0;
		cam = game.getObject().getCam();
		
		cam.setToOrtho(false,width,height);
	
		lineShape = game.getObject().getLineShape();
		// TODO Auto-generated method stub
		stage = game.getObject().getStage();
		skin = game.getObject().getSkin();
		Gdx.input.setInputProcessor(stage);
		
		saveGameFromFile = Files.getSaveGame();
//		saveGameStats = "Star : " + saveGameFromFile.getStar() + ",\n Candy : " + saveGameFromFile.getCandy() + ",Coin : " + saveGameFromFile.getCoin() + ",CupCake : " + saveGameFromFile.getCupCake() + ",Fruit : " + saveGameFromFile.getFruit() + ", SuperPower : " + saveGameFromFile.getSuperPowerCount();
		
		star =  saveGameFromFile.getStar();
		candy = saveGameFromFile.getCandy(); 
		coin = saveGameFromFile.getCoin();
		cupcake = saveGameFromFile.getCupCake(); 
		fruit = saveGameFromFile.getFruit(); 
		superPower = saveGameFromFile.getSuperPowerCount();
		clockPower = saveGameFromFile.getClockPowerCount();
	}
	
	public void update(float delta) {

//		stage.act(delta);
		category = store.getCategoryList().get(pointerToCategory);
		item = category.getItems().get(pointerToItem);
		currentCategory = category.getName();
		currentItem = item.getName();
		
		checkTouches();
		
		//select or Buy
		if(!item.isLocked() && currentCategory != "PowerUp"){
			select.setDefaultTexture(StoreAssets.select);
		}else{
			select.setDefaultTexture(StoreAssets.buy);
		}
		
	}
	
	private void checkTouches() {
		//update for Touch < > select and back button
			
			
			if(select.isPressed()) {
				selectedOutput = "";
				
				switch(currentCategory){
					case "Character":selectCharacter();break;
					case "PowerUp":selectPowerUP();break;
				}
			}
			
			if(goBack.isPressed()) {
				selectedOutput = "";
//				game.setScreen(new MainScreen(game));
				game.setScreen(game.getMainScreen());
			}
			
			if(categoryRightTouch.isPressed()){
				selectedOutput = "";
				if(pointerToCategory < store.getMaximumCategory()-1){
					pointerToCategory++;
					pointerToItem = 0;
				}
			}
			
			if(categoryLeftTouch.isPressed()){
				selectedOutput = "";
				if(pointerToCategory > 0){
					pointerToCategory--;
					pointerToItem = 0;
				}
			}
			
			//----------
			
			if(itemRightTouch.isPressed()){
				selectedOutput = "";
				if(pointerToItem != category.getItems().size()-1){
					pointerToItem++;
				}
			}
			
			if(itemLeftTouch.isPressed()){
				selectedOutput = "";
				if(pointerToItem > 0){
					pointerToItem--;
				}
			}
			
		}
	
	private void selectCharacter(){
		
		Json json = new Json();
		FileHandle file = Gdx.files.local(("bin/sampleJson.json"));
		
		if(!item.isLocked()){
			SaveGame saveToFile = saveGameFromFile;			
			saveToFile.setCharacter(currentItem);
			file.writeString(json.toJson(saveToFile),false);
			game.setSaveGame(saveToFile);
			displayDialog("Character", "Selected Character : " + currentItem, "Ok");
		}else{
			displayDialog("Buy Item", "Sure do you want to buy this item ?", "Yes", "No",file);
		}
		
	}


	private void selectPowerUP(){
		
		Json json = new Json();
		if(currentItem.equals("PowerUp1")){
			if(!item.isLocked()){
				FileHandle file = Gdx.files.local("bin/data/jsonFiles/Powerups/powerUp1.json");
				FileHandle file2 = Gdx.files.local(("bin/sampleJson.json"));
				displayDialog("Buy Item", "Sure do you want to buy this item ?", "Yes", "No",file2);
//				SaveGame saveToFile = saveGameFromFile;
//				saveToFile.setSuperPowerCount((saveToFile.getSuperPowerCount() + 1));
//				file2.writeString(json.toJson(saveToFile),false);
//				game.setSaveGame(saveToFile);
//				saveGameFromFile = Files.getSaveGame();
//				saveGameStats = "Star : " + saveGameFromFile.getStar() + ",Candy : " + saveGameFromFile.getCandy() + ",Coin : " + saveGameFromFile.getCoin() + ",CupCake : " + saveGameFromFile.getCupCake() + ",Fruit : " + saveGameFromFile.getFruit() + ", SuperPower : " + saveGameFromFile.getSuperPowerCount();
			}else{
				displayDialog("Oooopppssss", "Selected Item is not available to Buy", "Ok");
			}
		}else if(currentItem.equals("PowerUp2")){
			FileHandle file = Gdx.files.local("bin/data/jsonFiles/Powerups/powerUp2.json");
			FileHandle file2 = Gdx.files.local(("bin/sampleJson.json"));
			displayDialog("Buy Item", "Sure do you want to buy this item ?", "Yes", "No",file2);
		}
		
	}


	public void buyItem(FileHandle file){
		Json json = new Json();
	
		int checkAmount = item.getCheckAmount();
		int availableAmount = saveGameFromFile.getValueOf(item.getCItem());
	//	System.out.println("Item Name : " + item.getCItem());
		//System.out.println("CheckAmount : " + checkAmount + "  - availableAmount : " + availableAmount);
		if(availableAmount >= checkAmount){
		int amount = availableAmount - checkAmount;
		SaveGame saveGameToFile = saveGameFromFile;
		saveGameToFile.setValueOf(item.getCItem(), amount);
		if(currentCategory.toLowerCase().equals("powerup")){
			if(currentItem.toLowerCase().equals("powerup1")){
				saveGameToFile.setSuperPowerCount((saveGameToFile.getSuperPowerCount() + 1));
			}else if(currentItem.toLowerCase().equals("powerup2")){
				saveGameToFile.setClockPowerCount((saveGameToFile.getClockPowerCount() + 1));
			}
		}
		file.writeString(json.toJson(saveGameToFile), false);
		game.setSaveGame(saveGameToFile);
		saveGameFromFile = Files.getSaveGame();
//		saveGameStats = "Star : " + saveGameFromFile.getStar() + ",\nCandy : " + saveGameFromFile.getCandy() + ",\nCoin : " + saveGameFromFile.getCoin() + ",\nCupCake : " + saveGameFromFile.getCupCake() + ",\nFruit : " + saveGameFromFile.getFruit() + ",\nSuperPower : " + saveGameFromFile.getSuperPowerCount();
		
		star =  saveGameFromFile.getStar();
		candy = saveGameFromFile.getCandy(); 
		coin = saveGameFromFile.getCoin();
		cupcake = saveGameFromFile.getCupCake(); 
		fruit = saveGameFromFile.getFruit(); 
		superPower = saveGameFromFile.getSuperPowerCount();
		clockPower = saveGameFromFile.getClockPowerCount();
		
		//item
		item.setLock(false);
		FileHandle itemFile = Files.getFileOf(item.getName());
		itemFile.writeString(json.toJson(item),false);
		displayDialog("Unlocked Successfully", "The Selected Item is unlocked successfully", "Ok");
		}else{
			displayDialog("Ooooopppsss!!!", "Sorry You dont have enough " + item.getCItem() + " to buy this item", "Ok");
		}
	}

	public void displayDialog(String title,String str,String Ok){
		new Dialog(title, skin){
			@Override
			protected void result(Object object) {
				System.out.println(object);
			}
		}.text(str).button("   OK   ",true).show(stage).toFront();
	}

	public void displayDialog(String title,String str,String yes,String no,final FileHandle file){
		new Dialog(title, skin){
			@Override
			protected void result(Object object) {
				System.out.println(object);
				if(object.equals(true)){
					buyItem(file);
				}
			}
		}.text(str).button("   Yes   ",true).button("   No   ",false).show(stage).toFront();
	}

	@Override
	public void hide() {	
		dispose();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {

	}

}
