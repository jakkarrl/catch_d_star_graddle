package com.jakkarrlgames.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.jakkarrlgames.assetloader.AssetLoader;
import com.jakkarrlgames.entity.GUIEntity;

public class Help extends InputAdapter implements Screen{
	
	private SpriteBatch batch;
	private Texture hint[];
	private float x=0,y=0;
	private catch_d_stars catchdstars;
	private float touchDown,touchUp;
	private String side = "";
	private int pointer = 0;
	private GUIEntity back,forward,left,right;
	private OrthographicCamera camera;
	public Help(catch_d_stars catchdstars){this.catchdstars = catchdstars;}
	
	
	@Override
	public void show() {
//		camera = new OrthographicCamera(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
//		camera.setToOrtho(false);
//		batch = new SpriteBatch();
//		hint = new Texture[6];
//		hint[0] = new Texture(Gdx.files.internal("data/images/help/hint1.png"));
//		hint[1] = new Texture(Gdx.files.internal("data/images/help/hint2.png"));
//		hint[2] = new Texture(Gdx.files.internal("data/images/help/hint3.png"));
//		hint[3] = new Texture(Gdx.files.internal("data/images/help/hint4.png"));
//		hint[4] = new Texture(Gdx.files.internal("data/images/help/hint5.png"));
//		hint[5] = new Texture(Gdx.files.internal("data/images/help/hint6.png"));
//		back = new GUIEntity(AssetLoader.back,AssetLoader.backPressed);
//		back.setSize(50, 50);
//		back.setPosition(10, 10);
//		forward = new GUIEntity(AssetLoader.playNowText,AssetLoader.playNowPressedText);
//		forward.setSize(80, 50);
//		forward.setPosition(Gdx.graphics.getWidth()  - 90, 10);
//		
//		left = new GUIEntity(AssetLoader.left,AssetLoader.leftPressed);
//		left.setSize(50, 50);
//		left.setPosition(10, Gdx.graphics.getHeight()/2);
//		
//		right = new GUIEntity(AssetLoader.right,AssetLoader.rightPressed);
//		right.setSize(50, 50);
//		right.setPosition(Gdx.graphics.getWidth() - 70, Gdx.graphics.getHeight()/2);
		
		camera = catchdstars.getObject().getCam();
		camera.setToOrtho(false);
		batch = catchdstars.getObject().getBatch();
		hint = AssetLoader.helpScreen;
		
		back = new GUIEntity(AssetLoader.back,AssetLoader.backPressed);
		back.setSize(50, 50);
		back.setPosition(10, 10);
		forward = new GUIEntity(AssetLoader.playNowText,AssetLoader.playNowPressedText);
		forward.setSize(80, 50);
		forward.setPosition(Gdx.graphics.getWidth()  - 90, 10);
		
		left = new GUIEntity(AssetLoader.left,AssetLoader.leftPressed);
		left.setSize(50, 50);
		left.setPosition(10, Gdx.graphics.getHeight()/2);
		
		right = new GUIEntity(AssetLoader.right,AssetLoader.rightPressed);
		right.setSize(50, 50);
		right.setPosition(Gdx.graphics.getWidth() - 70, Gdx.graphics.getHeight()/2);

		
	}

	public void update(float delta){
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		Gdx.input.setInputProcessor(this);
		
		if(back.isPressed()){
			catchdstars.setScreen(catchdstars.getMainScreen());
		}
		
		if(forward.isPressed()){
			catchdstars.setScreen(catchdstars.getPreGame());
		}
		
		if(left.isPressed()){
			System.out.println("leftttt");
			if(pointer > 0) pointer--;
		}
		
		if(right.isPressed()){
			System.out.println("righttt");
			if(pointer < (hint.length-1)) pointer++;
		}
		System.out.println(pointer);
	}
	@Override
	public void render(float delta) {
		update(delta);
		batch.begin();
		batch.draw(hint[pointer],x,y,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		batch.end();
		
		back.render(batch, camera);
		forward.render(batch, camera);
		left.render(batch, camera);
		right.render(batch, camera);
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		hint[1].dispose();
		hint[2].dispose();
		hint[3].dispose();
		hint[4].dispose();
		hint[5].dispose();
		hint[0].dispose();
		batch.dispose();
		
	}
	
	
	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
//		System.out.println("ScreenX --> " + screenX );
//		System.out.println("ScreenY --> " + screenY );
//		System.out.println("poiter --> " + pointer );
		return true;
	}
	
	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		touchDown = screenX;
		return false;
	}
	
	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
//		if(touchDown < screenX){
//			//leftside
//			side = "left";
//			System.out.println("Lesser than");
//		}else if(touchDown > screenX){
//			//rightside
//			side = "right";
//			System.out.println("Greater than");
//		}else{
//			System.out.println("condition failed");
//		}
		return false;
	}

}
