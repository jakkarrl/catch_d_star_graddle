package com.jakkarrlgames.game;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Json;
import com.jakkarrlgames.assetloader.AssetLoader;
import com.jakkarrlgames.entity.FontRenderer;
import com.jakkarrlgames.entity.Player;
import com.jakkarrlgames.helperclass.SaveGame;
import com.jakkarrlgames.persistant.Files;
import com.jakkarrlgames.scroller.word.Word;

public class GameOut implements Screen {
	private catch_d_stars catchStar;
	private SpriteBatch batch;
	private FontRenderer[] fontRenderer;
	private SaveGame saveFromFile;
	private boolean setScore = false,highScore = false;
	public GameOut(catch_d_stars catchStar){
		this.catchStar = catchStar;
		batch = new SpriteBatch();
//		fontRenderer = catchStar.getObject().getFontArray();
//		fontRenderer[0] =  new FontRenderer("data/font/SF Slapstick Comic.ttf",30);
//		fontRenderer[1] =  new FontRenderer("data/font/SF Slapstick Comic.ttf",30);
//		fontRenderer[2] =  new FontRenderer("data/font/SF Slapstick Comic.ttf",30);
//		fontRenderer[3] =  new FontRenderer("data/font/SF Slapstick Comic.ttf",30);
//		fontRenderer[4] =  new FontRenderer("data/font/SF Slapstick Comic.ttf",30);
//		fontRenderer[5] =  new FontRenderer("data/font/SF Slapstick Comic.ttf",30);
//		fontRenderer[6] =  new FontRenderer("data/font/SF Slapstick Comic.ttf",30);
//		fontRenderer[7] =  new FontRenderer("data/font/SF Slapstick Comic.ttf",30);
//		for(int i = 0;i <7;i++) {
//			fontRenderer[i].font.setColor(Color.BLACK);
//		}
	}
	
	public void setFontArray(FontRenderer[] fontRenderer){
		this.fontRenderer = fontRenderer;
	}
	
	@Override
	public void show() {
		saveFromFile = Files.getSaveGame();
		
		
		
//		saveToFile = new SaveGame(0,0,0,0,0,0,"Boy",0,0,catch_d_stars.isMusicOn);
		
//		if(saveFromFile.getCharacter() != null ){
//			saveToFile.setCharacter(saveFromFile.getCharacter());
//		}else{
//			saveToFile.setCharacter("Boy");
//		}
		
//		if(Player.SCORE > saveFromFile.getScore()){
//			saveToFile.setScore(Player.SCORE);
//		}else{
//			saveToFile.setScore(saveFromFile.getScore());
//		}
		
		
//		if((saveFromFile.getStar() + Player.STAR) != 0)
//			saveToFile.setStar(saveFromFile.getStar() + Player.STAR);
//		else
//			saveToFile.setStar(0);
//		
//		if((saveFromFile.getCandy() + Player.CANDY)!=0)
//			saveToFile.setCandy(saveFromFile.getCandy() + Player.CANDY);
//		else
//			saveToFile.setCandy(0);
//		
//		if((saveFromFile.getCoin() + Player.COIN)!=0)
//			saveToFile.setCoin(saveFromFile.getCoin() + Player.COIN);
//		else
//			saveToFile.setCoin(0);
//			
//		if((saveFromFile.getCupCake() + Player.CUPCAKE)!=0)
//			saveToFile.setCupCake(saveFromFile.getCupCake() + Player.CUPCAKE);
//		else
//			saveToFile.setCupCake(0);
//		
//		if((saveFromFile.getFruit() + Player.FRUIT)!=0)
//			saveToFile.setFruit(saveFromFile.getFruit() + Player.FRUIT);
//		else
//			saveToFile.setFruit(0);
		
//		if((saveFromFile.getSuperPowerCount())!=0)
//			saveToFile.setSuperPowerCount(saveFromFile.getSuperPowerCount());
//		else
//			saveToFile.setSuperPowerCount(0);
//		
//		if((saveFromFile.getClockPowerCount())!=0)
//			saveToFile.setClockPowerCount(saveFromFile.getClockPowerCount());
//		else
//			saveToFile.setClockPowerCount(0);
//		
//		if(saveFromFile.isMusicOn() != catch_d_stars.isMusicOn)
//			saveToFile.setMusicOn(catch_d_stars.isMusicOn);
//		
		
		if(Player.SCORE > saveFromFile.getScore()){
			if(!setScore && isAllWordCollected()){Player.SCORE += 1000;setScore = true; }
			saveFromFile.setScore(Player.SCORE);
			highScore = true;
		}
		saveFromFile.setStar(saveFromFile.getStar() + Player.STAR);
		saveFromFile.setCandy(saveFromFile.getCandy() + Player.CANDY);
		saveFromFile.setCoin(saveFromFile.getCoin() + Player.COIN);
		saveFromFile.setCupCake(saveFromFile.getCupCake() + Player.CUPCAKE);
		saveFromFile.setFruit(saveFromFile.getFruit() + Player.FRUIT);
		
		FileHandle file = Gdx.files.local("bin/sampleJson.json");
		Json json = new Json();
		file.writeString(json.toJson(saveFromFile),false);
		saveFromFile = Files.getSaveGame();
//		catchStar.getGamePlay().dispose();
		System.gc();
	}
	
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		batch.begin();

		float tempX = (FontRenderer.CAMWIDTH - AssetLoader.promptBG.getWidth())/2;
		float tempY = (FontRenderer.CAMHEIGHT - AssetLoader.promptBG.getHeight())/2;
		
		batch.draw(AssetLoader.background, 0, 0,FontRenderer.CAMWIDTH,FontRenderer.CAMHEIGHT);
		batch.draw(AssetLoader.promptBG, tempX,tempY);
		
	if(fontRenderer[1].renderIncrementally							(1,batch, "       STAR    \t: ",	Player.STAR, 	310,450)) {
		if(fontRenderer[2].renderIncrementally						(1,batch, "      CANDY    \t: ", 	Player.CANDY, 	310, 420)) {
			if(fontRenderer[3].renderIncrementally					(1,batch, "       COIN    \t : ", 	Player.COIN, 	310, 390)) {
				if(fontRenderer[4].renderIncrementally				(1,batch, "   CUPCAKE    \t: ", 	Player.CUPCAKE, 540, 450)) {
					if(fontRenderer[5].renderIncrementally			(1,batch, "      FRUIT    \t : ", 	Player.FRUIT, 	540, 420)) {
						if(fontRenderer[6].renderIncrementally		(1,batch,"         SPP    : ", 	saveFromFile.getSuperPowerCount(),540,390)) {
							Iterator<Character> it = Player.collectedLetters.iterator();
							fontRenderer[7].font.setColor(Color.MAGENTA);
							String str = "Letters :";
							while(it.hasNext()){
								str += it.next() + " ";
							}
							fontRenderer[7].render(batch, str, 310 + (450 - str.length())/3.5f, 360);
							
							if(isAllWordCollected()){
								if(fontRenderer[1].renderIncrementally(10,batch, "      BONUS    \t: ",1000, 400, 330)){
									display(tempY, 300);
								}
							}else display(tempY, 330);
							
							}
						}
					}
				}
			}
		}
	
	
//		fontRenderer[1].render(batch, "collected all word ? " + isAllWordCollected(), 100, 300);
		
		//catchStar.setScreen(catchStar.getMainScreen());

		
		
		batch.end();
	}
	
	private void display(float tempY,int y){
		if(fontRenderer[0].renderIncrementally	(10,batch,"      SCORE    \t: ",	Player.SCORE, 	400,y)) {
			if(highScore){batch.draw(AssetLoader.highScore, 310 + AssetLoader.promptBG.getWidth() - AssetLoader.highScore.getWidth()/2, tempY,100,100);}
			fontRenderer[7].render(batch,"Tap To Continue",400,270);
			if(Gdx.input.isTouched()){
				fontRenderer[0].render(batch, "Touched on screen", Gdx.graphics.getWidth()/2.54f, Gdx.graphics.getHeight()/1.75f);
				setScore = false;highScore = false;
				catchStar.setScreen(catchStar.getMainScreen());	}
			}
	}

	private boolean isAllWordCollected() {
		char[] currWord = Word.currentWord;
		Object[] collectedWord = Player.collectedLetters.toArray();
		if(currWord.length == collectedWord.length){
			System.out.println("Passed length condition");
			return true;
		}else{ System.out.println("Failed length condition");return false;}
		
	}
	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	
	@Override
	public void hide() {
		// TODO Auto-generated method stub

	} 

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		batch.dispose();

	}
	
	
	
	
}
