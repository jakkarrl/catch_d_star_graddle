package com.jakkarrlgames.game;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeType.Bitmap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.jakkarrlgames.assetloader.AssetLoader;
import com.jakkarrlgames.entity.Entity;
import com.jakkarrlgames.entity.FontRenderer;
import com.jakkarrlgames.entity.GUIEntity;
import com.jakkarrlgames.helperclass.SaveGame;
import com.jakkarrlgames.persistant.Files;
import com.jakkarrlgames.scroller.ScrollHandler;

public class MainScreen implements Screen {

	
	private GUIEntity playNow;
	private GUIEntity store,help;
	private GUIEntity exit;
	private GUIEntity music;
	private Texture back,blur;
	private Sprite backSprite;
	private Entity backEntity;
	private Entity faderEntity;
	private Texture faderTexture;
	private Sprite faderSprite;
	private ScrollHandler scrollHandler;
	private Skin skin;
	private Stage stage;
	private catch_d_stars catchdstars;
	private float screenWidth,screenHeight;
	private float ratio = (screenWidth/screenHeight);
	OrthographicCamera cam;
	private SpriteBatch batch;
	private Music mainMenuMusic;
	private SaveGame saveGame;
	private FontRenderer fontRenderer;
	private BitmapFont font;
	float alpha=1.0f;
	public MainScreen(catch_d_stars catchdstars){
		this.catchdstars = catchdstars;
		
	}
	
	@Override
	public void render(float delta) {
		update();
		
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
//		
		if(saveGame.isMusicOn()) {
			music.setDefaultTexture(AssetLoader.musicEnabled);
		}
		else {
			music.setDefaultTexture(AssetLoader.musicDisabled);
		}
		batch.setProjectionMatrix(cam.combined);
		batch.begin();
		backSprite.draw(batch);
		scrollHandler.render(batch);
		batch.draw(blur, 0, 0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		batch.end();
		playNow.render(batch, cam);
		store.render(batch, cam);
		exit.render(batch, cam);
		batch.begin();
		String str = "HighScore : " + saveGame.getScore();
		faderSprite.setAlpha(alpha);
		faderSprite.draw(batch);
		fontRenderer.render(batch,str , 30, 50);
//		font.draw(batch, str, screenWidth/3.5f - str.length(), 200);
		
		batch.end();
		help.render(batch, cam);
		music.render(batch, cam);
		
		stage.act();
		stage.draw();
	//	catchdstars.setScreen(catchdstars.getGamePlay());

	}

	private void update() {
		Gdx.input.setInputProcessor(stage);
		scrollHandler.update(Gdx.graphics.getDeltaTime());
		if(alpha>0.01f) {
			alpha-=0.02f;
			System.out.println("alpha = "+alpha);
		}
		else {
			//alpha=0.0f;
		}
		if(saveGame.isMusicOn()) {
			if(!mainMenuMusic.isPlaying()) {
				if(Files.getSaveGame().isMusicOn()) mainMenuMusic.play();
			}
		}
		else {
			mainMenuMusic.stop();
		}
		screenWidth = Gdx.graphics.getWidth();
		screenHeight= Gdx.graphics.getHeight(); 
		
//		System.out.println(screenWidth+","+screenHeight);
		if(playNow.isPressed()) {
			if(mainMenuMusic.isPlaying()) {
				mainMenuMusic.stop();
			}
			if(Files.isFirstTimePlay()){
				catchdstars.setScreen(catchdstars.getHelp());
			}else{
				
				catchdstars.setScreen(catchdstars.getPreGame());
			}
		}
		
		if(store.isPressed()) {
			if(mainMenuMusic.isPlaying()) {
				mainMenuMusic.stop();
			}
			catchdstars.setScreen(catchdstars.getStoreScreen());
//			catchdstars.setScreen(catchdstars.getDemo());
		}
		if(help.isPressed()){
			if(mainMenuMusic.isPlaying()) {
				mainMenuMusic.stop();
			}
			catchdstars.setScreen(catchdstars.getHelp());
		}
		
		if(music.isPressed()){
			if(saveGame.isMusicOn()) {
//				catch_d_stars.isMusicOn = false;
//				saveGame.setMusicOn(catch_d_stars.isMusicOn);
				saveGame.setMusicOn(false);
				catch_d_stars.isMusicOn = false;
			}
			else{
//				catch_d_stars.isMusicOn = true;
				saveGame.setMusicOn(true);
				catch_d_stars.isMusicOn = true;
			}
			Files.saveGameStatus(saveGame);
		}
		if(exit.isPressed()) {
			displayDialog("Exit", "Do you want to exit from the game ?");
		}
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		
		screenWidth = Gdx.graphics.getWidth();
		screenHeight= Gdx.graphics.getHeight(); 
		GamePlay.isPlaying = false;
		cam = catchdstars.getObject().getCam();
		cam.setToOrtho(false, screenWidth, screenHeight);
		backEntity = catchdstars.getObject().getBackEntity();
		back = AssetLoader.background;
		blur = AssetLoader.blur;	
		backEntity.getRegionFromTexture(back, 0, 0, back.getWidth(), back.getHeight());
		cam.setToOrtho(false,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		backSprite = backEntity.createSprite(new Vector2(Gdx.graphics.getWidth(),Gdx.graphics.getHeight()),new Vector2(0,0));
		
		scrollHandler = catchdstars.getObject().getScrollHandler();
		scrollHandler.resetToOld();
		scrollHandler.startScrolling();
		
		faderEntity = catchdstars.getObject().getFaderEntity();
		faderTexture = AssetLoader.fader;
		faderEntity.getRegionFromTexture(faderTexture, 0, 0, back.getWidth(), back.getHeight());
		cam.setToOrtho(false,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		faderSprite = faderEntity.createSprite(new Vector2(Gdx.graphics.getWidth(),Gdx.graphics.getHeight()),new Vector2(0,0));
		
		mainMenuMusic = AssetLoader.mainMenuMusic;
		skin = catchdstars.getObject().getSkin();
		stage = catchdstars.getObject().getStage();
		batch = catchdstars.getObject().getBatch();
		batch.setProjectionMatrix(cam.combined);
		
		playNow = catchdstars.getObject().getPlayNow();
		playNow.setPosition(screenWidth/20f,screenHeight/1.8f);
		playNow.setSize(screenWidth/4,screenHeight/5.8f);
		
		help = catchdstars.getObject().getHelp();
		help.setDefaultTexture(AssetLoader.help);
		help.setPressedTexture(AssetLoader.helpPressed);
		help.setPosition(Gdx.graphics.getWidth() - 60,15);
		help.setSize(50,50);
		
		music = catchdstars.getObject().getMusic();
		music.setPosition(Gdx.graphics.getWidth()- 120, 11);
		music.setSize(50,50);
		
		store = new GUIEntity(AssetLoader.storeText,AssetLoader.storePressedText);
		store.setPosition(screenWidth/20f,screenHeight/2.7f);
		store.setSize(screenWidth/4,screenHeight/5.8f);
		
		exit = catchdstars.getObject().getExit();
		exit.setPosition(screenWidth/20f,screenHeight/5.0f);
		exit.setSize(screenWidth/4,screenHeight/5.8f);
		
		
		
		/*playNow = new GUIEntity("data/images/playNow.png");
		playNow.setPosition(100,100);
		playNow.setSize(200,100);
		store = new GUIEntity("data/images/store.png");
		store.setPosition(100,200);
		store.setSize(200,100);
		exit = new GUIEntity("data/images/exit.png");
		exit.setPosition(100,300);
		exit.setSize(200,100);*/
		
		saveGame = Files.getSaveGame();
		fontRenderer= catchdstars.getObject().getFontRenderer();
		fontRenderer.setFonts("data/font/kenvector_future_thin.ttf", 40);
		fontRenderer.font.setColor(Color.BLACK);
//		font = new BitmapFont();
	}
	
	private void displayDialog(String title,String str){
		System.out.println("Call for Exit");
		new Dialog(title, skin){
			@Override
			protected void result(Object object) {
				if(object.equals(true)){
					System.out.println("Exiting");
					Gdx.app.exit();
				}else if(object.equals(false)){
					System.out.println("alive aagain");
				}
			}
		}.text(str).button("   Yes   ",true).button("   No   ",false).show(stage).toFront();
		
		Iterator<Actor> act = stage.getActors().iterator();
		while(act.hasNext()){
			Actor actor = act.next();
			actor.setSize(400,100);
		}
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		scrollHandler.dipose();
	}

}
