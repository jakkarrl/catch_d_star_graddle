package com.jakkarrlgames.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Json;
import com.jakkarrlgames.assetloader.AssetLoader;
import com.jakkarrlgames.assetloader.Loader;
import com.jakkarrlgames.entity.Player;
import com.jakkarrlgames.helperclass.ObjectInjector;
import com.jakkarrlgames.helperclass.SaveGame;

public class Loading implements Screen {
TextureRegion progressBar;
TextureRegion progressBarFrame;
TextureRegion progressShade;
Texture loadingTexture;
Texture background;
catch_d_stars catchdstars;
SpriteBatch batch;
private String character;
float width;
float height;
float screenWidth;
float screenHeight;
Loader loader;
	public Loading(catch_d_stars game) {
		width = 1;
		height = 1;
		this.catchdstars = game;
	}

	@Override
	public void show() {
		loader = new Loader();
		//To load the selected Character
		if(catchdstars.getSaveGame().getCharacter() == null){
			character = "Boy";
		}
		else{
			character =catchdstars.getSaveGame().getCharacter();
		}
		
				
		
		batch = new SpriteBatch();
		loadingTexture = new Texture(Gdx.files.internal("data/images/loading/loading.png"));
		progressBar = new TextureRegion(loadingTexture,000,304,879,120);
		progressShade = new TextureRegion(loadingTexture,000,016,879,056);
		progressBarFrame = new TextureRegion(loadingTexture,000,140,879,120);
		background = new Texture(Gdx.files.internal("data/images/background/storyBoard.png"));
		
	}

	@Override
	public void render(float delta) {
		screenWidth = Gdx.graphics.getWidth();
		screenHeight = Gdx.graphics.getHeight();
//		System.out.println("TEXTURE : "+nightBackGround); 
		update();
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		batch.draw(background, 0, 0, Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
//		batch.draw(progressBarFrame,(screenWidth/4f),screenWidth/8,screenWidth/2,screenHeight/9.6f);
//		batch.draw(progressBar,(screenWidth/4f),screenWidth/8,width,screenHeight/9.6f);
//		batch.draw(progressShade,(screenWidth/4f),screenWidth/6,screenWidth/2,screenHeight/19.2f);
		
//		batch.draw(progressBarFrame,(screenWidth/4f),screenHeight/15.5f,screenWidth/2,screenHeight/9.6f);
//		batch.draw(progressBar,(screenWidth/4f),screenHeight/15.5f,width,screenHeight/9.6f);
//		batch.draw(progressShade,(screenWidth/4f),screenHeight/15.5f,screenWidth/2,screenHeight/19.2f);
		
		batch.draw(progressBarFrame,(screenWidth/4f),screenHeight/26.5f,screenWidth/2,screenHeight/15.6f);
		batch.draw(progressBar,(screenWidth/4f),screenHeight/26.5f,width,screenHeight/15.6f);
		batch.draw(progressShade,(screenWidth/4f),screenHeight/26.5f,screenWidth/2,screenHeight/25.2f);
		
		batch.end();
		if(catch_d_stars.manager.update()) {
			AssetLoader.load(character);
			ObjectInjector object = new ObjectInjector(catchdstars);
			catchdstars.SetObjectInjector(object);
			catchdstars.getGameOut().setFontArray(object.getFontArray());
			catchdstars.setScreen(catchdstars.getMainScreen());
		}
	}

	private void update() {
		width = screenWidth/2*catch_d_stars.manager.getProgress();
//		System.out.println(catch_d_stars.manager.getProgress());
		
	}

	@Override
	public void resize(int width, int height) {
		

	}

	
	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
