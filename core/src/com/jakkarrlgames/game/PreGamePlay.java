package com.jakkarrlgames.game;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.jakkarrlgames.assetloader.AssetLoader;
import com.jakkarrlgames.entity.Player;
import com.jakkarrlgames.helperclass.SaveGame;
import com.jakkarrlgames.persistant.Files;

public class PreGamePlay implements Screen {

	public static Animation playerRunAnimation,playerJumpAnimation,playerDeadAnimation;
	private String character;
	private Texture player;
	private TextureRegion player1,player2,player3,player4,player5,player6,player7,player8,player9,player10;
	private catch_d_stars catchdstars;
	public PreGamePlay(catch_d_stars catchdstars){
		this.catchdstars = catchdstars;
	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		SaveGame saveGame = Files.getSaveGame();
		character = saveGame.getCharacter();
		if(character.equals("Girl")){
			System.out.println("Girl Loop");
//			player = catch_d_stars.manager.get("data/images/CharacterSpriteSheet/NinjaGirl/ninjaGirl.png");
//			player1 = new TextureRegion(player,964,267,167,243);
//			player2 = new TextureRegion(player,1398,0,166,252);
//			player3 = new TextureRegion(player,783,748,163,251);
//			player4 = new TextureRegion(player,783,267,180,238);
//			player5 = new TextureRegion(player,1044,0,181,241);
//			player6 = new TextureRegion(player,783,506,167,241);
//			player7 = new TextureRegion(player,1132,242,165,251);
//			player8 = new TextureRegion(player,1565,0,161,256);
//			player9 = new TextureRegion(player,951,511,163,253);
//			player10 = new TextureRegion(player,1226,0,171,238);

//			playerRunAnimation = new Animation(0.08f, new  TextureRegion[]{player1,player2,player3,player4,player5,player6,player7,player8,player9,player10});
//			playerRunAnimation.setPlayMode(PlayMode.LOOP);
			
			//Jump Animation
//			player1 = new TextureRegion(player,1277,505,152,237);
//			player2 = new TextureRegion(player,1298,253,155,251);
//			player3 = new TextureRegion(player,1115,740,145,238);
//			player4 = new TextureRegion(player,1601,257,144,237);
//			player5 = new TextureRegion(player,1881,0,144,237);
//			player6 = new TextureRegion(player,1430,505,144,237);
//			player7 = new TextureRegion(player,947,765,155,234);
//			player8 = new TextureRegion(player,1454,257,146,230);
//			player9 = new TextureRegion(player,1115,511,161,228);
//			player10 = new TextureRegion(player,1727,0,153,228);

//			playerJumpAnimation = new Animation(0.15f, new  TextureRegion[]{player1,player2,player3,player4,player5,player6,player7,player8,player9,player10});
//			playerJumpAnimation.setPlayMode(PlayMode.LOOP);
			//Dead Animation
//			player1 = new TextureRegion(player,0,0,260,266);
//			player2 = new TextureRegion(player,261,0,260,266);
//			player3 = new TextureRegion(player,522,0,260,266);
//			player4 = new TextureRegion(player,0,267,260,266);
//			player5 = new TextureRegion(player,261,267,260,266);
//			player6 = new TextureRegion(player,0,534,260,266);
//			player7 = new TextureRegion(player,522,267,260,266);
//			player8 = new TextureRegion(player,261,534,260,266);
//			player9 = new TextureRegion(player,522,534,260,266);
//			player10 = new TextureRegion(player,783,0,260,266);

//			playerDeadAnimation = new Animation(0.08f, new  TextureRegion[]{player1,player2,player3,player4,player5,player6,player7,player8,player9,player10});
			playerRunAnimation = AssetLoader.girlRunAnimation;
			playerJumpAnimation = AssetLoader.girlJumpAnimation;
			playerDeadAnimation = AssetLoader.girlDeadAnimation;
			
			
		}else if(character.equals("Boy")){
			System.out.println("Boy loop");
//			player = catch_d_stars.manager.get("data/images/CharacterSpriteSheet/CowBoy/cowBoy.png");
//			//RunAnimation
//			player1 = new TextureRegion(player,933,752,172,237);
//			player2 = new TextureRegion(player,1323,243,181,245);
//			player3 = new TextureRegion(player,1446,0,184,242);
//			player4= new TextureRegion(player,1244,0,201,231);
//			player5 = new TextureRegion(player,933,278,201,235);
//			player6 = new TextureRegion(player,1807,0,170,235);
//			player7 = new TextureRegion(player,1631,0,175,245);
//			player8 = new TextureRegion(player,933,514,183,237);
//			player9 = new TextureRegion(player,1117,514,179,223);
//			player10 = new TextureRegion(player,1135,278,187,231);
//
//			playerRunAnimation = new Animation(0.08f, new  TextureRegion[]{player1,player2,player3,player4,player5,player6,player7,player8,player9,player10});
//			playerRunAnimation.setPlayMode(PlayMode.LOOP);
//
//			//JumpAnimation
//			player1 = new TextureRegion(player,1297,510,167,235);
//			player2 = new TextureRegion(player,1117,738,164,243);
//			player3 = new TextureRegion(player,1505,246,161,223);
//			player4 = new TextureRegion(player,1667,246,161,222);
//			player5 = new TextureRegion(player,1505,470,161,222);
//			player6 = new TextureRegion(player,1282,746,161,222);
//			player7 = new TextureRegion(player,1829,461,156,241);
//			player8 = new TextureRegion(player,1444,746,158,239);
//			player9 = new TextureRegion(player,1829,236,160,224);
//			player10 = new TextureRegion(player,1667,469,160,224);
//
//			
//			playerJumpAnimation = new Animation(0.03f, new  TextureRegion[]{player1,player2,player3,player4,player5,player6,player7,player8,player9,player10});
//			playerJumpAnimation.setPlayMode(PlayMode.LOOP);
//			
//			//DeadAnimation
//			player1 = new TextureRegion(player,0,0,310,277);
//			player2 = new TextureRegion(player,311,0,310,277);
//			player3 = new TextureRegion(player,622,0,310,277);
//			player4 = new TextureRegion(player,0,278,310,277);
//			player5 = new TextureRegion(player,0,556,310,277);
//			player6 = new TextureRegion(player,311,278,310,277);
//			player7 = new TextureRegion(player,311,556,310,277);
//			player8 = new TextureRegion(player,622,278,310,277);
//			player9 = new TextureRegion(player,622,556,310,277);
//			player10 = new TextureRegion(player,933,0,310,277);
//
//			playerDeadAnimation = new Animation(0.08f, new  TextureRegion[]{player1,player2,player3,player4,player5,player6,player7,player8,player9,player10});
			playerRunAnimation = AssetLoader.boyRunAnimation;
			playerJumpAnimation = AssetLoader.boyJumpAnimation;
			playerDeadAnimation = AssetLoader.boyDeadAnimation;
			
		}
		System.out.println("--PrGame");
		
		Player.SCORE = 0;
		Player.CANDY = 0;
		Player.COIN = 0;
		Player.CUPCAKE = 0;
		Player.FRUIT = 0;
		Player.STAR = 0;
		
		catchdstars.setScreen(catchdstars.getGamePlay());
		
		
		
		System.out.println("existing pregame");
		
		
	}
	
	public void update(float delta){
		
	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		player1=null;player2=null;player3=null;player4=null;player5=null;player6=null;player7=null;player8=null;player9=null;
		player.dispose();
	}

}
