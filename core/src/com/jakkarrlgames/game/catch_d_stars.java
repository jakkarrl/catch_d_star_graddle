package com.jakkarrlgames.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Json;
import com.jakkarrlgames.entity.Player;
import com.jakkarrlgames.helperclass.ClockCount;
import com.jakkarrlgames.helperclass.ObjectInjector;
import com.jakkarrlgames.helperclass.SaveGame;
import com.jakkarrlgames.persistant.Files;
import com.jakkarrlgames.scroller.ScrollHandler;

public class catch_d_stars extends Game{
	private ObjectInjector objectInjector;
//	public static boolean DEBUG_ON = false;
	private MainScreen mainMenu;
	private PreGamePlay preGame;
	private GamePlay gamePlay;
	private GameOut gameOut;
	private Loading loading;
	private DemoDialog demo;
	private StoreScreen store;
	public static int GAME_LEVEL = 1;
	public static AssetManager manager;
	public static float screenWidth,screenHeight;
	private SaveGame saveGame;
	public static boolean isMusicOn;
	private Help help;
	public void create() {
		
//		Json json = new Json();
//		FileHandle file = Gdx.files.local(("bin/sampleJson.json"));
//		if(file.exists()){
//			saveGame = json.fromJson(SaveGame.class, file);
//		}else{
//			saveGame = new SaveGame();
//			saveGame.setScore(0);
//			saveGame.setStar(0);
//			saveGame.setCandy(0);
//			saveGame.setCoin(0);
//			saveGame.setCupCake(0);
//			saveGame.setFruit(0);
//			saveGame.setCharacter("Girl");
//		}
//		file.writeString(json.toJson(saveGame),false);
		saveGame = Files.getSaveGame();
		isMusicOn = saveGame.isMusicOn();
		
		loading = new Loading(this);
		manager = new AssetManager();
//		objectInjector = new ObjectInjector();
		mainMenu = new MainScreen(this);
		preGame = new PreGamePlay(this);
		gamePlay = new GamePlay(this);
		gameOut = new GameOut(this);
		store = new StoreScreen(this);
		demo = new DemoDialog(this);
		help = new Help(this);
		this.setScreen(loading);
		
		screenWidth = Gdx.graphics.getWidth();
		screenHeight = Gdx.graphics.getHeight();
		
	}
	public PreGamePlay getPreGame(){return preGame;}
	public GamePlay getGamePlay(){return gamePlay;}
	public GameOut getGameOut(){return gameOut;}
	public Loading getLoading(){return loading; }
	public StoreScreen getStoreScreen() { return store;}
	public MainScreen getMainScreen() {return mainMenu;}
	public void setSaveGame(SaveGame game){this.saveGame = game;}
	public SaveGame getSaveGame(){return saveGame;}
	public DemoDialog getDemo(){return demo;}
	public Help getHelp(){return help;}
	public ObjectInjector getObject(){return objectInjector;}
	public void SetObjectInjector(ObjectInjector object){this.objectInjector = object;}
}
