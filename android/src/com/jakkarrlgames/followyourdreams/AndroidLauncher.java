package com.jakkarrlgames.followyourdreams;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import com.jakkarrlgames.game.catch_d_stars;


public class AndroidLauncher extends AndroidApplication {
	RelativeLayout layout;
	View gameView;
	AdView adView;
	InterstitialAd mInterstitialAd;
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		layout = new RelativeLayout(this);
		 mInterstitialAd = new InterstitialAd(this);
	        mInterstitialAd.setAdUnitId("ca-app-pub-1108080929046936/4337391773");
	        requestNewInterstitial();
		
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initializeWindows();
		gameView = initializeForView(new catch_d_stars(),config);
		 AdView adView = new AdView(this);
		 adView.setAdSize(AdSize.SMART_BANNER);
		 adView.setAdUnitId("ca-app-pub-1108080929046936/2860658578");
		 // Put in your secret key here
		 AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
		 adView.loadAd(adRequestBuilder.build());
		 layout.addView(gameView);
		 RelativeLayout.LayoutParams adParams = 
		            new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, 
		                    RelativeLayout.LayoutParams.WRAP_CONTENT);
		        adParams.addRule(RelativeLayout.ALIGN_BOTTOM);
		        adParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
		        layout.addView(adView, adParams);
		        setContentView(layout);
		        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		        
		//initialize(new catch_d_stars(), config);
		        
		        mInterstitialAd.setAdListener(new AdListener() {
		            @Override
		            public void onAdClosed() {
		                requestNewInterstitial();
		            }
		        });
	}
	
	@Override
	public void onBackPressed() {
		if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
        	Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            startActivity(intent);
        }
	}
	
	

private void requestNewInterstitial() {
    AdRequest adRequest = new AdRequest.Builder().build();

    mInterstitialAd.loadAd(adRequest);
}

	
	
	public void initializeWindows() {
		 requestWindowFeature(Window.FEATURE_NO_TITLE);
	        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
	                WindowManager.LayoutParams.FLAG_FULLSCREEN);
	        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
	}
}
