package com.jakkarrlgames.test.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.jakkarrlgames.game.catch_d_stars;


public class DesktopLauncher640X480 {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(new catch_d_stars(), config);
		config.width = 640;config.height = 480;
//		config.width = 800;config.height = 600;
//		config.width = 1200;config.height = 800;
	}
}
