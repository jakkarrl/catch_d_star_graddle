package com.jakkarrlgames.test.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.jakkarrlgames.game.catch_d_stars;


public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(new catch_d_stars(), config);
		config.useGL30=true;
		config.width = 1200;
		config.height = 768;
	}
	
}
